#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/* Here is a good place to include header files that are required across
your application. */

#define configUSE_PREEMPTION                    0	//0- Cooperative RTOS scheduler, have to use taskYIELD()
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 1	//For CLZ intsruction, but now we have 32 task priorities only.
#define configUSE_TICKLESS_IDLE                 0 //Set configUSE_TICKLESS_IDLE to 1 to use the low power tickless mode, or 0 to keep the tick interrupt running at all times.
#define configCPU_CLOCK_HZ                      ((unsigned long) 72000000)	//HCLK frequency, for SysTick timer.
#define configTICK_RATE_HZ                      ((TickType_t) 1000)	//1 time in millisecond.
#define configMAX_PRIORITIES                    (5) //The number of priorities available to the application tasks.
#define configMINIMAL_STACK_SIZE                ((unsigned short) 130) //The size of the stack for the idle task. Generally this should not be less then the value from the demo application for your port. 
#define configMAX_TASK_NAME_LEN                 (16)	//Length of the Name of task (include '\0'), for debug purposes.
#define configUSE_16_BIT_TICKS                  0	//In Cortex we have 24 bit SysTick timer
#define configIDLE_SHOULD_YIELD                 0 //0 - disable. Something for tasks with idle task's priority.
#define configUSE_TASK_NOTIFICATIONS            0 //0 - disable. I don't sure, may be it's a good idea for unblocking tasks instead semaphores.
#define configUSE_MUTEXES                       1	//For
#define configUSE_RECURSIVE_MUTEXES             1	//	    education
#define configUSE_COUNTING_SEMAPHORES           1	//					purposes.
#define configUSE_ALTERNATIVE_API               0 /* Deprecated! */
#define configQUEUE_REGISTRY_SIZE               10 //Number of queues/semaphores for debug monitoring. See vQueueAddToRegistry() and vQueueUnregisterQueue() for more information. 
#define configUSE_QUEUE_SETS                    0	//0 - disable. I don't sure. Queues and semaphores are grouped into sets and task can be blocked now by set, not only one Queue|Semaphore.
#define configUSE_TIME_SLICING                  1 //If configUSE_TIME_SLICING is set to 1 (or undefined) then tasks that share the same priority will time slice.
#define configUSE_NEWLIB_REENTRANT              0 //For the NEWLIB using.
#define configENABLE_BACKWARD_COMPATIBILITY     1 //For #define maping the names used in versions of FreeRTOS prior to version 8.0.0 to the names used in FreeRTOS version 8.0.0.
#define configNUM_THREAD_LOCAL_STORAGE_POINTERS 5 //Thread local storage (or TLS) allows the application writer to store values inside a task's control block.

/* Memory allocation related definitions. */
#define configSUPPORT_STATIC_ALLOCATION        ( 0 ) //1 - RTOS objects can be created using RAM provided by the application writer, instead heap.
#define configSUPPORT_DYNAMIC_ALLOCATION        1 //If 0 then RTOS objects can only be created using RAM provided by the application writer, instead automatically allocation from heap. 
#define configTOTAL_HEAP_SIZE                  ((size_t) (120 * 1024))	//120kB RAM to HEAP, 8 kB RAM to global variables and e.t.c.
#define configAPPLICATION_ALLOCATED_HEAP       ( 0 )

/* Hook function related definitions. */
#define configUSE_IDLE_HOOK                     0 //0 - disable. Idle Hook is way to run user's C function when the Idle Task works.
#define configUSE_TICK_HOOK                     0 //The tick interrupt can optionally call an application defined hook (or callback) function - the tick hook.
#define configCHECK_FOR_STACK_OVERFLOW          0 //Can detect stack overflow. Calls void vApplicationStackOverflowHook( TaskHandle_t xTask, signed char *pcTaskName ); when it happends.
#define configUSE_MALLOC_FAILED_HOOK            0	//0 - desable. User function will be called if pvPortMalloc() ("embeded malloc()") ever returns NULL.
#define configUSE_DAEMON_TASK_STARTUP_HOOK      0 //Any application initialisation code that needs the RTOS to be running can be placed in the hook function.

/* Run time and task stats gathering related definitions. */
#define configGENERATE_RUN_TIME_STATS           0 //FreeRTOS can optionally collect information on the amount of processing time that has been used by each task.
#define configUSE_TRACE_FACILITY                0 //Set to 1 if you wish to include additional structure members and functions to assist with execution visualisation and tracing.
#define configUSE_STATS_FORMATTING_FUNCTIONS    0 //Set configUSE_TRACE_FACILITY and configUSE_STATS_FORMATTING_FUNCTIONS to 1 to use vTaskList() and vTaskGetRunTimeStats().

/* Co-routine related definitions. */
#define configUSE_CO_ROUTINES                   0	//Set to 1 to include co-routine functionality in the build.
#define configMAX_CO_ROUTINE_PRIORITIES         1

/* Software timer related definitions. */
#define configUSE_TIMERS                        0	//A software timer (or just a 'timer') allows a function to be executed at a set time in the future.
//I assume that three next parameters will not care, if configUSE_TIMERS is set to 0.
#define configTIMER_TASK_PRIORITY               3 //Sets the priority of the software timer service/daemon task.
#define configTIMER_QUEUE_LENGTH                10 //Sets the length of the software timer command queue.
#define configTIMER_TASK_STACK_DEPTH            configMINIMAL_STACK_SIZE

/* Interrupt nesting behaviour configuration. */
#define configKERNEL_INTERRUPT_PRIORITY         255 //  equivalent 0xF0, the weakest priority 
#define configMAX_SYSCALL_INTERRUPT_PRIORITY    191 //  equivalent 0xB0,  interrupts with priority >= 0xA0 will never be delayed by the RTOS kernel execution
//#define configMAX_API_CALL_INTERRUPT_PRIORITY   //[dependent on processor and application] == configMAX_SYSCALL_INTERRUPT_PRIORITY

/* Define to trap errors during development. We suppose, that UART has already enabled.*/
void vAssertCalled(char * file_name, uint32_t line_num);
#define configASSERT(x) if( ( x ) == 0 ) vAssertCalled( __FILE__, __LINE__ );	//For DEBUG 
//#define configASSERT(x)  if( ( x ) == 0 ) { taskDISABLE_INTERRUPTS(); for( ;; ); }

/* FreeRTOS MPU specific definitions. */
#define configINCLUDE_APPLICATION_DEFINED_PRIVILEGED_FUNCTIONS 0
/*If configINCLUDE_APPLICATION_DEFINED_PRIVILEGED_FUNCTIONS is set to 1 then the application writer must provide a header
file called "application_defined_privileged_functions.h", in which functions the application writer needs to
execute in privileged mode can be implemented.*/

/* Optional functions - most linkers will remove unused functions anyway. */
#define INCLUDE_vTaskPrioritySet                1
#define INCLUDE_uxTaskPriorityGet               1
#define INCLUDE_vTaskDelete                     0
#define INCLUDE_vTaskSuspend                    1	//It enables using macros portMAX_DELAY in timeouts.
#define INCLUDE_xResumeFromISR                  1
#define INCLUDE_vTaskDelayUntil                 1
#define INCLUDE_vTaskDelay                      1
#define INCLUDE_xTaskGetSchedulerState          1
#define INCLUDE_xTaskGetCurrentTaskHandle       1
#define INCLUDE_uxTaskGetStackHighWaterMark     1	//Disable after debugging!
#define INCLUDE_xTaskGetIdleTaskHandle          0
#define INCLUDE_eTaskGetState                   1
#define INCLUDE_xEventGroupSetBitFromISR        1
#define INCLUDE_xTimerPendFunctionCall          0
#define INCLUDE_xTaskAbortDelay                 0
#define INCLUDE_xTaskGetHandle                  1
#define INCLUDE_xTaskResumeFromISR              1

/* Cortex-M specific definitions. */
#ifdef __NVIC_PRIO_BITS
	/* __BVIC_PRIO_BITS will be specified when CMSIS is being used. */
	#define configPRIO_BITS       		__NVIC_PRIO_BITS
#else
	#define configPRIO_BITS       		4        /* 15 priority levels */
#endif

/* Definitions that map the FreeRTOS port interrupt handlers to their CMSIS
standard names. */
#define xPortSysTickHandler SysTick_Handler
#define xPortPendSVHandler PendSV_Handler
#define vPortSVCHandler SVC_Handler

/* A header file that defines trace macro can be included here. */

#endif /* FREERTOS_CONFIG_H */
