#ifndef __UART_OUTPUT_H
#define __UART_OUTPUT_H

//For UART2 (system messages). It assumes, APB1 bus has frequency 36MHz (or APB2 - 72 Mhz). Baud rate 2 000 000 max. Baud rate - 19200.
void USART_init_myfunc(void);

//Sends string throught UART2 (or UART6). String must be \0 terminated.
void USART_send_my_string(uint8_t * string);


//This functions for interaction with ESP Wi-Fi module. 
//We suppose, that APB2 bus has frequency 72MHz. Baud rate 2 000 000 max. Let's take baud rate = 19200. 
void USART_ESP_init(void);

//Sends string to ESP Wi-Fi throught UART6. String must be \0 terminated.
void USART_ESP_send_string(uint8_t * string);

#endif
