#include "stm32f4xx.h" // SPL config file stm32f4xx_conf.h is autoincluded (by USE_STDPERIPH_DRIVER in stm32f4xx.h)
#include "stm32f411_bit_band.h" //Macros and definitions for using bit band.
#include "UART_output.h" //Settings for UART.

//For UART2. It assumes, that GPIOA is clocked. APB1 bus has frequency 36MHz. Baud rate 2 000 000 max. Let's take baud rate = 19200.) 
void USART_init_myfunc(void)
{
	
	// Enable clock of GPIOA (it still works correct if clock has already been enabled before.)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	
	//Enable clock to UART2 in APB1 bus
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
	
	
/*	For other peripherals:
	� Configure the desired I/O as an alternate function in the GPIOx_MODER register
	� Select the type, pull-up/pull-down and output speed via the GPIOx_OTYPER,
	GPIOx_PUPDR and GPIOx_OSPEEDER registers, respectively
	� Connect the I/O to the desired AFx in the GPIOx_AFRL or GPIOx_AFRH register */
		
	//Setting Alternative Function for PA2 - USART_TX.
	//Setting Alternative Function for PA3 - USART_RX.
	//So, we need AF07, MODER - AF, OTYPER - push-pull, OSPEEDR - low, PUPDR - pull-up.
	
	GPIOA->MODER |= GPIO_MODER_MODER2_1 | GPIO_MODER_MODER3_1; //MODER - AF
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_2 | GPIO_OTYPER_OT_3); //Push-pull. It is not need to do after reset.
	GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR2 | GPIO_OSPEEDER_OSPEEDR3); // Low speed. It is not need after reset.
	GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR3_0); //Pull-up.
	GPIOA->AFR[0] |= (0x7 << 8 | 0x7 << 12); //AF07 for USART2.
	
	//Enable USART2
	BIT_BAND_REG_SET(USART2_BASE, USART_CR1, USART_CR1_UE_BIT_NUM);
	
	//Word length to 8 data bits. M := 0. Not need after reset.
	BIT_BAND_REG_CLR(USART2_BASE, USART_CR1, USART_CR1_M_BIT_NUM);
	
	//Two STOP bits.
	USART2->CR2 |= USART_CR2_STOP_1;
	
	//Oversampling by 16. OVER8 := 0
	BIT_BAND_REG_CLR(USART2_BASE, USART_CR1, USART_CR1_OVER8_BIT_NUM);

	//Config baudrate. USARTDIV(dec) == 117.1875 for Fck = 36MHz, baud rate = 19200, OVER8 = 0;
	//0.1875 * 16 -> 0x3, 117 -> 0x75. USART_BRR = 0x753
	USART2->BRR = 0x753;
/*
	//Config baudrate. USARTDIV(dec) == 468.75 for Fck = 36MHz, baud rate = 4800, OVER8 = 0;
	//0.75 * 16 -> 0xC, 468 -> 1D4. USART_BRR = 0x1D4C
	USART2->BRR = 0x1D4C; */
		
	//All settings have been completed.
}

//Sends string throught UART2. String must be \0 terminated.
void USART_send_my_string(uint8_t * string)
{
	uint16_t i;
	
	//Set TE bit to send Idle frame, to enable baud rate generator and to enable TX pin.
	BIT_BAND_REG_SET(USART2_BASE, USART_CR1, USART_CR1_TE_BIT_NUM);

	for(i = 0; string[i] != '\0'; i++)
	{
		//Wait untill the byte from USART_DR have been send.
		do
			; 
		while(BIT_BAND_REG_READ(USART2_BASE, USART_SR, USART_SR_TXE_BIT_NUM) != 1);
			
		//Sending character through UART.
		USART2->DR = (uint16_t)string[i];
	}
	
	
	//Wait untill the last byte from USART_DR have been send.
	do
		; 
	while(BIT_BAND_REG_READ(USART2_BASE, USART_SR, USART_SR_TC_BIT_NUM) != 1);
	
	//Clear TE bit to disable boud rate generator and TX pin.
	BIT_BAND_REG_CLR(USART2_BASE, USART_CR1, USART_CR1_TE_BIT_NUM);
}



//This functions for interaction with ESP Wi-Fi module.

//For UART6. It enables clock of GPIOC port, if not enabled. 
//We suppose, that APB2 bus has frequency 72MHz. Baud rate 2 000 000 max. Let's take baud rate = 19200.) 
void USART_ESP_init(void)
{
	// Enable clock of GPIOC (it still works correct if clock has already been enabled before.)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	
	//Enable clock to UART6 in APB2 bus
	RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
	
	
/*	For other peripherals:
	� Configure the desired I/O as an alternate function in the GPIOx_MODER register
	� Select the type, pull-up/pull-down and output speed via the GPIOx_OTYPER,
	GPIOx_PUPDR and GPIOx_OSPEEDER registers, respectively
	� Connect the I/O to the desired AFx in the GPIOx_AFRL or GPIOx_AFRH register */
		
	//Setting Alternative Function for PC6 - USART_TX.
	//Setting Alternative Function for PC7 - USART_RX.
	//So, we need AF08, MODER - AF, OTYPER - push-pull, OSPEEDR - low, PUPDR - pull-up.
	
	GPIOC->MODER |= GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1; //MODER - AF.
	GPIOC->OTYPER &= ~(GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7); //Push-pull. It is not need to do after reset.
	GPIOC->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR7); // Low speed. It is not need after reset.
	GPIOC->PUPDR |= (GPIO_PUPDR_PUPDR6_0 | GPIO_PUPDR_PUPDR7_0); //Pull-up.
//	GPIOC->AFR[0] |= ((0x00000008 << 24) | (0x00000008 << 28)); //AF08 for USART6 - compiler gives a warning for this line...?
GPIOC->AFR[0] |= ((0x80000000) | (0x08000000)); //AF08 for USART6
	
	//Enable USART6
	BIT_BAND_REG_SET(USART6_BASE, USART_CR1, USART_CR1_UE_BIT_NUM);
	
	//Word length to 8 data bits. M := 0. Not need after reset.
	BIT_BAND_REG_CLR(USART6_BASE, USART_CR1, USART_CR1_M_BIT_NUM);
	
	//Two STOP bits.
	USART6->CR2 |= USART_CR2_STOP_1;
	
	//Oversampling by 16. OVER8 := 0
	BIT_BAND_REG_CLR(USART6_BASE, USART_CR1, USART_CR1_OVER8_BIT_NUM);

	//Config baudrate. USARTDIV(dec) == 234.375 for Fck = 72MHz, baud rate = 19200, OVER8 = 0;
	//0.375 * 16 -> 0x6, 234 -> 0xEA. USART_BRR = 0xEA6
	USART6->BRR = 0xEA6;
/*
	//Config baudrate. USARTDIV(dec) == 468.75 for Fck = 36MHz, baud rate = 4800, OVER8 = 0;
	//0.75 * 16 -> 0xC, 468 -> 1D4. USART_BRR = 0x1D4C
	USART2->BRR = 0x1D4C; */
		
	//All settings have been completed.
}

//Sends string throught UART6. String must be \0 terminated.
void USART_ESP_send_string(uint8_t * string)
{
	uint16_t i;
	
	//Set TE bit to send Idle frame, to enable baud rate generator and to enable TX pin.
	BIT_BAND_REG_SET(USART6_BASE, USART_CR1, USART_CR1_TE_BIT_NUM);

	for(i = 0; string[i] != '\0'; i++)
	{
		//Wait untill the byte from USART_DR have been send.
		do
			; 
		while(BIT_BAND_REG_READ(USART6_BASE, USART_SR, USART_SR_TXE_BIT_NUM) != 1);
			
		//Sending character through UART.
		USART6->DR = (uint16_t)string[i];
	}
	
	
	//Wait untill the last byte from USART_DR have been send.
	do
		; 
	while(BIT_BAND_REG_READ(USART6_BASE, USART_SR, USART_SR_TC_BIT_NUM) != 1);
	
	//Clear TE bit to disable boud rate generator and TX pin.
	BIT_BAND_REG_CLR(USART6_BASE, USART_CR1, USART_CR1_TE_BIT_NUM);
}
