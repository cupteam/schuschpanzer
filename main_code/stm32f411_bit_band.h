/* Self-made definition of macroses and addresses for bit band */

#ifndef __STM32F411_BIT_BAND_H
#define __STM32F411_BIT_BAND_H

//Let's define some macros to use the bit-band access to peripheral registers.
//bit_word_addr = bit_band_base + (byte_offset x 32) + (bit_number � 4)
//For using this macro, it have to take care about (&(PPP->REG) - PERIPH_BASE)*32 + BIT_NUM*4) < 0x2000000, i.e. offset of alias word still is in the 32M alias area.
//But any peripheral register in AHB1, APB1, APB2 buses, except. AHB2 (USB) satisfy this condition in stm32f411xC.

#define BIT_BAND_REG_SET(PPP_BASE, REG, BIT_NUM)	(WRITE_REG((* (volatile uint32_t *)(PERIPH_BB_BASE + ((PPP_BASE) + (REG) - PERIPH_BASE)*32 + (BIT_NUM)*4)), 0x00000001))

#define BIT_BAND_REG_CLR(PPP_BASE, REG, BIT_NUM)	(WRITE_REG((* (volatile uint32_t *)(PERIPH_BB_BASE + ((PPP_BASE) + (REG) - PERIPH_BASE)*32 + (BIT_NUM)*4)), 0x00000000))

#define BIT_BAND_REG_READ(PPP_BASE, REG, BIT_NUM)	(READ_REG(* (volatile uint32_t *)(PERIPH_BB_BASE + ((PPP_BASE) + (REG) - PERIPH_BASE)*32 + (BIT_NUM)*4)))
		
//#define BIT_BAND_REG_READ(...) 
	
//#define BIT_BAND_RAM_SET(...) 
//#define BIT_BAND_RAM_CLR(...) 

/******************************************************************************/
/*                                                                            */
/*                            General Purpose I/O                             */
/*                                                                            */
/******************************************************************************/

/* GPIO struct registers offsets */
#define GPIO_MODER 		0x00   	 /*!< GPIO port mode register,               Address offset: 0x00      */
#define GPIO_OTYPER 	0x04   	 /*!< GPIO port output type register,        Address offset: 0x04      */
#define GPIO_OSPEEDR 	0x08  	 /*!< GPIO port output speed register,       Address offset: 0x08      */
#define GPIO_PUPDR 		0x0C  	 /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
#define GPIO_IDR 			0x10     /*!< GPIO port input data register,         Address offset: 0x10      */
#define GPIO_ODR 			0x14     /*!< GPIO port output data register,        Address offset: 0x14      */
#define GPIO_BSRR 		0x18   	 /*!< GPIO port bit set/reset register,      Address offset: 0x18      */
#define GPIO_LCKR 		0x1C     /*!< GPIO port configuration lock register, Address offset: 0x1C      */
#define GPIO_AFRL 		0x20 		 /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
#define GPIO_AFRH 		0x24  	 /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */


/******************  Bits definition for GPIO_OTYPER register  ****************/
#define GPIO_OTYPER_OT_0_BIT_NUM                     ((uint32_t)0)
#define GPIO_OTYPER_OT_1_BIT_NUM                     ((uint32_t)1)
#define GPIO_OTYPER_OT_2_BIT_NUM                     ((uint32_t)2)
#define GPIO_OTYPER_OT_3_BIT_NUM                     ((uint32_t)3)
#define GPIO_OTYPER_OT_4_BIT_NUM                     ((uint32_t)4)
#define GPIO_OTYPER_OT_5_BIT_NUM                     ((uint32_t)5)
#define GPIO_OTYPER_OT_6_BIT_NUM                     ((uint32_t)6)
#define GPIO_OTYPER_OT_7_BIT_NUM                     ((uint32_t)7)
#define GPIO_OTYPER_OT_8_BIT_NUM                     ((uint32_t)8)
#define GPIO_OTYPER_OT_9_BIT_NUM                     ((uint32_t)9)
#define GPIO_OTYPER_OT_10_BIT_NUM                    ((uint32_t)10)
#define GPIO_OTYPER_OT_11_BIT_NUM                    ((uint32_t)11)
#define GPIO_OTYPER_OT_12_BIT_NUM                    ((uint32_t)12)
#define GPIO_OTYPER_OT_13_BIT_NUM                    ((uint32_t)13)
#define GPIO_OTYPER_OT_14_BIT_NUM                    ((uint32_t)14)
#define GPIO_OTYPER_OT_15_BIT_NUM                    ((uint32_t)15)

/******************  Bits definition for GPIO_IDR register  *******************/
#define GPIO_IDR_IDR_0_BIT_NUM                       ((uint32_t)0)
#define GPIO_IDR_IDR_1_BIT_NUM                       ((uint32_t)1)
#define GPIO_IDR_IDR_2_BIT_NUM                       ((uint32_t)2)
#define GPIO_IDR_IDR_3_BIT_NUM                       ((uint32_t)3)
#define GPIO_IDR_IDR_4_BIT_NUM                       ((uint32_t)4)
#define GPIO_IDR_IDR_5_BIT_NUM                       ((uint32_t)5)
#define GPIO_IDR_IDR_6_BIT_NUM                       ((uint32_t)6)
#define GPIO_IDR_IDR_7_BIT_NUM                       ((uint32_t)7)
#define GPIO_IDR_IDR_8_BIT_NUM                       ((uint32_t)8)
#define GPIO_IDR_IDR_9_BIT_NUM                       ((uint32_t)9)
#define GPIO_IDR_IDR_10_BIT_NUM                      ((uint32_t)10)
#define GPIO_IDR_IDR_11_BIT_NUM                      ((uint32_t)11)
#define GPIO_IDR_IDR_12_BIT_NUM                      ((uint32_t)12)
#define GPIO_IDR_IDR_13_BIT_NUM                      ((uint32_t)13)
#define GPIO_IDR_IDR_14_BIT_NUM                      ((uint32_t)14)
#define GPIO_IDR_IDR_15_BIT_NUM                      ((uint32_t)15)

/******************  Bits definition for GPIO_ODR register  *******************/
#define GPIO_ODR_ODR_0_BIT_NUM                       ((uint32_t)0)
#define GPIO_ODR_ODR_1_BIT_NUM                       ((uint32_t)1)
#define GPIO_ODR_ODR_2_BIT_NUM                       ((uint32_t)2)
#define GPIO_ODR_ODR_3_BIT_NUM                       ((uint32_t)3)
#define GPIO_ODR_ODR_4_BIT_NUM                       ((uint32_t)4)
#define GPIO_ODR_ODR_5_BIT_NUM                       ((uint32_t)5)
#define GPIO_ODR_ODR_6_BIT_NUM                       ((uint32_t)6)
#define GPIO_ODR_ODR_7_BIT_NUM                       ((uint32_t)7)
#define GPIO_ODR_ODR_8_BIT_NUM                       ((uint32_t)8)
#define GPIO_ODR_ODR_9_BIT_NUM                       ((uint32_t)9)
#define GPIO_ODR_ODR_10_BIT_NUM                      ((uint32_t)10)
#define GPIO_ODR_ODR_11_BIT_NUM                      ((uint32_t)11)
#define GPIO_ODR_ODR_12_BIT_NUM                      ((uint32_t)12)
#define GPIO_ODR_ODR_13_BIT_NUM                      ((uint32_t)13)
#define GPIO_ODR_ODR_14_BIT_NUM                      ((uint32_t)14)
#define GPIO_ODR_ODR_15_BIT_NUM                      ((uint32_t)15)

/******************  Bits definition for GPIO_BSRR register  ******************/
#define GPIO_BSRR_BS_0_BIT_NUM                       ((uint32_t)0)
#define GPIO_BSRR_BS_1_BIT_NUM                       ((uint32_t)1)
#define GPIO_BSRR_BS_2_BIT_NUM                       ((uint32_t)2)
#define GPIO_BSRR_BS_3_BIT_NUM                       ((uint32_t)3)
#define GPIO_BSRR_BS_4_BIT_NUM                       ((uint32_t)4)
#define GPIO_BSRR_BS_5_BIT_NUM                       ((uint32_t)5)
#define GPIO_BSRR_BS_6_BIT_NUM                       ((uint32_t)6)
#define GPIO_BSRR_BS_7_BIT_NUM                       ((uint32_t)7)
#define GPIO_BSRR_BS_8_BIT_NUM                       ((uint32_t)8)
#define GPIO_BSRR_BS_9_BIT_NUM                       ((uint32_t)9)
#define GPIO_BSRR_BS_10_BIT_NUM                      ((uint32_t)10)
#define GPIO_BSRR_BS_11_BIT_NUM                      ((uint32_t)11)
#define GPIO_BSRR_BS_12_BIT_NUM                      ((uint32_t)12)
#define GPIO_BSRR_BS_13_BIT_NUM                      ((uint32_t)13)
#define GPIO_BSRR_BS_14_BIT_NUM                      ((uint32_t)14)
#define GPIO_BSRR_BS_15_BIT_NUM                      ((uint32_t)15)
#define GPIO_BSRR_BR_0_BIT_NUM                       ((uint32_t)16)
#define GPIO_BSRR_BR_1_BIT_NUM                       ((uint32_t)17)
#define GPIO_BSRR_BR_2_BIT_NUM                       ((uint32_t)18)
#define GPIO_BSRR_BR_3_BIT_NUM                       ((uint32_t)19)
#define GPIO_BSRR_BR_4_BIT_NUM                       ((uint32_t)20)
#define GPIO_BSRR_BR_5_BIT_NUM                       ((uint32_t)21)
#define GPIO_BSRR_BR_6_BIT_NUM                       ((uint32_t)22)
#define GPIO_BSRR_BR_7_BIT_NUM                       ((uint32_t)23)
#define GPIO_BSRR_BR_8_BIT_NUM                       ((uint32_t)24)
#define GPIO_BSRR_BR_9_BIT_NUM                       ((uint32_t)25)
#define GPIO_BSRR_BR_10_BIT_NUM                      ((uint32_t)26)
#define GPIO_BSRR_BR_11_BIT_NUM                      ((uint32_t)27)
#define GPIO_BSRR_BR_12_BIT_NUM                      ((uint32_t)28)
#define GPIO_BSRR_BR_13_BIT_NUM                      ((uint32_t)29)
#define GPIO_BSRR_BR_14_BIT_NUM                      ((uint32_t)30)
#define GPIO_BSRR_BR_15_BIT_NUM                      ((uint32_t)31)

/******************  Bits definition for GPIO_LCKR register  ******************/
#define	GPIO_LCKR_LCK_0_BIT_NUM											 ((uint32_t)0)
#define	GPIO_LCKR_LCK_1_BIT_NUM											 ((uint32_t)1)
#define	GPIO_LCKR_LCK_2_BIT_NUM											 ((uint32_t)2)
#define	GPIO_LCKR_LCK_3_BIT_NUM											 ((uint32_t)3)
#define	GPIO_LCKR_LCK_4_BIT_NUM											 ((uint32_t)4)
#define	GPIO_LCKR_LCK_5_BIT_NUM											 ((uint32_t)5)
#define	GPIO_LCKR_LCK_6_BIT_NUM											 ((uint32_t)6)
#define	GPIO_LCKR_LCK_7_BIT_NUM											 ((uint32_t)7)
#define	GPIO_LCKR_LCK_8_BIT_NUM											 ((uint32_t)8)
#define	GPIO_LCKR_LCK_9_BIT_NUM											 ((uint32_t)9)
#define	GPIO_LCKR_LCK_10_BIT_NUM										 ((uint32_t)10)
#define	GPIO_LCKR_LCK_11_BIT_NUM										 ((uint32_t)11)
#define	GPIO_LCKR_LCK_12_BIT_NUM										 ((uint32_t)12)
#define	GPIO_LCKR_LCK_13_BIT_NUM										 ((uint32_t)13)
#define	GPIO_LCKR_LCK_14_BIT_NUM										 ((uint32_t)14)
#define	GPIO_LCKR_LCK_15_BIT_NUM										 ((uint32_t)15)
#define	GPIO_LCKR_LCKK_BIT_NUM											 ((uint32_t)16)


/******************************************************************************/
/*                                                                            */
/*                    External Interrupt/Event Controller                     */
/*                                                                            */
/******************************************************************************/

/* EXTI_ struct registers offsets */
  #define EXTI_IMR 			0x00    /*!< EXTI Interrupt mask register,            Address offset: 0x00 */
  #define EXTI_EMR 			0x04    /*!< EXTI Event mask register,                Address offset: 0x04 */
  #define EXTI_RTSR 		0x08  	/*!< EXTI Rising trigger selection register,  Address offset: 0x08 */
  #define EXTI_FTSR 		0x0C   	/*!< EXTI Falling trigger selection register, Address offset: 0x0C */
  #define EXTI_SWIER 		0x10  	/*!< EXTI Software interrupt event register,  Address offset: 0x10 */
  #define EXTI_PR 			0x14    /*!< EXTI Pending register,                   Address offset: 0x14 */

/*******************  Bit definition for EXTI_IMR register  *******************/
#define  EXTI_IMR_LINE0_BIT_NUM                        ((uint32_t)0)        /*!< Interrupt Mask on line 0 */
#define  EXTI_IMR_LINE1_BIT_NUM                        ((uint32_t)1)        /*!< Interrupt Mask on line 1 */
#define  EXTI_IMR_LINE2_BIT_NUM                        ((uint32_t)2)        /*!< Interrupt Mask on line 2 */
#define  EXTI_IMR_LINE3_BIT_NUM                        ((uint32_t)3)        /*!< Interrupt Mask on line 3 */
#define  EXTI_IMR_LINE4_BIT_NUM                        ((uint32_t)4)        /*!< Interrupt Mask on line 4 */
#define  EXTI_IMR_LINE5_BIT_NUM                        ((uint32_t)5)        /*!< Interrupt Mask on line 5 */
#define  EXTI_IMR_LINE6_BIT_NUM                        ((uint32_t)6)        /*!< Interrupt Mask on line 6 */
#define  EXTI_IMR_LINE7_BIT_NUM                        ((uint32_t)7)        /*!< Interrupt Mask on line 7 */
#define  EXTI_IMR_LINE8_BIT_NUM                        ((uint32_t)8)        /*!< Interrupt Mask on line 8 */
#define  EXTI_IMR_LINE9_BIT_NUM                        ((uint32_t)9)        /*!< Interrupt Mask on line 9 */
#define  EXTI_IMR_LINE10_BIT_NUM                       ((uint32_t)10)        /*!< Interrupt Mask on line 10 */
#define  EXTI_IMR_LINE11_BIT_NUM                       ((uint32_t)11)        /*!< Interrupt Mask on line 11 */
#define  EXTI_IMR_LINE12_BIT_NUM                       ((uint32_t)12)        /*!< Interrupt Mask on line 12 */
#define  EXTI_IMR_LINE13_BIT_NUM                       ((uint32_t)13)        /*!< Interrupt Mask on line 13 */
#define  EXTI_IMR_LINE14_BIT_NUM                       ((uint32_t)14)        /*!< Interrupt Mask on line 14 */
#define  EXTI_IMR_LINE15_BIT_NUM                       ((uint32_t)15)        /*!< Interrupt Mask on line 15 */
#define  EXTI_IMR_LINE16_BIT_NUM                       ((uint32_t)16)        /*!< Interrupt Mask on line 16 */
#define  EXTI_IMR_LINE17_BIT_NUM                       ((uint32_t)17)        /*!< Interrupt Mask on line 17 */
#define  EXTI_IMR_LINE18_BIT_NUM                       ((uint32_t)18)        /*!< Interrupt Mask on line 18 */
																																					 /*!< Reserved line 19 */
																																					 /*!< Reserved line 20 */
#define  EXTI_IMR_LINE21_BIT_NUM                       ((uint32_t)21)        /*!< Interrupt Mask on line 21 */
#define  EXTI_IMR_LINE22_BIT_NUM                       ((uint32_t)22)        /*!< Interrupt Mask on line 22 */

/*******************  Bit definition for EXTI_EMR register  *******************/
#define  EXTI_EMR_LINE0_BIT_NUM                        ((uint32_t)0)         /*!< Event Mask on line 0 */
#define  EXTI_EMR_LINE1_BIT_NUM                        ((uint32_t)1)         /*!< Event Mask on line 1 */
#define  EXTI_EMR_LINE2_BIT_NUM                        ((uint32_t)2)         /*!< Event Mask on line 2 */
#define  EXTI_EMR_LINE3_BIT_NUM                        ((uint32_t)3)         /*!< Event Mask on line 3 */
#define  EXTI_EMR_LINE4_BIT_NUM                        ((uint32_t)4)         /*!< Event Mask on line 4 */
#define  EXTI_EMR_LINE5_BIT_NUM                        ((uint32_t)5)         /*!< Event Mask on line 5 */
#define  EXTI_EMR_LINE6_BIT_NUM                        ((uint32_t)6)         /*!< Event Mask on line 6 */
#define  EXTI_EMR_LINE7_BIT_NUM                        ((uint32_t)7)         /*!< Event Mask on line 7 */
#define  EXTI_EMR_LINE8_BIT_NUM                        ((uint32_t)8)         /*!< Event Mask on line 8 */
#define  EXTI_EMR_LINE9_BIT_NUM                        ((uint32_t)9)         /*!< Event Mask on line 9 */
#define  EXTI_EMR_LINE10_BIT_NUM                       ((uint32_t)10)        /*!< Event Mask on line 10 */
#define  EXTI_EMR_LINE11_BIT_NUM                       ((uint32_t)11)        /*!< Event Mask on line 11 */
#define  EXTI_EMR_LINE12_BIT_NUM                       ((uint32_t)12)        /*!< Event Mask on line 12 */
#define  EXTI_EMR_LINE13_BIT_NUM                       ((uint32_t)13)        /*!< Event Mask on line 13 */
#define  EXTI_EMR_LINE14_BIT_NUM                       ((uint32_t)14)        /*!< Event Mask on line 14 */
#define  EXTI_EMR_LINE15_BIT_NUM                       ((uint32_t)15)        /*!< Event Mask on line 15 */
#define  EXTI_EMR_LINE16_BIT_NUM                       ((uint32_t)16)        /*!< Event Mask on line 16 */
#define  EXTI_EMR_LINE17_BIT_NUM                       ((uint32_t)17)        /*!< Event Mask on line 17 */
#define  EXTI_EMR_LINE18_BIT_NUM                       ((uint32_t)18)        /*!< Event Mask on line 18 */
																																						 /*!< Reserved line 19 */
																																					   /*!< Reserved line 20 */
#define  EXTI_EMR_LINE21_BIT_NUM                       ((uint32_t)21)        /*!< Event Mask on line 21 */
#define  EXTI_EMR_LINE22_BIT_NUM                       ((uint32_t)22)        /*!< Event Mask on line 22 */

/******************  Bit definition for EXTI_RTSR register  *******************/
#define  EXTI_RTSR_LINE0_BIT_NUM                       ((uint32_t)0)         /*!< Rising trigger event configuration bit of line 0 */
#define  EXTI_RTSR_LINE1_BIT_NUM                       ((uint32_t)1)         /*!< Rising trigger event configuration bit of line 1 */
#define  EXTI_RTSR_LINE2_BIT_NUM                       ((uint32_t)2)         /*!< Rising trigger event configuration bit of line 2 */
#define  EXTI_RTSR_LINE3_BIT_NUM                       ((uint32_t)3)         /*!< Rising trigger event configuration bit of line 3 */
#define  EXTI_RTSR_LINE4_BIT_NUM                       ((uint32_t)4)         /*!< Rising trigger event configuration bit of line 4 */
#define  EXTI_RTSR_LINE5_BIT_NUM                       ((uint32_t)5)         /*!< Rising trigger event configuration bit of line 5 */
#define  EXTI_RTSR_LINE6_BIT_NUM                       ((uint32_t)6)         /*!< Rising trigger event configuration bit of line 6 */
#define  EXTI_RTSR_LINE7_BIT_NUM                       ((uint32_t)7)         /*!< Rising trigger event configuration bit of line 7 */
#define  EXTI_RTSR_LINE8_BIT_NUM                       ((uint32_t)8)         /*!< Rising trigger event configuration bit of line 8 */
#define  EXTI_RTSR_LINE9_BIT_NUM                       ((uint32_t)9)         /*!< Rising trigger event configuration bit of line 9 */
#define  EXTI_RTSR_LINE10_BIT_NUM                      ((uint32_t)10)        /*!< Rising trigger event configuration bit of line 10 */
#define  EXTI_RTSR_LINE11_BIT_NUM                      ((uint32_t)11)        /*!< Rising trigger event configuration bit of line 11 */
#define  EXTI_RTSR_LINE12_BIT_NUM                      ((uint32_t)12)        /*!< Rising trigger event configuration bit of line 12 */
#define  EXTI_RTSR_LINE13_BIT_NUM                      ((uint32_t)13)        /*!< Rising trigger event configuration bit of line 13 */
#define  EXTI_RTSR_LINE14_BIT_NUM                      ((uint32_t)14)        /*!< Rising trigger event configuration bit of line 14 */
#define  EXTI_RTSR_LINE15_BIT_NUM                      ((uint32_t)15)        /*!< Rising trigger event configuration bit of line 15 */
#define  EXTI_RTSR_LINE16_BIT_NUM                      ((uint32_t)16)        /*!< Rising trigger event configuration bit of line 16 */
#define  EXTI_RTSR_LINE17_BIT_NUM                      ((uint32_t)17)        /*!< Rising trigger event configuration bit of line 17 */
#define  EXTI_RTSR_LINE18_BIT_NUM                      ((uint32_t)18)        /*!< Rising trigger event configuration bit of line 18 */
																																						 /*!< Reserved line 19 */
																																					   /*!< Reserved line 20 */
#define  EXTI_RTSR_LINE21_BIT_NUM                      ((uint32_t)21)        /*!< Rising trigger event configuration bit of line 21 */
#define  EXTI_RTSR_LINE22_BIT_NUM                      ((uint32_t)22)        /*!< Rising trigger event configuration bit of line 22 */

/******************  Bit definition for EXTI_FTSR register  *******************/
#define  EXTI_FTSR_LINE0_BIT_NUM                       ((uint32_t)0)         /*!< Falling trigger event configuration bit of line 0 */
#define  EXTI_FTSR_LINE1_BIT_NUM                       ((uint32_t)1)         /*!< Falling trigger event configuration bit of line 1 */
#define  EXTI_FTSR_LINE2_BIT_NUM                       ((uint32_t)2)         /*!< Falling trigger event configuration bit of line 2 */
#define  EXTI_FTSR_LINE3_BIT_NUM                       ((uint32_t)3)         /*!< Falling trigger event configuration bit of line 3 */
#define  EXTI_FTSR_LINE4_BIT_NUM                       ((uint32_t)4)         /*!< Falling trigger event configuration bit of line 4 */
#define  EXTI_FTSR_LINE5_BIT_NUM                       ((uint32_t)5)         /*!< Falling trigger event configuration bit of line 5 */
#define  EXTI_FTSR_LINE6_BIT_NUM                       ((uint32_t)6)         /*!< Falling trigger event configuration bit of line 6 */
#define  EXTI_FTSR_LINE7_BIT_NUM                       ((uint32_t)7)         /*!< Falling trigger event configuration bit of line 7 */
#define  EXTI_FTSR_LINE8_BIT_NUM                       ((uint32_t)8)         /*!< Falling trigger event configuration bit of line 8 */
#define  EXTI_FTSR_LINE9_BIT_NUM                       ((uint32_t)9)         /*!< Falling trigger event configuration bit of line 9 */
#define  EXTI_FTSR_LINE10_BIT_NUM                      ((uint32_t)10)        /*!< Falling trigger event configuration bit of line 10 */
#define  EXTI_FTSR_LINE11_BIT_NUM                      ((uint32_t)11)        /*!< Falling trigger event configuration bit of line 11 */
#define  EXTI_FTSR_LINE12_BIT_NUM                      ((uint32_t)12)        /*!< Falling trigger event configuration bit of line 12 */
#define  EXTI_FTSR_LINE13_BIT_NUM                      ((uint32_t)13)        /*!< Falling trigger event configuration bit of line 13 */
#define  EXTI_FTSR_LINE14_BIT_NUM                      ((uint32_t)14)        /*!< Falling trigger event configuration bit of line 14 */
#define  EXTI_FTSR_LINE15_BIT_NUM                      ((uint32_t)15)        /*!< Falling trigger event configuration bit of line 15 */
#define  EXTI_FTSR_LINE16_BIT_NUM                      ((uint32_t)16)        /*!< Falling trigger event configuration bit of line 16 */
#define  EXTI_FTSR_LINE17_BIT_NUM                      ((uint32_t)17)        /*!< Falling trigger event configuration bit of line 17 */
#define  EXTI_FTSR_LINE18_BIT_NUM                      ((uint32_t)18)        /*!< Falling trigger event configuration bit of line 18 */
																																						 /*!< Reserved line 19 */
																																					   /*!< Reserved line 20 */
#define  EXTI_FTSR_LINE21_BIT_NUM                      ((uint32_t)21)        /*!< Falling trigger event configuration bit of line 21 */
#define  EXTI_FTSR_LINE22_BIT_NUM                      ((uint32_t)22)        /*!< Falling trigger event configuration bit of line 22 */

/******************  Bit definition for EXTI_SWIER register  ******************/
#define  EXTI_SWIER_LINE0_BIT_NUM                        ((uint32_t)0)         /*!< Software Interrupt on line 0 */
#define  EXTI_SWIER_LINE1_BIT_NUM                        ((uint32_t)1)         /*!< Software Interrupt on line 1 */
#define  EXTI_SWIER_LINE2_BIT_NUM                        ((uint32_t)2)         /*!< Software Interrupt on line 2 */
#define  EXTI_SWIER_LINE3_BIT_NUM                        ((uint32_t)3)         /*!< Software Interrupt on line 3 */
#define  EXTI_SWIER_LINE4_BIT_NUM                        ((uint32_t)4)         /*!< Software Interrupt on line 4 */
#define  EXTI_SWIER_LINE5_BIT_NUM                        ((uint32_t)5)         /*!< Software Interrupt on line 5 */
#define  EXTI_SWIER_LINE6_BIT_NUM                        ((uint32_t)6)         /*!< Software Interrupt on line 6 */
#define  EXTI_SWIER_LINE7_BIT_NUM                        ((uint32_t)7)         /*!< Software Interrupt on line 7 */
#define  EXTI_SWIER_LINE8_BIT_NUM                        ((uint32_t)8)         /*!< Software Interrupt on line 8 */
#define  EXTI_SWIER_LINE9_BIT_NUM                        ((uint32_t)9)         /*!< Software Interrupt on line 9 */
#define  EXTI_SWIER_LINE10_BIT_NUM                       ((uint32_t)10)        /*!< Software Interrupt on line 10 */
#define  EXTI_SWIER_LINE11_BIT_NUM                       ((uint32_t)11)        /*!< Software Interrupt on line 11 */
#define  EXTI_SWIER_LINE12_BIT_NUM                       ((uint32_t)12)        /*!< Software Interrupt on line 12 */
#define  EXTI_SWIER_LINE13_BIT_NUM                       ((uint32_t)13)        /*!< Software Interrupt on line 13 */
#define  EXTI_SWIER_LINE14_BIT_NUM                       ((uint32_t)14)        /*!< Software Interrupt on line 14 */
#define  EXTI_SWIER_LINE15_BIT_NUM                       ((uint32_t)15)        /*!< Software Interrupt on line 15 */
#define  EXTI_SWIER_LINE16_BIT_NUM                       ((uint32_t)16)        /*!< Software Interrupt on line 16 */
#define  EXTI_SWIER_LINE17_BIT_NUM                       ((uint32_t)17)        /*!< Software Interrupt on line 17 */
#define  EXTI_SWIER_LINE18_BIT_NUM                       ((uint32_t)18)        /*!< Software Interrupt on line 18 */
																																							 /*!< Reserved line 19 */
																																							 /*!< Reserved line 20 */
#define  EXTI_SWIER_LINE21_BIT_NUM                       ((uint32_t)21)        /*!< Software Interrupt on line 21 */
#define  EXTI_SWIER_LINE22_BIT_NUM                       ((uint32_t)22)        /*!< Software Interrupt on line 22 */


/*******************  Bit definition for EXTI_PR register  ********************/
#define  EXTI_PR_LINE0_BIT_NUM                        ((uint32_t)0)         /*!< Pending bit for line 0 */
#define  EXTI_PR_LINE1_BIT_NUM                        ((uint32_t)1)         /*!< Pending bit for line 1 */
#define  EXTI_PR_LINE2_BIT_NUM                        ((uint32_t)2)         /*!< Pending bit for line 2 */
#define  EXTI_PR_LINE3_BIT_NUM                        ((uint32_t)3)         /*!< Pending bit for line 3 */
#define  EXTI_PR_LINE4_BIT_NUM                        ((uint32_t)4)         /*!< Pending bit for line 4 */
#define  EXTI_PR_LINE5_BIT_NUM                        ((uint32_t)5)         /*!< Pending bit for line 5 */
#define  EXTI_PR_LINE6_BIT_NUM                        ((uint32_t)6)         /*!< Pending bit for line 6 */
#define  EXTI_PR_LINE7_BIT_NUM                        ((uint32_t)7)         /*!< Pending bit for line 7 */
#define  EXTI_PR_LINE8_BIT_NUM                        ((uint32_t)8)         /*!< Pending bit for line 8 */
#define  EXTI_PR_LINE9_BIT_NUM                        ((uint32_t)9)         /*!< Pending bit for line 9 */
#define  EXTI_PR_LINE10_BIT_NUM                       ((uint32_t)10)        /*!< Pending bit for line 10 */
#define  EXTI_PR_LINE11_BIT_NUM                       ((uint32_t)11)        /*!< Pending bit for line 11 */
#define  EXTI_PR_LINE12_BIT_NUM                       ((uint32_t)12)        /*!< Pending bit for line 12 */
#define  EXTI_PR_LINE13_BIT_NUM                       ((uint32_t)13)        /*!< Pending bit for line 13 */
#define  EXTI_PR_LINE14_BIT_NUM                       ((uint32_t)14)        /*!< Pending bit for line 14 */
#define  EXTI_PR_LINE15_BIT_NUM                       ((uint32_t)15)        /*!< Pending bit for line 15 */
#define  EXTI_PR_LINE16_BIT_NUM                       ((uint32_t)16)        /*!< Pending bit for line 16 */
#define  EXTI_PR_LINE17_BIT_NUM                       ((uint32_t)17)        /*!< Pending bit for line 17 */
#define  EXTI_PR_LINE18_BIT_NUM                       ((uint32_t)18)        /*!< Pending bit for line 18 */
																																						/*!< Reserved line 19 */
																																					  /*!< Reserved line 20 */
#define  EXTI_PR_LINE21_BIT_NUM                       ((uint32_t)21)        /*!< Pending bit for line 21 */
#define  EXTI_PR_LINE22_BIT_NUM                       ((uint32_t)22)        /*!< Pending bit for line 22 */


/******************************************************************************/
/*                                                                            */
/*         Universal Synchronous Asynchronous Receiver Transmitter            */
/*                                                                            */
/******************************************************************************/

/* USART struct registers offsets */
  #define USART_SR    0x00     /*!< USART Status register,                   Address offset: 0x00 */
  #define USART_DR    0x04     /*!< USART Data register,                     Address offset: 0x04 */
  #define USART_BRR   0x08     /*!< USART Baud rate register,                Address offset: 0x08 */
  #define USART_CR1   0x0C     /*!< USART Control register 1,                Address offset: 0x0C */
  #define USART_CR2   0x10     /*!< USART Control register 2,                Address offset: 0x10 */
  #define USART_CR3   0x14     /*!< USART Control register 3,                Address offset: 0x14 */
  #define USART_GTPR  0x18     /*!< USART Guard time and prescaler register, Address offset: 0x18 */

/*******************  Bit definition for USART_SR register  *******************/
#define  USART_SR_PE_BIT_NUM                         ((uint32_t)0)            /*!<Parity Error                 */
#define  USART_SR_FE_BIT_NUM                         ((uint32_t)1)            /*!<Framing Error                */
#define  USART_SR_NF_BIT_NUM                         ((uint32_t)2)            /*!<Noise Error Flag             */
#define  USART_SR_ORE_BIT_NUM                        ((uint32_t)3)            /*!<OverRun Error                */
#define  USART_SR_IDLE_BIT_NUM                       ((uint32_t)4)            /*!<IDLE line detected           */
#define  USART_SR_RXNE_BIT_NUM                       ((uint32_t)5)            /*!<Read Data Register Not Empty */
#define  USART_SR_TC_BIT_NUM                         ((uint32_t)6)            /*!<Transmission Complete        */
#define  USART_SR_TXE_BIT_NUM                        ((uint32_t)7)            /*!<Transmit Data Register Empty */
#define  USART_SR_LBD_BIT_NUM                        ((uint32_t)8)            /*!<LIN Break Detection Flag     */
#define  USART_SR_CTS_BIT_NUM                        ((uint32_t)9)            /*!<CTS Flag                     */

/*******************  Bit definition for USART_DR register  *******************/

/******************  Bit definition for USART_BRR register  *******************/

/******************  Bit definition for USART_CR1 register  *******************/
#define  USART_CR1_SBK_BIT_NUM                       ((uint32_t)0)            /*!<Send Break                             */
#define  USART_CR1_RWU_BIT_NUM                       ((uint32_t)1)            /*!<Receiver wakeup                        */
#define  USART_CR1_RE_BIT_NUM                        ((uint32_t)2)            /*!<Receiver Enable                        */
#define  USART_CR1_TE_BIT_NUM                        ((uint32_t)3)            /*!<Transmitter Enable                     */
#define  USART_CR1_IDLEIE_BIT_NUM                    ((uint32_t)4)            /*!<IDLE Interrupt Enable                  */
#define  USART_CR1_RXNEIE_BIT_NUM                    ((uint32_t)5)            /*!<RXNE Interrupt Enable                  */
#define  USART_CR1_TCIE_BIT_NUM                      ((uint32_t)6)            /*!<Transmission Complete Interrupt Enable */
#define  USART_CR1_TXEIE_BIT_NUM                     ((uint32_t)7)            /*!<PE Interrupt Enable                    */
#define  USART_CR1_PEIE_BIT_NUM                      ((uint32_t)8)            /*!<PE Interrupt Enable                    */
#define  USART_CR1_PS_BIT_NUM                        ((uint32_t)9)            /*!<Parity Selection                       */
#define  USART_CR1_PCE_BIT_NUM                       ((uint32_t)10)           /*!<Parity Control Enable                  */
#define  USART_CR1_WAKE_BIT_NUM                      ((uint32_t)11)           /*!<Wakeup method                          */
#define  USART_CR1_M_BIT_NUM                         ((uint32_t)12)           /*!<Word length                            */
#define  USART_CR1_UE_BIT_NUM                        ((uint32_t)13)           /*!<USART Enable                           */
//Bit 14 - Reserved.
#define  USART_CR1_OVER8_BIT_NUM                     ((uint32_t)15)           /*!<USART Oversampling by 8 enable         */

/******************  Bit definition for USART_CR2 register  *******************/
	/*!<Address of the USART node ADD[3:0]   */
//Bit 4 - Reserved.																																					 
#define  USART_CR2_LBDL_BIT_NUM                      ((uint32_t)5)            /*!<LIN Break Detection Length           */
#define  USART_CR2_LBDIE_BIT_NUM                     ((uint32_t)6)            /*!<LIN Break Detection Interrupt Enable */
//Bit 7 - Reserved.
#define  USART_CR2_LBCL_BIT_NUM                      ((uint32_t)8)            /*!<Last Bit Clock pulse                 */
#define  USART_CR2_CPHA_BIT_NUM                      ((uint32_t)9)            /*!<Clock Phase                          */
#define  USART_CR2_CPOL_BIT_NUM                      ((uint32_t)10)           /*!<Clock Polarity                       */
#define  USART_CR2_CLKEN_BIT_NUM                     ((uint32_t)11)           /*!<Clock Enable                         */
//Bits 12,13 - STOP [1:0]
#define  USART_CR2_LINEN_BIT_NUM                     ((uint32_t)14)           /*!<LIN mode enable */

/******************  Bit definition for USART_CR3 register  *******************/
#define  USART_CR3_EIE_BIT_NUM                       ((uint32_t)0)            /*!<Error Interrupt Enable      */
#define  USART_CR3_IREN_BIT_NUM                      ((uint32_t)1)            /*!<IrDA mode Enable            */
#define  USART_CR3_IRLP_BIT_NUM                      ((uint32_t)2)            /*!<IrDA Low-Power              */
#define  USART_CR3_HDSEL_BIT_NUM                     ((uint32_t)3)            /*!<Half-Duplex Selection       */
#define  USART_CR3_NACK_BIT_NUM                      ((uint32_t)4)            /*!<Smartcard NACK enable       */
#define  USART_CR3_SCEN_BIT_NUM                      ((uint32_t)5)            /*!<Smartcard mode enable       */
#define  USART_CR3_DMAR_BIT_NUM                      ((uint32_t)6)            /*!<DMA Enable Receiver         */
#define  USART_CR3_DMAT_BIT_NUM                      ((uint32_t)7)            /*!<DMA Enable Transmitter      */
#define  USART_CR3_RTSE_BIT_NUM                      ((uint32_t)8)            /*!<RTS Enable                  */
#define  USART_CR3_CTSE_BIT_NUM                      ((uint32_t)9)            /*!<CTS Enable                  */
#define  USART_CR3_CTSIE_BIT_NUM                     ((uint32_t)10)           /*!<CTS Interrupt Enable        */
#define  USART_CR3_ONEBIT_BIT_NUM                    ((uint32_t)11)           /*!<USART One bit method enable */

/******************  Bit definition for USART_GTPR register  ******************/


/******************************************************************************/
/*                                                                            */
/*                        Serial Peripheral Interface                         */
/*                                                                            */
/******************************************************************************/

/* SPI struct registers offsets */
#define SPI_CR1				0x00        /*!< SPI control register 1 (not used in I2S mode),      Address offset: 0x00 */
#define SPI_CR2				0x04        /*!< SPI control register 2,                             Address offset: 0x04 */
#define SPI_SR				0x08        /*!< SPI status register,                                Address offset: 0x08 */
#define SPI_DR				0x0C        /*!< SPI data register,                                  Address offset: 0x0C */
#define SPI_CRCPR			0x10      	/*!< SPI CRC polynomial register (not used in I2S mode), Address offset: 0x10 */
#define SPI_RXCRCR		0x14     		/*!< SPI RX CRC register (not used in I2S mode),         Address offset: 0x14 */
#define SPI_TXCRCR		0x18     		/*!< SPI TX CRC register (not used in I2S mode),         Address offset: 0x18 */
#define SPI_I2SCFGR		0x1C    		/*!< SPI_I2S configuration register,                     Address offset: 0x1C */
#define SPI_I2SPR			0x20     		/*!< SPI_I2S prescaler register,                         Address offset: 0x20 */


/*******************  Bit definition for SPI_CR1 register  ********************/
#define  SPI_CR1_CPHA_BIT_NUM                        ((uint32_t)0)            /*!<Clock Phase      */
#define  SPI_CR1_CPOL_BIT_NUM                        ((uint32_t)1)            /*!<Clock Polarity   */
#define  SPI_CR1_MSTR_BIT_NUM                        ((uint32_t)2)            /*!<Master Selection */

																																			/*!<BR[2:0] bits (Baud Rate Control) */

#define  SPI_CR1_SPE_BIT_NUM                         ((uint32_t)6)            /*!<SPI Enable                          */
#define  SPI_CR1_LSBFIRST_BIT_NUM                    ((uint32_t)7)            /*!<Frame Format                        */
#define  SPI_CR1_SSI_BIT_NUM                         ((uint32_t)8)            /*!<Internal slave select               */
#define  SPI_CR1_SSM_BIT_NUM                         ((uint32_t)9)            /*!<Software slave management           */
#define  SPI_CR1_RXONLY_BIT_NUM                      ((uint32_t)10)           /*!<Receive only                        */
#define  SPI_CR1_DFF_BIT_NUM                         ((uint32_t)11)           /*!<Data Frame Format                   */
#define  SPI_CR1_CRCNEXT_BIT_NUM                     ((uint32_t)12)           /*!<Transmit CRC next                   */
#define  SPI_CR1_CRCEN_BIT_NUM                       ((uint32_t)13)           /*!<Hardware CRC calculation enable     */
#define  SPI_CR1_BIDIOE_BIT_NUM                      ((uint32_t)14)           /*!<Output enable in bidirectional mode */
#define  SPI_CR1_BIDIMODE_BIT_NUM                    ((uint32_t)15)           /*!<Bidirectional data mode enable      */

/*******************  Bit definition for SPI_CR2 register  ********************/
#define  SPI_CR2_RXDMAEN_BIT_NUM                     ((uint32_t)0)            /*!<Rx Buffer DMA Enable                 */
#define  SPI_CR2_TXDMAEN_BIT_NUM                     ((uint32_t)1)            /*!<Tx Buffer DMA Enable                 */
#define  SPI_CR2_SSOE_BIT_NUM                        ((uint32_t)2)            /*!<SS Output Enable                     */
																																							/* Bit 3 - Reserved 	*/
#define	 SPI_CR2_FRF_BIT_NUM												 ((uint32_t)4)            /*!<Frame format               		      */
#define  SPI_CR2_ERRIE_BIT_NUM                       ((uint32_t)5)            /*!<Error Interrupt Enable               */
#define  SPI_CR2_RXNEIE_BIT_NUM                      ((uint32_t)6)            /*!<RX buffer Not Empty Interrupt Enable */
#define  SPI_CR2_TXEIE_BIT_NUM                       ((uint32_t)7)            /*!<Tx buffer Empty Interrupt Enable     */

/********************  Bit definition for SPI_SR register  ********************/
#define  SPI_SR_RXNE_BIT_NUM                         ((uint32_t)0)               /*!<Receive buffer Not Empty */
#define  SPI_SR_TXE_BIT_NUM                          ((uint32_t)1)               /*!<Transmit buffer Empty    */
#define  SPI_SR_CHSIDE_BIT_NUM                       ((uint32_t)2)               /*!<Channel side             */
#define  SPI_SR_UDR_BIT_NUM                          ((uint32_t)3)               /*!<Underrun flag            */
#define  SPI_SR_CRCERR_BIT_NUM                       ((uint32_t)4)               /*!<CRC Error flag           */
#define  SPI_SR_MODF_BIT_NUM                         ((uint32_t)5)               /*!<Mode fault               */
#define  SPI_SR_OVR_BIT_NUM                          ((uint32_t)6)               /*!<Overrun flag             */
#define  SPI_SR_BSY_BIT_NUM                          ((uint32_t)7)               /*!<Busy flag                */
#define  SPI_SR_FRE_BIT_NUM                          ((uint32_t)8)               /*!<Frame format error       */

/********************  Bit definition for SPI_DR register  ********************/

/*******************  Bit definition for SPI_CRCPR register  ******************/

/******************  Bit definition for SPI_RXCRCR register  ******************/

/******************  Bit definition for SPI_TXCRCR register  ******************/

/******************  Bit definition for SPI_I2SCFGR register  *****************/
#define  SPI_I2SCFGR_CHLEN_BIT_NUM                   ((uint32_t)0)            /*!<Channel length (number of bits per audio channel) */

																																			/*!<DATLEN[1:0] bits (Data length to be transferred)  */
#define  SPI_I2SCFGR_DATLEN_0_BIT_NUM                ((uint32_t)1)            	/*!<Bit 0 */
#define  SPI_I2SCFGR_DATLEN_1_BIT_NUM                ((uint32_t)2)            	/*!<Bit 1 */

#define  SPI_I2SCFGR_CKPOL_BIT_NUM                   ((uint32_t)3)            /*!<steady state clock polarity               */

																																			/*!<I2SSTD[1:0] bits (I2S standard selection) */
#define  SPI_I2SCFGR_I2SSTD_0_BIT_NUM                ((uint32_t)4)          	  /*!<Bit 0 */
#define  SPI_I2SCFGR_I2SSTD_1_BIT_NUM                ((uint32_t)5)          	  /*!<Bit 1 */

#define  SPI_I2SCFGR_PCMSYNC_BIT_NUM                 ((uint32_t)7)            /*!<PCM frame synchronization                 */

																																			/*!<I2SCFG[1:0] bits (I2S configuration mode) */
#define  SPI_I2SCFGR_I2SCFG_0_BIT_NUM                ((uint32_t)8)           	 	/*!<Bit 0 */
#define  SPI_I2SCFGR_I2SCFG_1_BIT_NUM                ((uint32_t)9)            	/*!<Bit 1 */

#define  SPI_I2SCFGR_I2SE_BIT_NUM                    ((uint32_t)10)            /*!<I2S Enable         */
#define  SPI_I2SCFGR_I2SMOD_BIT_NUM                  ((uint32_t)11)            /*!<I2S mode selection */


/******************  Bit definition for SPI_I2SPR register  *******************/
																																							/*!<I2S Linear prescaler         */
#define  SPI_I2SPR_ODD_BIT_NUM                       ((uint32_t)8)            /*!<Odd factor for the prescaler */
#define  SPI_I2SPR_MCKOE_BIT_NUM                     ((uint32_t)9)            /*!<Master Clock Output Enable   */





/******************************************************************************/
/*                                                                            */
/*                                    TIM                                     */
/*                                                                            */
/******************************************************************************/

/* TIM struct registers offsets */
#define TIM_CR1			0x00       /*!< TIM control register 1,              Address offset: 0x00 */
#define TIM_CR2			0x04       /*!< TIM control register 2,              Address offset: 0x04 */
#define TIM_SMCR		0x08       /*!< TIM slave mode control register,     Address offset: 0x08 */
#define TIM_DIER		0x0C       /*!< TIM DMA/interrupt enable register,   Address offset: 0x0C */
#define TIM_SR			0x10       /*!< TIM status register,                 Address offset: 0x10 */
#define TIM_EGR			0x14       /*!< TIM event generation register,       Address offset: 0x14 */
#define TIM_CCMR1		0x18       /*!< TIM capture/compare mode register 1, Address offset: 0x18 */
#define TIM_CCMR2		0x1C       /*!< TIM capture/compare mode register 2, Address offset: 0x1C */
#define TIM_CCER		0x20       /*!< TIM capture/compare enable register, Address offset: 0x20 */
#define TIM_CNT			0x24       /*!< TIM counter register,                Address offset: 0x24 */
#define TIM_PSC			0x28       /*!< TIM prescaler,                       Address offset: 0x28 */
#define TIM_ARR			0x2C       /*!< TIM auto-reload register,            Address offset: 0x2C */
#define TIM_RCR			0x30       /*!< TIM repetition counter register,     Address offset: 0x30 */
#define TIM_CCR1		0x34       /*!< TIM capture/compare register 1,      Address offset: 0x34 */
#define TIM_CCR2		0x38       /*!< TIM capture/compare register 2,      Address offset: 0x38 */
#define TIM_CCR3		0x3C       /*!< TIM capture/compare register 3,      Address offset: 0x3C */
#define TIM_CCR4		0x40       /*!< TIM capture/compare register 4,      Address offset: 0x40 */
#define TIM_BDTR		0x44       /*!< TIM break and dead-time register,    Address offset: 0x44 */
#define TIM_DCR			0x48       /*!< TIM DMA control register,            Address offset: 0x48 */
#define TIM_DMAR		0x4C       /*!< TIM DMA address for full transfer,   Address offset: 0x4C */
#define TIM_OR			0x50       /*!< TIM option register,                 Address offset: 0x50 */

/*******************  Bit definition for TIM_CR1 register  ********************/
#define TIM_CR1_CEN_BIT_NUM                         ((uint32_t)0)            /*!<Counter enable        */
#define TIM_CR1_UDIS_BIT_NUM                        ((uint32_t)1)            /*!<Update disable        */
#define TIM_CR1_URS_BIT_NUM                         ((uint32_t)2)            /*!<Update request source */
#define TIM_CR1_OPM_BIT_NUM                         ((uint32_t)3)            /*!<One pulse mode        */
#define TIM_CR1_DIR_BIT_NUM                         ((uint32_t)4)            /*!<Direction             */

																			 /*!<CMS[1:0] bits (Center-aligned mode selection) */
#define TIM_CR1_CMS_0_BIT_NUM                       ((uint32_t)5)            /*!<Bit 0 */
#define TIM_CR1_CMS_1_BIT_NUM                       ((uint32_t)6)            /*!<Bit 1 */

#define TIM_CR1_ARPE_BIT_NUM                        ((uint32_t)7)            /*!<Auto-reload preload enable     */

																			 /*!<CKD[1:0] bits (clock division) */
#define TIM_CR1_CKD_0_BIT_NUM                       ((uint32_t)8)            /*!<Bit 0 */
#define TIM_CR1_CKD_1_BIT_NUM                       ((uint32_t)9)            /*!<Bit 1 */

/*******************  Bit definition for TIM_CR2 register  ********************/
#define TIM_CR2_CCPC_BIT_NUM                        ((uint32_t)0)            /*!<Capture/Compare Preloaded Control        */
#define TIM_CR2_CCUS_BIT_NUM                        ((uint32_t)2)            /*!<Capture/Compare Control Update Selection */
#define TIM_CR2_CCDS_BIT_NUM                        ((uint32_t)3)            /*!<Capture/Compare DMA Selection            */

																			 /*!<MMS[2:0] bits (Master Mode Selection) */
#define TIM_CR2_MMS_0_BIT_NUM                       ((uint32_t)4)            /*!<Bit 0 */
#define TIM_CR2_MMS_1_BIT_NUM                       ((uint32_t)5)            /*!<Bit 1 */
#define TIM_CR2_MMS_2_BIT_NUM                       ((uint32_t)6)            /*!<Bit 2 */

#define TIM_CR2_TI1S_BIT_NUM                        ((uint32_t)7)            /*!<TI1 Selection */
#define TIM_CR2_OIS1_BIT_NUM                        ((uint32_t)8)            /*!<Output Idle state 1 (OC1 output)  */
#define TIM_CR2_OIS1N_BIT_NUM                       ((uint32_t)9)            /*!<Output Idle state 1 (OC1N output) */
#define TIM_CR2_OIS2_BIT_NUM                        ((uint32_t)10)           /*!<Output Idle state 2 (OC2 output)  */
#define TIM_CR2_OIS2N_BIT_NUM                       ((uint32_t)11)           /*!<Output Idle state 2 (OC2N output) */
#define TIM_CR2_OIS3_BIT_NUM                        ((uint32_t)12)           /*!<Output Idle state 3 (OC3 output)  */
#define TIM_CR2_OIS3N_BIT_NUM                       ((uint32_t)13)           /*!<Output Idle state 3 (OC3N output) */
#define TIM_CR2_OIS4_BIT_NUM                        ((uint32_t)14)           /*!<Output Idle state 4 (OC4 output)  */

/*******************  Bit definition for TIM_SMCR register  *******************/
																			 /*!<SMS[2:0] bits (Slave mode selection)    */
#define TIM_SMCR_SMS_0_BIT_NUM                      ((uint32_t)0)            /*!<Bit 0 */
#define TIM_SMCR_SMS_1_BIT_NUM                      ((uint32_t)1)            /*!<Bit 1 */
#define TIM_SMCR_SMS_2_BIT_NUM                      ((uint32_t)2)            /*!<Bit 2 */

																			 /*!<TS[2:0] bits (Trigger selection)        */
#define TIM_SMCR_TS_0_BIT_NUM                       ((uint32_t)4)            /*!<Bit 0 */
#define TIM_SMCR_TS_1_BIT_NUM                       ((uint32_t)5)            /*!<Bit 1 */
#define TIM_SMCR_TS_2_BIT_NUM                       ((uint32_t)6)            /*!<Bit 2 */

#define TIM_SMCR_MSM_BIT_NUM                        ((uint32_t)7)            /*!<Master/slave mode                 */

																			 /*!<ETF[3:0] bits (External trigger filter) */
#define TIM_SMCR_ETF_0_BIT_NUM                      ((uint32_t)8)            /*!<Bit 0 */
#define TIM_SMCR_ETF_1_BIT_NUM                      ((uint32_t)9)            /*!<Bit 1 */
#define TIM_SMCR_ETF_2_BIT_NUM                      ((uint32_t)10)           /*!<Bit 2 */
#define TIM_SMCR_ETF_3_BIT_NUM                      ((uint32_t)11)           /*!<Bit 3 */

																			 /*!<ETPS[1:0] bits (External trigger prescaler) */
#define TIM_SMCR_ETPS_0_BIT_NUM                     ((uint32_t)12)           /*!<Bit 0 */
#define TIM_SMCR_ETPS_1_BIT_NUM                     ((uint32_t)13)           /*!<Bit 1 */

#define TIM_SMCR_ECE_BIT_NUM                        ((uint32_t)14)           /*!<External clock enable     */
#define TIM_SMCR_ETP_BIT_NUM                        ((uint32_t)15)           /*!<External trigger polarity */

/*******************  Bit definition for TIM_DIER register  *******************/
#define TIM_DIER_UIE_BIT_NUM                        ((uint32_t)0)            /*!<Update interrupt enable */
#define TIM_DIER_CC1IE_BIT_NUM                      ((uint32_t)1)            /*!<Capture/Compare 1 interrupt enable   */
#define TIM_DIER_CC2IE_BIT_NUM                      ((uint32_t)2)            /*!<Capture/Compare 2 interrupt enable   */
#define TIM_DIER_CC3IE_BIT_NUM                      ((uint32_t)3)            /*!<Capture/Compare 3 interrupt enable   */
#define TIM_DIER_CC4IE_BIT_NUM                      ((uint32_t)4)            /*!<Capture/Compare 4 interrupt enable   */
#define TIM_DIER_COMIE_BIT_NUM                      ((uint32_t)5)            /*!<COM interrupt enable                 */
#define TIM_DIER_TIE_BIT_NUM                        ((uint32_t)6)            /*!<Trigger interrupt enable             */
#define TIM_DIER_BIE_BIT_NUM                        ((uint32_t)7)            /*!<Break interrupt enable               */
#define TIM_DIER_UDE_BIT_NUM                        ((uint32_t)8)            /*!<Update DMA request enable            */
#define TIM_DIER_CC1DE_BIT_NUM                      ((uint32_t)9)            /*!<Capture/Compare 1 DMA request enable */
#define TIM_DIER_CC2DE_BIT_NUM                      ((uint32_t)10)           /*!<Capture/Compare 2 DMA request enable */
#define TIM_DIER_CC3DE_BIT_NUM                      ((uint32_t)11)           /*!<Capture/Compare 3 DMA request enable */
#define TIM_DIER_CC4DE_BIT_NUM                      ((uint32_t)12)           /*!<Capture/Compare 4 DMA request enable */
#define TIM_DIER_COMDE_BIT_NUM                      ((uint32_t)13)           /*!<COM DMA request enable               */
#define TIM_DIER_TDE_BIT_NUM                        ((uint32_t)14)           /*!<Trigger DMA request enable           */

/********************  Bit definition for TIM_SR register  ********************/
#define TIM_SR_UIF_BIT_NUM                          ((uint32_t)0)            /*!<Update interrupt Flag              */
#define TIM_SR_CC1IF_BIT_NUM                        ((uint32_t)1)            /*!<Capture/Compare 1 interrupt Flag   */
#define TIM_SR_CC2IF_BIT_NUM                        ((uint32_t)2)            /*!<Capture/Compare 2 interrupt Flag   */
#define TIM_SR_CC3IF_BIT_NUM                        ((uint32_t)3)            /*!<Capture/Compare 3 interrupt Flag   */
#define TIM_SR_CC4IF_BIT_NUM                        ((uint32_t)4)            /*!<Capture/Compare 4 interrupt Flag   */
#define TIM_SR_COMIF_BIT_NUM                        ((uint32_t)5)            /*!<COM interrupt Flag                 */
#define TIM_SR_TIF_BIT_NUM                          ((uint32_t)6)            /*!<Trigger interrupt Flag             */
#define TIM_SR_BIF_BIT_NUM                          ((uint32_t)7)            /*!<Break interrupt Flag               */
#define TIM_SR_CC1OF_BIT_NUM                        ((uint32_t)9)            /*!<Capture/Compare 1 Overcapture Flag */
#define TIM_SR_CC2OF_BIT_NUM                        ((uint32_t)10)            /*!<Capture/Compare 2 Overcapture Flag */
#define TIM_SR_CC3OF_BIT_NUM                        ((uint32_t)11)            /*!<Capture/Compare 3 Overcapture Flag */
#define TIM_SR_CC4OF_BIT_NUM                        ((uint32_t)12)            /*!<Capture/Compare 4 Overcapture Flag */

/*******************  Bit definition for TIM_EGR register  ********************/
#define TIM_EGR_UG_BIT_NUM                          ((uint32_t)0)               /*!<Update Generation                         */
#define TIM_EGR_CC1G_BIT_NUM                        ((uint32_t)1)               /*!<Capture/Compare 1 Generation              */
#define TIM_EGR_CC2G_BIT_NUM                        ((uint32_t)2)               /*!<Capture/Compare 2 Generation              */
#define TIM_EGR_CC3G_BIT_NUM                        ((uint32_t)3)               /*!<Capture/Compare 3 Generation              */
#define TIM_EGR_CC4G_BIT_NUM                        ((uint32_t)4)               /*!<Capture/Compare 4 Generation              */
#define TIM_EGR_COMG_BIT_NUM                        ((uint32_t)5)               /*!<Capture/Compare Control Update Generation */
#define TIM_EGR_TG_BIT_NUM                          ((uint32_t)6)               /*!<Trigger Generation                        */
#define TIM_EGR_BG_BIT_NUM                          ((uint32_t)7)               /*!<Break Generation                          */

/******************  Bit definition for TIM_CCMR1 register  *******************/
																			 /*!<CC1S[1:0] bits (Capture/Compare 1 Selection) */
#define TIM_CCMR1_CC1S_0_BIT_NUM                    ((uint32_t)0)            /*!<Bit 0 */
#define TIM_CCMR1_CC1S_1_BIT_NUM                    ((uint32_t)1)            /*!<Bit 1 */

#define TIM_CCMR1_OC1FE_BIT_NUM                     ((uint32_t)2)            /*!<Output Compare 1 Fast enable                 */
#define TIM_CCMR1_OC1PE_BIT_NUM                     ((uint32_t)3)            /*!<Output Compare 1 Preload enable              */

																			 /*!<OC1M[2:0] bits (Output Compare 1 Mode)       */
#define TIM_CCMR1_OC1M_0_BIT_NUM                    ((uint32_t)4)            /*!<Bit 0 */
#define TIM_CCMR1_OC1M_1_BIT_NUM                    ((uint32_t)5)            /*!<Bit 1 */
#define TIM_CCMR1_OC1M_2_BIT_NUM                    ((uint32_t)6)            /*!<Bit 2 */

#define TIM_CCMR1_OC1CE_BIT_NUM                     ((uint32_t)7)            /*!<Output Compare 1Clear Enable                 */

																			 /*!<CC2S[1:0] bits (Capture/Compare 2 Selection) */
#define TIM_CCMR1_CC2S_0_BIT_NUM                    ((uint32_t)8)            /*!<Bit 0 */
#define TIM_CCMR1_CC2S_1_BIT_NUM                    ((uint32_t)9)            /*!<Bit 1 */

#define TIM_CCMR1_OC2FE_BIT_NUM                     ((uint32_t)10)           /*!<Output Compare 2 Fast enable                 */
#define TIM_CCMR1_OC2PE_BIT_NUM                     ((uint32_t)11)           /*!<Output Compare 2 Preload enable              */

																			 /*!<OC2M[2:0] bits (Output Compare 2 Mode)       */
#define TIM_CCMR1_OC2M_0_BIT_NUM                    ((uint32_t)12)           /*!<Bit 0 */
#define TIM_CCMR1_OC2M_1_BIT_NUM                    ((uint32_t)13)           /*!<Bit 1 */
#define TIM_CCMR1_OC2M_2_BIT_NUM                    ((uint32_t)14)           /*!<Bit 2 */

#define TIM_CCMR1_OC2CE_BIT_NUM                     ((uint32_t)15)            /*!<Output Compare 2 Clear Enable */

/*----------------------------------------------------------------------------*/

																			 /*!<IC1PSC[1:0] bits (Input Capture 1 Prescaler) */
#define TIM_CCMR1_IC1PSC_0_BIT_NUM                  ((uint32_t)2)            /*!<Bit 0 */
#define TIM_CCMR1_IC1PSC_1_BIT_NUM                  ((uint32_t)3)            /*!<Bit 1 */

																			 /*!<IC1F[3:0] bits (Input Capture 1 Filter)      */
#define TIM_CCMR1_IC1F_0_BIT_NUM                    ((uint32_t)4)            /*!<Bit 0 */
#define TIM_CCMR1_IC1F_1_BIT_NUM                    ((uint32_t)5)            /*!<Bit 1 */
#define TIM_CCMR1_IC1F_2_BIT_NUM                    ((uint32_t)6)            /*!<Bit 2 */
#define TIM_CCMR1_IC1F_3_BIT_NUM                    ((uint32_t)7)            /*!<Bit 3 */

																			 /*!<IC2PSC[1:0] bits (Input Capture 2 Prescaler)  */
#define TIM_CCMR1_IC2PSC_0_BIT_NUM                  ((uint32_t)10)           /*!<Bit 0 */
#define TIM_CCMR1_IC2PSC_1_BIT_NUM                  ((uint32_t)11)           /*!<Bit 1 */

																			 /*!<IC2F[3:0] bits (Input Capture 2 Filter)       */
#define TIM_CCMR1_IC2F_0_BIT_NUM                    ((uint32_t)12)           /*!<Bit 0 */
#define TIM_CCMR1_IC2F_1_BIT_NUM                    ((uint32_t)13)           /*!<Bit 1 */
#define TIM_CCMR1_IC2F_2_BIT_NUM                    ((uint32_t)14)           /*!<Bit 2 */
#define TIM_CCMR1_IC2F_3_BIT_NUM                    ((uint32_t)15)           /*!<Bit 3 */

/******************  Bit definition for TIM_CCMR2 register  *******************/
																			 /*!<CC3S[1:0] bits (Capture/Compare 3 Selection)  */
#define TIM_CCMR2_CC3S_0_BIT_NUM                    ((uint32_t)0)            /*!<Bit 0 */
#define TIM_CCMR2_CC3S_1_BIT_NUM                    ((uint32_t)1)            /*!<Bit 1 */

#define TIM_CCMR2_OC3FE_BIT_NUM                     ((uint32_t)2)            /*!<Output Compare 3 Fast enable           */
#define TIM_CCMR2_OC3PE_BIT_NUM                     ((uint32_t)3)            /*!<Output Compare 3 Preload enable        */

																			 /*!<OC3M[2:0] bits (Output Compare 3 Mode) */
#define TIM_CCMR2_OC3M_0_BIT_NUM                    ((uint32_t)4)            /*!<Bit 0 */
#define TIM_CCMR2_OC3M_1_BIT_NUM                    ((uint32_t)5)            /*!<Bit 1 */
#define TIM_CCMR2_OC3M_2_BIT_NUM                    ((uint32_t)6)            /*!<Bit 2 */

#define TIM_CCMR2_OC3CE_BIT_NUM                     ((uint32_t)7)            /*!<Output Compare 3 Clear Enable */

																			 /*!<CC4S[1:0] bits (Capture/Compare 4 Selection) */
#define TIM_CCMR2_CC4S_0_BIT_NUM                    ((uint32_t)8)            /*!<Bit 0 */
#define TIM_CCMR2_CC4S_1_BIT_NUM                    ((uint32_t)9)            /*!<Bit 1 */

#define TIM_CCMR2_OC4FE_BIT_NUM                     ((uint32_t)10)           /*!<Output Compare 4 Fast enable    */
#define TIM_CCMR2_OC4PE_BIT_NUM                     ((uint32_t)11)           /*!<Output Compare 4 Preload enable */

																			 /*!<OC4M[2:0] bits (Output Compare 4 Mode) */
#define TIM_CCMR2_OC4M_0_BIT_NUM                    ((uint32_t)12)           /*!<Bit 0 */
#define TIM_CCMR2_OC4M_1_BIT_NUM                    ((uint32_t)13)           /*!<Bit 1 */
#define TIM_CCMR2_OC4M_2_BIT_NUM                    ((uint32_t)14)           /*!<Bit 2 */

#define TIM_CCMR2_OC4CE_BIT_NUM                     ((uint32_t)15)           /*!<Output Compare 4 Clear Enable */

/*----------------------------------------------------------------------------*/

																			 /*!<IC3PSC[1:0] bits (Input Capture 3 Prescaler) */
#define TIM_CCMR2_IC3PSC_0_BIT_NUM                  ((uint32_t)2)            /*!<Bit 0 */
#define TIM_CCMR2_IC3PSC_1_BIT_NUM                  ((uint32_t)3)            /*!<Bit 1 */

																			 /*!<IC3F[3:0] bits (Input Capture 3 Filter) */
#define TIM_CCMR2_IC3F_0_BIT_NUM                    ((uint32_t)4)            /*!<Bit 0 */
#define TIM_CCMR2_IC3F_1_BIT_NUM                    ((uint32_t)5)            /*!<Bit 1 */
#define TIM_CCMR2_IC3F_2_BIT_NUM                    ((uint32_t)6)            /*!<Bit 2 */
#define TIM_CCMR2_IC3F_3_BIT_NUM                    ((uint32_t)7)            /*!<Bit 3 */

																			 /*!<IC4PSC[1:0] bits (Input Capture 4 Prescaler) */
#define TIM_CCMR2_IC4PSC_0_BIT_NUM                  ((uint32_t)10)           /*!<Bit 0 */
#define TIM_CCMR2_IC4PSC_1_BIT_NUM                  ((uint32_t)11)           /*!<Bit 1 */

																			 /*!<IC4F[3:0] bits (Input Capture 4 Filter) */
#define TIM_CCMR2_IC4F_0_BIT_NUM                    ((uint32_t)12)           /*!<Bit 0 */
#define TIM_CCMR2_IC4F_1_BIT_NUM                    ((uint32_t)13)           /*!<Bit 1 */
#define TIM_CCMR2_IC4F_2_BIT_NUM                    ((uint32_t)14)           /*!<Bit 2 */
#define TIM_CCMR2_IC4F_3_BIT_NUM                    ((uint32_t)15)           /*!<Bit 3 */

/*******************  Bit definition for TIM_CCER register  *******************/
#define TIM_CCER_CC1E_BIT_NUM                       ((uint32_t)0)            /*!<Capture/Compare 1 output enable                 */
#define TIM_CCER_CC1P_BIT_NUM                       ((uint32_t)1)            /*!<Capture/Compare 1 output Polarity               */
#define TIM_CCER_CC1NE_BIT_NUM                      ((uint32_t)2)            /*!<Capture/Compare 1 Complementary output enable   */
#define TIM_CCER_CC1NP_BIT_NUM                      ((uint32_t)3)            /*!<Capture/Compare 1 Complementary output Polarity */
#define TIM_CCER_CC2E_BIT_NUM                       ((uint32_t)4)            /*!<Capture/Compare 2 output enable                 */
#define TIM_CCER_CC2P_BIT_NUM                       ((uint32_t)5)            /*!<Capture/Compare 2 output Polarity               */
#define TIM_CCER_CC2NE_BIT_NUM                      ((uint32_t)6)            /*!<Capture/Compare 2 Complementary output enable   */
#define TIM_CCER_CC2NP_BIT_NUM                      ((uint32_t)7)            /*!<Capture/Compare 2 Complementary output Polarity */
#define TIM_CCER_CC3E_BIT_NUM                       ((uint32_t)8)            /*!<Capture/Compare 3 output enable                 */
#define TIM_CCER_CC3P_BIT_NUM                       ((uint32_t)9)            /*!<Capture/Compare 3 output Polarity               */
#define TIM_CCER_CC3NE_BIT_NUM                      ((uint32_t)10)           /*!<Capture/Compare 3 Complementary output enable   */
#define TIM_CCER_CC3NP_BIT_NUM                      ((uint32_t)11)           /*!<Capture/Compare 3 Complementary output Polarity */
#define TIM_CCER_CC4E_BIT_NUM                       ((uint32_t)12)           /*!<Capture/Compare 4 output enable                 */
#define TIM_CCER_CC4P_BIT_NUM                       ((uint32_t)13)           /*!<Capture/Compare 4 output Polarity               */
#define TIM_CCER_CC4NP_BIT_NUM                      ((uint32_t)15)           /*!<Capture/Compare 4 Complementary output Polarity */

/*******************  Bit definition for TIM_CNT register  ********************/

/*******************  Bit definition for TIM_PSC register  ********************/

/*******************  Bit definition for TIM_ARR register  ********************/

/*******************  Bit definition for TIM_RCR register  ********************/

/*******************  Bit definition for TIM_CCR1 register  *******************/

/*******************  Bit definition for TIM_CCR2 register  *******************/

/*******************  Bit definition for TIM_CCR3 register  *******************/

/*******************  Bit definition for TIM_CCR4 register  *******************/

/*******************  Bit definition for TIM_BDTR register  *******************/
																			 /*!<DTG[0:7] bits (Dead-Time Generator set-up) */
#define TIM_BDTR_DTG_0_BIT_NUM                      ((uint32_t)0)            /*!<Bit 0 */
#define TIM_BDTR_DTG_1_BIT_NUM                      ((uint32_t)1)            /*!<Bit 1 */
#define TIM_BDTR_DTG_2_BIT_NUM                      ((uint32_t)2)            /*!<Bit 2 */
#define TIM_BDTR_DTG_3_BIT_NUM                      ((uint32_t)3)            /*!<Bit 3 */
#define TIM_BDTR_DTG_4_BIT_NUM                      ((uint32_t)4)            /*!<Bit 4 */
#define TIM_BDTR_DTG_5_BIT_NUM                      ((uint32_t)5)            /*!<Bit 5 */
#define TIM_BDTR_DTG_6_BIT_NUM                      ((uint32_t)6)            /*!<Bit 6 */
#define TIM_BDTR_DTG_7_BIT_NUM                      ((uint32_t)7)            /*!<Bit 7 */

																			 /*!<LOCK[1:0] bits (Lock Configuration) */
#define TIM_BDTR_LOCK_0_BIT_NUM                     ((uint32_t)8)            /*!<Bit 0 */
#define TIM_BDTR_LOCK_1_BIT_NUM                     ((uint32_t)9)            /*!<Bit 1 */

#define TIM_BDTR_OSSI_BIT_NUM                       ((uint32_t)10)           /*!<Off-State Selection for Idle mode */
#define TIM_BDTR_OSSR_BIT_NUM                       ((uint32_t)11)           /*!<Off-State Selection for Run mode  */
#define TIM_BDTR_BKE_BIT_NUM                        ((uint32_t)12)           /*!<Break enable                      */
#define TIM_BDTR_BKP_BIT_NUM                        ((uint32_t)13)           /*!<Break Polarity                    */
#define TIM_BDTR_AOE_BIT_NUM                        ((uint32_t)14)           /*!<Automatic Output enable           */
#define TIM_BDTR_MOE_BIT_NUM                        ((uint32_t)15)           /*!<Main Output enable                */

/*******************  Bit definition for TIM_DCR register  ********************/
																			 /*!<DBA[4:0] bits (DMA Base Address) */
#define TIM_DCR_DBA_0_BIT_NUM                       ((uint32_t)0)            /*!<Bit 0 */
#define TIM_DCR_DBA_1_BIT_NUM                       ((uint32_t)1)            /*!<Bit 1 */
#define TIM_DCR_DBA_2_BIT_NUM                       ((uint32_t)2)            /*!<Bit 2 */
#define TIM_DCR_DBA_3_BIT_NUM                       ((uint32_t)3)            /*!<Bit 3 */
#define TIM_DCR_DBA_4_BIT_NUM                       ((uint32_t)4)            /*!<Bit 4 */

																			 /*!<DBL[4:0] bits (DMA Burst Length) */
#define TIM_DCR_DBL_0_BIT_NUM                       ((uint32_t)8)            /*!<Bit 0 */
#define TIM_DCR_DBL_1_BIT_NUM                       ((uint32_t)9)            /*!<Bit 1 */
#define TIM_DCR_DBL_2_BIT_NUM                       ((uint32_t)10)           /*!<Bit 2 */
#define TIM_DCR_DBL_3_BIT_NUM                       ((uint32_t)11)           /*!<Bit 3 */
#define TIM_DCR_DBL_4_BIT_NUM                       ((uint32_t)12)           /*!<Bit 4 */

/*******************  Bit definition for TIM_DMAR register  *******************/

/*******************  Bit definition for TIM_OR register  *********************/
																	  /*!<TI4_RMP[1:0] bits (TIM5 Input 4 remap)             */
#define TIM_OR_TI4_RMP_0_BIT_NUM                     ((uint32_t)6)            /*!<Bit 0 */
#define TIM_OR_TI4_RMP_1_BIT_NUM                     ((uint32_t)7)            /*!<Bit 1 */

																	  /*!<ITR1_RMP[1:0] bits (TIM2 Internal trigger 1 remap) */
#define TIM_OR_ITR1_RMP_0_BIT_NUM                    ((uint32_t)10)           /*!<Bit 0 */
#define TIM_OR_ITR1_RMP_1_BIT_NUM                    ((uint32_t)11)           /*!<Bit 1 */

#endif
