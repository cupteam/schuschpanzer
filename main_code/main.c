#include "stm32f4xx.h" // SPL config file stm32f4xx_conf.h is autoincluded (by USE_STDPERIPH_DRIVER in stm32f4xx.h)
#include "stm32f411_bit_band.h" //Macros and definitions for using bit band.
//For UART
#include "UART_output.h"
//For FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "ADNS-9800_drv.h" //For MPU support.
//Tasks.
#include "orientation_task.h"

//Stdlib functions
#include <stdio.h> //For printf.

#include "SPI_ADNS-9800.h"

// Stuct for GPIO initialization.
GPIO_InitTypeDef GPIO_InitStructure;

volatile uint32_t glob_var1 = 0; //For communication between interrupt handler routine and task.

//It's interesting, but it looks as EXTI not need to be clocked from APB2 bus.

//Periph. bit bang is possible, if adress of register < 0x40100000. It includes all AHB1, APB1, APB2 periph. devices, except. AHB2 (USB).

/* For DEBUG, in FreeRTOS */
//This function catches misusing of FreeRTOS system calls for debug purposes.
//To disable this check (when program works well) comment #define configASSERT( ... in FreeRTOSConfig.h
//It's assumed, that UARTx is initialized.
void vAssertCalled(char * file_name, unsigned int line_num)
{
	char err_string[100]; //For number of line.
	
	taskDISABLE_INTERRUPTS(); //To disable SysTick interrupts, i.e. scheduler for freezing the program.
	
	sprintf(err_string, "Assert Error: file - %s, line - %u! \n\r", file_name, line_num);
	USART_send_my_string((uint8_t *) err_string);
	
	while(1)	//Infinite loop.
	{
		//Programm has failed. Waiting for reset.
	}
}


//For FreeRTOS
typedef struct 
{
	uint8_t string[20]; //It must be a null-terminated string.
} text_message;

xQueueHandle xMyQueue;	//Handler of MyQueue.

xTaskHandle Task1_handler; //Handler for stack watermark.

//Tasks 
void vTask1 (void * pvParameters)
{
	text_message hello = { "Hello " };
	
	//Infinite loop
	while(1)
	{
		//Send struct {hello} through queue without timeout.
		xQueueSend(xMyQueue, &hello, portMAX_DELAY); //It will blocked in second sending untill vTask2 receive message.
	}
}



//Second task is in orientation_task.c file.

void IRQ_EXTI_13_switch_on(void)
{
	//For correct EXTI pin maping settings we have to clock SYSCFG.
		RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;	
	
	
	/* EXTI Configuration */
	
	//Connecting PC13 pin to EXTI line #13. (Using SYSCFG periph. dev.)
	SYSCFG->EXTICR[3] |=  SYSCFG_EXTICR4_EXTI13_PC; //Type conv. 32bit |= 16 bit
	
	//Configuration the Trigger selection bits (Fall condition).
	BIT_BAND_REG_SET(EXTI_BASE, EXTI_FTSR, EXTI_FTSR_LINE13_BIT_NUM);
	
	//Configure the mask bits to Enable Interrupt for line 13.
	BIT_BAND_REG_SET(EXTI_BASE, EXTI_IMR, EXTI_IMR_LINE13_BIT_NUM);
	
	
	
	/* Config NVIC for IRQ#40 (=EXTI15_10_IRQn) */
	
	//Set priority to 10(Decimal), but we also can leave default value 0.
	NVIC_SetPriority(EXTI15_10_IRQn, 0xA); //This interrupt can not use _From_ISR functions. 
	
	//Enable interrupt IRQ#40 (=EXTI15_10_IRQn)
	NVIC_EnableIRQ(EXTI15_10_IRQn);
	
	//Here we should enable global interrupts by calling __enable_irq();, but they has been enabled after reset (by default).
}


//Interrupt handler for EXTI line #13.
void EXTI15_10_IRQHandler(void)
{
	//We should test, if interrupt was really came from #13 line of EXTI.
	if((EXTI->PR & EXTI_PR_PR13) != 0) //But it's better to use BIT_BAND_REG_READ(EXTI_BASE, EXTI_PR, EXTI_PR_LINE13_BIT_NUM);
	{
		//Clear line_13_bit in EXTI_PR to clear an interrupt asserting by writing 1 to #13 bit in PR.
		BIT_BAND_REG_SET(EXTI_BASE, EXTI_PR, EXTI_PR_LINE13_BIT_NUM);
	
		//Payload of interrupt.
		glob_var1++;
		
		return;
	}
	else //It was another EXTI line.
		return;
}
 
int main(void)
{
	/*System init (System clock source, PLL Multiplier and Divider factors,
	AHB/APBx prescalers and Flash settings)have already set in system_stm32f4xx.c */
	
//	uint32_t i; //For delays.
	
//	uint8_t message[] = "It's my life!\n\r";
	uint8_t q_fail[] = "Queue haven't been created!\n\r";
 
	// Enable clock of GPIOA
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	
	/* BUTTON: GPIOC, pin 13 - input, floating (it's by default after reset) */
	// Enable clock of GPIOC
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	
	USART_init_myfunc();	//Init USART at 19600 bod, 2 stop, no parity. It's also need for configASSERT.
	IRQ_EXTI_13_switch_on(); //Configures NVIC, EXTI and SYSCFG for using EXTI's line 13 interrupt for PC13 pin (falling).

/* 
	//	GPIO_SetBits(GPIOA,GPIO_Pin_5);		// Set pin to 1
		BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_5_BIT_NUM);
		for(i = 0; i < 12*1000*1000; i++) //Make delay approx. 1s.
		{
		}
//		GPIO_ResetBits(GPIOA,GPIO_Pin_5);		// Reset pin to 0
  	BIT_BAND_REG_CLR(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_5_BIT_NUM);
		for(i = 0; i < 12*1000*1000; i++) //Make delay approx. 1s.
		{
		}
*/
		USART_send_my_string("Main. OK.\n\r");
//		delay_micro_s(1e6);
//		USART_send_my_string("Main. OK. 1 000 000 us\n\r");

	//Create queue from 1 element of text_message type.
		xMyQueue = xQueueCreate(1, sizeof(text_message));
		if(xMyQueue != NULL) //Success.
		{
			//Create tasks
			xTaskCreate(vTask1, "Sender", configMINIMAL_STACK_SIZE + 100, NULL, 1, &Task1_handler);
			xTaskCreate(vOrientation_task, "Receiver", configMINIMAL_STACK_SIZE + 10000, NULL, 1, NULL);
			
			//Starting of Scheduler
			vTaskStartScheduler();
		}
		else
		{
			USART_send_my_string(q_fail);
			while(1);
		}
		
}

#ifdef  USE_FULL_ASSERT	//For DEBUG, in SPL.
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
 
  /* Infinite loop */
  while (1)
  {
  }
}
#endif


#if defined(ADNS9800_DEBUG) || defined(PWM_AB_debug) /* For DEBUG, in ADNS9800 driver, and PWM_AB driver */

//This function catches misusing of ADNS9800 driver and PWM_AB lib (for debug purposes).
//To disable this check (when program works well) comment #define ADNS9800_DEBUG in ADNS9800_drv.h and #define PWM_AB_DEBUG in PWM_AB.h
//It's assumed, that UART is initialized.
void Lib_Error_Message(char * file_name, unsigned int line_num)
{
	char err_string[100]; //For number of line.
	
#ifdef FREERTOS_CONFIG_H	//Check if we using FreeRTOS
	taskDISABLE_INTERRUPTS(); //To disable SysTick interrupts, i.e. scheduler for freezing the programm.
#endif
	
	sprintf(err_string, "Lib Error: file - %s, line - %u! \n\r", file_name, line_num);
	USART_send_my_string((uint8_t *) err_string);
	
	while(1)	//Infinite loop.
	{
		//Programm has failed. Waiting for reset.
	}
}
#endif
