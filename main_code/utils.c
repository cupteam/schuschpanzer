#include "stm32f4xx.h"

//Two functions for delays. Not very pricision. May be deleted by compilers. Must be tested for working when compiler/optimization changes.

/* TODO
//Limits: SystemCoreClock - 20MHz ... 400 MHz, pause_length = 200ns ... 1.000.000ns (1 ms).
uint32_t delay_nano_s(uint32_t pause_length)
{
	Not realized!
}
*/

//Limits: SystemCoreClock - 20MHz ... 400 MHz, pause_length = 1us ... 1.000.000us (1s).
//delay_micro_s(1) is eq. about 1.5 us. Hence delay_micro_s(pause_length) give pause_length + 0.5 us delay.
uint32_t delay_micro_s(uint32_t pause_length)
{
	volatile uint32_t i; //Counter variable. We don't want optimization here.
	
	uint32_t max_i = ((SystemCoreClock/1e5)*(pause_length - 1))/10/11; //Added '/clocks_in_one_cycle'=11 to condition.
	
	for(i = 0; i < max_i; i++) 
	{
	}
	return i;
}
