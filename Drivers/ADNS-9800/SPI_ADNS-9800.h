#ifndef __SPI_ADNS9800_H
#define __SPI_ADNS9800_H

/* SPI Settings */
#define APB2_BUS_FREQUENCY 72000000 //Set this macros to actual APB2 bus frequency to proper configure ADNS-9800 lib.


//Define frame period (in us) for Motion Burst Read and SROM Download functions.
//In DS: Waiting of t(SRAD) is Eq. waiting of 1 frame. 
//But if we use def. frame rate, delay is 480us! (and even 500 us - max period)
#define FRAME_MAX_PERIOD 500
#define FRAME_PERIOD FRAME_MAX_PERIOD

/* Inits SPI1 on fSCLK <= 1 MHz, configures PA4 as GPIO output (for NSS) and sets to 1, 
	PA5 - SPI1_SCK.
	PA6 - SPI1_MISO.
	PA7 - SPI1_MOSI.
	*/
void Init_SPI1_for_ADNS9800(void);

//Writes 1 byte (reg_data) to ADNS-9800's register, specified by reg_adress.
//All registers in ADNS-9800 have 8-bit size.
void SPI_write_byte_ADNS9800(uint8_t reg_adress, uint8_t reg_data);

//Reads (and returns) the value of ADNS-9800's register, specified by reg_adress.
//All registers in ADNS-9800 have 8-bit size.
uint8_t SPI_read_byte_ADNS9800(uint8_t reg_adress);

//Reads up to 14. sensor's data out registers:
//data_out[0] - Motion.
//data_out[1] - Observation.
//data_out[2] - Delta_X_L.
//data_out[3] - Delta_X_H.
//data_out[4] - Delta_Y_L.
//data_out[5] - Delta_X_H.
//data_out[6] - SQUAL.
//data_out[7] - Pixel_Sum.
//data_out[8] - Maximum_Pixel.
//data_out[9] - Minimum_Pixel.
//data_out[10] - Shutter_Upper.
//data_out[11] - Shutter_Lower.
//data_out[12] - Frame_Period_Upper.
//data_out[13] - Frame_Period_Lower.
//Params: data_out[14] - massive for data loading, data_amount - how many bytes will be read (and length of used data_out part).
void motion_burst_read_ADNS9800(uint8_t * data_out, uint8_t data_amount);

//Downloads firmware to ADNS-9800. Must be used in power-on routine.
//SROM[3070] - global massive of 3070 bytes with factory firmware.
void SROM_Download_ADNS9800(void);

#endif
