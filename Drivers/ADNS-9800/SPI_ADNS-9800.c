//This file contains all for communication with ADNS-9800 through SPI. All SPI settings for ADNS-9800 can be choosed here.

//THIS PROGRAM USES SPI1 (PA5-PA7 PINS) and PA4 - #Slave_Select. IN NUCLEO-F411RE DEMOBOARD LED2 SHOULD BE UNCONNECTED FROM PA5 (BY SB21)!

#include "stdlib.h"
#include "stm32f4xx.h"
#include "stm32f411_bit_band.h"
#include "SPI_ADNS-9800.h"
#include "ADNS-9800_drv.h"
#include "adns9800_srom_A6.h" //File with sensor's SROM. Change to ..._A4 or ..._A5 if want to use other firmware version.
#include "utils.h"

extern uint32_t SystemCoreClock; //Contains HCLK frequency in Hz. This variable is defined in system_stm32f4xx.c


void Init_SPI1_for_ADNS9800(void)
{
	uint8_t i;
	
	//Enable clocking GPIOA on AHB1 bus.
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	
	//Enable SPI1 clocking on APB2 bus.
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
	
	//PA4 we will use as #slave select to data frame sinchronization.
	//Setting PA4 as usual GPIO output, low speed, push-pull, without pulling.
	
	BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Set PA4 to 1, to prevent unexpected #slave select signal.
	
	GPIOA->MODER |= GPIO_MODER_MODER4_0; //MODER - Output, 0b01.
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_4); //Push-pull. It is not need to do after reset.
	GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR4); // Low speed (Max 2-4 MHz). It is not need after reset.
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR4); //Without any Pulling. Not need after reset.
	
	/*	For other peripherals:
	� Configure the desired I/O as an alternate function in the GPIOx_MODER register
	� Select the type, pull-up/pull-down and output speed via the GPIOx_OTYPER,
	GPIOx_PUPDR and GPIOx_OSPEEDER registers, respectively
	� Connect the I/O to the desired AFx in the GPIOx_AFRL or GPIOx_AFRH register */
	//Setting Alternative Function for PA5 - SPI1_SCK.
	//Setting Alternative Function for PA6 - SPI1_MISO.
	//Setting Alternative Function for PA7 - SPI1_MOSI.
	//So, we need AF05, MODER - AF, OTYPER - push-pull, OSPEEDR - low, PUPDR - no pulling.
	
	GPIOA->MODER |= GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1; //MODER - AF for PA5-PA7.
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_5 | GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7); //Push-pull. It is not need to do after reset.
	GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR5 | GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR7); // Low speed (Max 2-4 MHz). It is not need after reset.
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR5 | GPIO_PUPDR_PUPDR6 | GPIO_PUPDR_PUPDR7); //Without any Pulling. Not need after reset.
	GPIOA->AFR[0] |= (0x5 << (4*5) | 0x5 << (4*6) | 0x5 << (4*7)); //AF05 for SPI1.
	
	/*
		BR[2:0]: Baud rate control
	0	000: fPCLK/2		2 = 2^1 = 1 << 1
	1	001: fPCLK/4    4 = 2^2 = 1 << 2
	2	010: fPCLK/8		8 = 2^3 = 1 << 3
	3	011: fPCLK/16		16 = 2^4 = 1 << 4
	4	100: fPCLK/32		32 = 2^5 = 1 << 5
	5	101: fPCLK/64		64 = 2^6 = 1 << 6
	6	110: fPCLK/128	128 = 2^7 = 1 << 7
	7	111: fPCLK/256	256 = 2^8 = 1 << 8
	*/
	
	for(i = 1; ((APB2_BUS_FREQUENCY / (1 << i)) > 1000000) && (i < 9); i++) //Find Baud rate.
		;
		
#ifdef ADNS9800_DEBUG	
	if(i > 8) //We cannot find baud rate.
		LIB_ERROR_CALL;
#endif
	
	//Frequency of SCK should be <= 1 MHz. We set BR[2:0] = 0b110 in the SPI_CR1 so 72MHz/128 = 0.5625 MHz.
	SPI1->CR1 |= ((i - 1) << 3); //If APB2 bus has frequency 72MHz then fSCLK = 0.5625 MHz.

	//CPOL = 1 (SCK is high in the idle state).
	BIT_BAND_REG_SET(SPI1_BASE, SPI_CR1, SPI_CR1_CPOL_BIT_NUM);
	
	//CPHA = 1 (Data capture on the rise SCK edge, if CPOL=1).
	BIT_BAND_REG_SET(SPI1_BASE, SPI_CR1, SPI_CR1_CPHA_BIT_NUM);
		
	//Data frame is 8 bit long (DFF = 0 in SPI_CR1 register).
	BIT_BAND_REG_CLR(SPI1_BASE, SPI_CR1, SPI_CR1_DFF_BIT_NUM);
	
	//SPI - MSB first (LSBFIRST = 0 in SPI_CR1 register).
	BIT_BAND_REG_CLR(SPI1_BASE, SPI_CR1, SPI_CR1_LSBFIRST_BIT_NUM);
	
	//Software NSS (== #SlaveSelect) pin management (SSM = 1 in SPI_CR1)
	BIT_BAND_REG_SET(SPI1_BASE, SPI_CR1, SPI_CR1_SSM_BIT_NUM);

	//NSS pin "software level" = 1 (SSI bit = 1 in SPI_CR1) to allow working of SPI in master mode.
	BIT_BAND_REG_SET(SPI1_BASE, SPI_CR1, SPI_CR1_SSI_BIT_NUM);
	
	//FRF = 0 in SPI_CR2 for SPI Motorola mode.
	BIT_BAND_REG_CLR(SPI1_BASE, SPI_CR2, SPI_CR2_FRF_BIT_NUM);
		
	//In full-duplex (BIDIMODE=0 and RXONLY=0), after reset it is't need.
	
	//No CRC (CRCEN = 0 bit in the SPI_CR1). It is not need to do after reset.
	
	//SPI mode - master, MSTR = 1 in SPI_CR1.
	BIT_BAND_REG_SET(SPI1_BASE, SPI_CR1, SPI_CR1_MSTR_BIT_NUM);

	//SPI enable.
	BIT_BAND_REG_SET(SPI1_BASE, SPI_CR1, SPI_CR1_SPE_BIT_NUM);
	
	return;
}


//Writes 1 byte of reg_data to reg_adress in ADNS-9800.
void SPI_write_byte_ADNS9800(uint8_t reg_adress, uint8_t reg_data)
{
	
	uint16_t received_data;
	
	
	/* Start ADNS-9800 transaction */
	BIT_BAND_REG_CLR(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Clear PA4 to 0, to enable slave data transfer.
	
	//Wait > 120ns (NCS-SCLK). 
	//This condition is satisfied automatically because before first rising SCLK edge we'll wait at least 1/2 of SCLK period: 500ns/2=250ns.
	
	reg_adress |= 1 << 7; //For write mode set [7] bit of reg_address.
	
	SPI1->DR = (uint16_t) reg_adress; //send first byte.
	
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	SPI1->DR = (uint16_t) reg_data; //send second byte.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
	{
	}
	received_data = SPI1->DR; //False reading, because it's dummy data.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
	{
	}
	received_data = SPI1->DR; //Would be real data, stored at reg_adress, if we are reading.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_BSY_BIT_NUM) != 0) //Wait until BSY = 0.
	{
	}
	
	delay_micro_s(20); //Wait 20us, t(SCLK-NCS).
	
	/* End ADNS-9800 transaction */
	BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Set PA4 to 1.
	
	delay_micro_s(100); //Wait t(SWR/SWW) minus 20 us.
	
	return;
}


//Reads (and returns) the value of ADNS-9800's register, specified by reg_adress.
//All registers in ADNS-9800 have 8-bit size.
uint8_t SPI_read_byte_ADNS9800(uint8_t reg_adress)
{
	uint8_t reg_data;
	uint16_t received_data;
	
	
	/* Start ADNS9800 transaction */
	BIT_BAND_REG_CLR(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Clear PA4 to 0, to enable slave data transfer.
		
	//Wait > 120ns (NCS-SCLK). 
	//This condition is satisfied automatically because before first rising SCLK edge we'll wait at least 1/2 of SCLK period: 500ns/2=250ns.
	
	reg_adress &= ~(1 << 7); //We're going to Read from ADNS-9800 - clear [7] bit of reg_address.
	
	SPI1->DR = (uint16_t) reg_adress; //Send first byte.
	
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1. (i.e. first byte will be really sended).
	{
	}
	received_data = SPI1->DR; //False reading, because it's dummy data.
	
	delay_micro_s(100); //Wait t(SRAD).
	
	//As we've received first (dummy) byte, we are sure that TX flag is set.
	SPI1->DR = (uint16_t) 0xA5; //Send second byte. We can send any byte, because we're reading now.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
	{
	}
	received_data = SPI1->DR; //Real data, stored at reg_adress in sensor's register.
	
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_BSY_BIT_NUM) != 0) //Wait until BSY = 0.
	{
	}
	
	delay_micro_s(1); //Wait > t(SCLK-NCS) = 120ns.
	
	//We are need in received byte.
	reg_data = (uint8_t) received_data;
	
	/* End of transaction */
	BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Set PA4 (i.e. NCS line) to 1.
	
	delay_micro_s(20); //Wait 20us, t(SRW/SRR).
	
	return reg_data;
}


//Reads up to 14. sensor's data out registers:
//data_out[0] - Motion.
//data_out[1] - Observation.
//data_out[2] - Delta_X_L.
//data_out[3] - Delta_X_H.
//data_out[4] - Delta_Y_L.
//data_out[5] - Delta_X_H.
//data_out[6] - SQUAL.
//data_out[7] - Pixel_Sum.
//data_out[8] - Maximum_Pixel.
//data_out[9] - Minimum_Pixel.
//data_out[10] - Shutter_Upper.
//data_out[11] - Shutter_Lower.
//data_out[12] - Frame_Period_Upper.
//data_out[13] - Frame_Period_Lower.

//Params: data_out[14] - massive for data loading, data_amount - how many bytes will be read (and length of used data_out part).
void motion_burst_read_ADNS9800(uint8_t * data_out, uint8_t data_amount)
{
	uint32_t i;
	uint16_t received_data;
	uint8_t reg_adress;
	
#ifdef ADNS9800_DEBUG
	if(data_amount > 14 || data_amount == 0 || data_out == NULL) //We try to read impossible amount of data or use bad pointer.
		LIB_ERROR_CALL;
#endif
	
	/* Start ADNS9800 transaction */
	BIT_BAND_REG_CLR(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Clear PA4 to 0, to enable slave data transfer.
		
	//Wait > 120ns (NCS-SCLK). 
	//This condition is satisfied automatically because before first rising SCLK edge we'll wait at least 1/2 of SCLK period: 500ns/2=250ns.
	
	reg_adress = ADNS9800_Motion_Burst; //We're going to Read from ADNS-9800 - bit [7] of reg_address must be cleared.
	SPI1->DR = (uint16_t) reg_adress; //Send first byte (Motion_Burst's address).
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1. (i.e. untill the first byte will be really sended).
	{
	}
	received_data = SPI1->DR; //False reading (because it's dummy data) to clear RXNE flag.
	
	delay_micro_s(FRAME_PERIOD);	//Wait for 1 frame. 
	
	//As we've received first (dummy) byte, we are sure that TX flag is set. Show must go!
	SPI1->DR = (uint16_t) 0xEC; //Send byte. We can send any byte, because we're reading now.
	for(i=0; i < data_amount - 1; i++)
	{
		while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
		{
		}
		SPI1->DR = (uint16_t) 0xEC; //Send next byte.
		while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
		{
		}
		data_out[i] = (uint8_t) SPI1->DR; //Store received data (it has byte size).
	}
	
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
	{
	}
	data_out[i] = (uint8_t) SPI1->DR; //Store received the last data byte.
	
	
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_BSY_BIT_NUM) != 0) //Wait until BSY = 0.
	{
	}
	
	delay_micro_s(1); //Wait > t(SCLK-NCS) = 120ns.
		
	/* End of transaction */
	BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Set PA4 (i.e. NCS line) to 1.
	
	//I don't sure, it can be increased to 120us for correct working.
	delay_micro_s(20); //Wait 20us, t(SRW/SRR). It's longer, than t(BEXIT) = 500ns.
	
	SPI_write_byte_ADNS9800(ADNS9800_Motion, 0x00); //6. Clear residual motion.
	
	return;
}

//Downloads firmware to ADNS-9800. Must be used in power-on routine.
//SROM[3070] - global massive of 3070 bytes with factory firmware.
void SROM_Download_ADNS9800(void)
{
	uint8_t reg_adress;
	uint16_t received_data;
	uint16_t i;
	
	
	/* Start ADNS-9800 transaction */
	BIT_BAND_REG_CLR(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Clear PA4 to 0, to enable slave data transfer.
	
	//Wait > 120ns (NCS-SCLK). 
	//This condition is satisfied automatically because before first rising SCLK edge we'll wait at least 1/2 of SCLK period: 500ns/2=250ns.
	
	//Set 3kB firmware size.
	reg_adress = ADNS9800_Conf_IV;
	reg_adress |= 1 << 7; //For write mode set [7] bit of reg_address.
	SPI1->DR = (uint16_t) reg_adress; //send first byte (address).
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	SPI1->DR = (uint16_t) 0x02; //send second byte, SROM_size(#1) = 1, i.e. 3kB size.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1, i.e. until transfer really completes.
	{
	}
	received_data = SPI1->DR; //False reading, because it's dummy data (done only to clear RXNE flag).
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
	{
	}
	received_data = SPI1->DR; //clear RXNE flag again.
	
	delay_micro_s(120); //Wait t(SWR/SWW).
	
	//Write 0x1d to ADNS9800_SROM_Enable register (initialize SROM downloading)
	reg_adress = ADNS9800_SROM_Enable;
	reg_adress |= 1 << 7; //For write mode set [7] bit of reg_address.
	SPI1->DR = (uint16_t) reg_adress; //send first byte (address).
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	SPI1->DR = (uint16_t) 0x1d; //send data byte - init downloading.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1, i.e. until transfer really completes.
	{
	}
	received_data = SPI1->DR; //False reading, because it's dummy data (clear RXNE flag).
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1.
	{
	}
	received_data = SPI1->DR; //clear RXNE flag again.
	
	delay_micro_s(20); //Wait 20us, t(SCLK-NCS).
	/* End ADNS-9800 transaction */
	BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Set PA4 to 1.
	delay_micro_s(FRAME_PERIOD); //Wait for 1 Frame.
	
	//Write 0x18 to ADNS9800_SROM_Enable register.
	SPI_write_byte_ADNS9800(ADNS9800_SROM_Enable, 0x18);
	
	/* Start ADNS-9800 transaction again*/
	BIT_BAND_REG_CLR(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Clear PA4 to 0, to enable slave data transfer.
	//Wait > 120ns (NCS-SCLK). 
	//This condition is satisfied automatically because before first rising SCLK edge we'll wait at least 1/2 of SCLK period: 500ns/2=250ns.
	
	//Write SROM data to SROM_Load_Burst register.
	reg_adress = ADNS9800_SROM_Load_Brst;
	reg_adress |= 1 << 7; //For write mode set [7] bit of reg_address.
	SPI1->DR = (uint16_t) reg_adress; //send first byte (address).

	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1, i.e. until transfer really completes.
	{
	}
	received_data = SPI1->DR; //clear RXNE flag.
	for(i = 0; i < 3070; i++)
	{
		delay_micro_s(15);
		SPI1->DR = (uint16_t) SROM[i]; //send next data byte.
		while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_RXNE_BIT_NUM) != 1) //Wait until RXNE = 1, i.e. until transfer really completes.
		{
		}
		received_data = SPI1->DR; //clear RXNE flag.
	}
	//Ending from ST's SPI datasheet.
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_TXE_BIT_NUM) != 1) //Wait until TXE = 1.
	{
	}
	while(BIT_BAND_REG_READ(SPI1_BASE, SPI_SR, SPI_SR_BSY_BIT_NUM) != 0) //Wait until BSY = 0.
	{
	}
	
	delay_micro_s(10);
	/* End ADNS-9800 transaction */
	BIT_BAND_REG_SET(GPIOA_BASE, GPIO_ODR, GPIO_ODR_ODR_4_BIT_NUM); //Set PA4 to 1.
	delay_micro_s(1); //Exit from burst mode, (tBEXIT).
	
	delay_micro_s(560); //Wait 160us minus 10us and minus 1us. (for Debug -  560 us.)
	
	return;
}

