#include "stm32f4xx.h"
#include "ADNS-9800_drv.h"
#include "SPI_ADNS-9800.h"
#include "utils.h"


/* Power ON sequence */

//Powers Up sensor and Loads firmware (Shadow ROM).
//Returns SROM_ID or 0x00 (if SROM loading has failed).
//Not enables Laser if SROM_ID = 0x00.
int8_t ADNS9800_PWRON(void)
{
	uint8_t reg_addr, data;
	
	//Reset all chip. Set all settings to def. values.
	SPI_write_byte_ADNS9800(ADNS9800_Pwr_Up_Rst, 0x5A);
	
	delay_micro_s(50e3); //Wait 50ms.
	
	//Clear residual motion.
	for(reg_addr = 0x02; reg_addr <= 0x06; reg_addr++)
		SPI_read_byte_ADNS9800(reg_addr);
	
	//SROM download and start execution.
	SROM_Download_ADNS9800();
//delay_micro_s(250); //Wait 50ms. For Debug.
	data = SPI_read_byte_ADNS9800(ADNS9800_SROM_ID); //If SROM is running?
	if(data == 0x00)
		return data;	//If SROM not works, we needn't enable laser.
	reg_addr = data; //Copy SROM_ID.
	
	//Enable LASER.
	data = SPI_read_byte_ADNS9800(ADNS9800_LASER_CTRL0);
	data &= ~(1 << 0); //Clear Laser Force_Disabled bit.
	SPI_write_byte_ADNS9800(ADNS9800_LASER_CTRL0, data);
	
	//Return SROM_ID.
	return reg_addr; 
}


/* INFO about device */

//Reads INFO data from registers.
void ADNS9800_INFO(ADNS9800_info_t * info_struct)
{
	info_struct->Product_ID = SPI_read_byte_ADNS9800(ADNS9800_Product_ID);
	info_struct->Revision_ID = SPI_read_byte_ADNS9800(ADNS9800_Revision_ID);
	info_struct->SROM_ID = SPI_read_byte_ADNS9800(ADNS9800_SROM_ID);
}


/* STATistic */

//Reads registers with STATistic data about frame image and it's processing.
//Details are in struct ADNS9800_STAT_t definition.
void ADNS9800_STAT(ADNS9800_STAT_t * stat_struct)
{
	uint8_t reg_value;
	uint16_t reg_value16;
	
	//SQUAL reg. multiplied by 4.
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_SQUAL);
	stat_struct->SQUAL4 = ((uint16_t) reg_value) << 2; //SQUAL reg. multiplied by 4.
	
	//Pixel_Sum reg. multiplied by 512. 
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_Pixel_Sum);
	stat_struct->Pixel_Sum512 = ((uint32_t) reg_value) << 9; //SQUAL reg. multiplied by 512.
	
	//Seven bit uint.
	stat_struct->Max_Pixel = SPI_read_byte_ADNS9800(ADNS9800_Max_Pixel);
	
	//Seven bit uint.
	stat_struct->Min_Pixel = SPI_read_byte_ADNS9800(ADNS9800_Min_Pixel);
	
	//Shutter, units are clock of internal osc. (50MHz).
	//We must read Upper reg. first.
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_Shtr_Upper);
	reg_value16 = ((uint16_t) reg_value) << 8;
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_Shtr_Lower);
	reg_value16 |= (uint16_t) reg_value;
	stat_struct->Shutter = reg_value16;
	
	//Frame Period, units are clock of internal osc. (50MHz).
	//We must read Upper reg. first.	
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_Frm_Prd_Upper);
	reg_value16 = ((uint16_t) reg_value) << 8;
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_Frm_Prd_Lower);
	reg_value16 |= (uint16_t) reg_value;
	stat_struct->Frame_Period = reg_value16;
	
}


/* CONFiguration functions */

//Sets resolution for X and Y axes. Resolution = 200 cpi * res_x (res_y, for Y axe).
//Max value for res_x and res_y is 0x29 (eq. 8200 cpi resolution). Def value = 0x09 (eq. 1800 cpi) for both axes.
void ADNS9800_CONF_resolution(uint8_t res_x, uint8_t res_y)
{
	uint8_t temp;
	
	SPI_write_byte_ADNS9800(ADNS9800_Conf_I, res_x);
	if(res_x == res_y)	//If we want equal setting for both axis
	{//Clear Rpt_Mod to use the same cpi value for X and Y.
		temp = SPI_read_byte_ADNS9800(ADNS9800_Conf_II);
		temp &= ~(1 << 2); 
		SPI_write_byte_ADNS9800(ADNS9800_Conf_II, temp);
	}
	else //We want different values cpi for X and Y axes.
	{
		SPI_write_byte_ADNS9800(ADNS9800_Conf_V, res_y);
		//Set Rpt_Mod to use different cpi value for X and Y.
		temp = SPI_read_byte_ADNS9800(ADNS9800_Conf_II);
		temp |= (1 << 2); 
		SPI_write_byte_ADNS9800(ADNS9800_Conf_II, temp);
	}
}

//Sets Lift Detection Threshold (see p.38 in DS, Lift_Detection_Thr register).
//Def. value -- 0x10 (2.4mm). Max value -- 0x1F.
void ADNS9800_CONF_set_lift_detect_thrsh(uint8_t threshold)
{
	SPI_write_byte_ADNS9800(ADNS9800_Lift_Dtct_Thr, (threshold & 0x1F));
}

//Enables/disables Angle Snapping function. (see p. 39 in DS, Snap_Angle register).
//Def - disabled.
void ADNS9800_CONF_snap_angle_ctrl(enum on_off action)
{
	uint8_t temp;
	
	if(action == enable)
	{
		temp = SPI_read_byte_ADNS9800(ADNS9800_Snap_Angle);
		temp |= (1 << 7);
		SPI_write_byte_ADNS9800(ADNS9800_Snap_Angle, temp);
	}
	else
	{		
		temp = SPI_read_byte_ADNS9800(ADNS9800_Snap_Angle);
		temp &= ~(1 << 7);
		SPI_write_byte_ADNS9800(ADNS9800_Snap_Angle, temp);
	}
}

//Sets the limits (bounds), given in Frame_Period_MAX_Bound, Frame_Period_MIN_Bound and Shutter_MAX_Bound for automatic frame rate and shutter control (auto).
//Params - desired values of the bounds. Possible values:
//		1. Frame_Period_MAX_Bound, Frame_Period_MIN_Bound -- 0x0FA0...0x61A8. (Frame Rate = 50MHz/Frame_Period_X_X).
//		2. Shutter_MAX_Bound -- Frame_Period_MAX_Bound >= Frame_Period_MIN_Bound + Shutter_MAX_Bound. (Shutter Value = (1/50MHz)*Shutter_MAX_Bound.)
//Def. values (after reset, satisfies conditions): Frame_Period_MAX_Bound = 0x5DC0 (2083fps), Frame_Period_MIN_Bound = 0x0FA0 (12000fps), 
//		Shutter_MAX_Bound = 0x4E20 (0.4 ms).
//After reset ADNS always runs in auto mode.
void ADNS9800_CONF_bounds_auto(uint16_t Frame_Period_MAX_Bound, uint16_t Frame_Period_MIN_Bound, uint16_t Shutter_MAX_Bound)
{
	
	//Write to Shutter_Max_Bound_Lower
	SPI_write_byte_ADNS9800(ADNS9800_Shtr_Max_Lower, BYTE_L(Shutter_MAX_Bound));
	//Write to Shutter_Max_Bound_Upper
	SPI_write_byte_ADNS9800(ADNS9800_Shtr_Max_Upper, BYTE_H(Shutter_MAX_Bound));
	
	//Write to Frame_Period_MIN_Bound_Lower
	SPI_write_byte_ADNS9800(ADNS9800_Frm_Prd_Min_Lower, BYTE_L(Frame_Period_MIN_Bound));
	//Write to Frame_Period_MIN_Bound_Upper
	SPI_write_byte_ADNS9800(ADNS9800_Frm_Prd_Min_Upper, BYTE_H(Frame_Period_MIN_Bound));
	
	//Write to Frame_Period_MAX_Bound_Lower
	//Writing to this register enables data in previous two registers.
	SPI_write_byte_ADNS9800(ADNS9800_Frm_Prd_Max_Lower, BYTE_L(Frame_Period_MAX_Bound));
	//Write to Frame_Period_MAX_Bound_Upper
	SPI_write_byte_ADNS9800(ADNS9800_Frm_Prd_Max_Upper, BYTE_H(Frame_Period_MAX_Bound));
	
	//Wait for two frame periods.
	delay_micro_s(2*FRAME_PERIOD);
	
	return;
}

/* For function switch_to_auto from manual mode.
//Enables automatic setting of Frame Period (Fixed_FR disabled) and Shutter Value (AGC enabled);
//In this mode, the limits, given in Frame_Period_MAX_Bound, Frame_Period_MIN_Bound and Shutter_MAX_Bound are enabled again.
//Params - desired values of the limits. Possible values:
//		1. Frame_Period_MAX_Bound, Frame_Period_MIN_Bound -- 0x0FA0...0x61A8. (Frame Rate = 50MHz/Frame_Period_X_X).
//		2. Shutter_MAX_Bound -- Frame_Period_MAX_Bound >= Frame_Period_MIN_Bound + Shutter_MAX_Bound. (Shutter Value = (1/50MHz)*Shutter_MAX_Bound.)
//Def. values (after reset) that satisfies conditions: Frame_Period_MAX_Bound = 0x5DC0 (2083fps), Frame_Period_MIN_Bound = 0x0FA0 (12000fps), 
//		Shutter_MAX_Bound = 0x4E20 (0.4 ms).

	uint8_t reg_value;

	//Load Configuration_II register.
	reg_value = SPI_read_byte_ADNS9800(ADNS9800_Conf_II);
	//Clear #4 bit (NAGC, enable auto gain) in Configuration_II.
	//Clear #3 bit (Fixed_FR, enable auto frame rate).
	reg_value &= ~(1 << 3 | 1 << 4);
	//Store Configuration_II value.
	SPI_write_byte_ADNS9800(ADNS9800_Conf_II, reg_value);

*/

/*
//Disables AGC and sets shutter value in manual mode.
void ADNS9800_CONF_set_manual_shtr(uint16_t shutter_value, enum sw_upd )
{
	uint8_t temp;
	
	//Disable AGC (Auto Gain Conrol???)
	temp = SPI_read_byte_ADNS9800(ADNS9800_Conf_II);
	temp |= (1 << 4); 
	SPI_write_byte_ADNS9800(ADNS9800_Conf_II, temp);
	
	Not finished yet!
}
*/


/* Movement data OUTput */

//Reads Motion register, and filles struct fields by movement data. 
//If motion from last report has occured - returns 1 (or 0 otherwise).
//Function also filles struct fields by state of some Motion register's bits.
uint8_t ADNS9800_OUT_movement(ADNS9800_out_t * data_out)
{
	uint8_t motion, temp, mot_flag;
	uint16_t temp16;
	
	motion = SPI_read_byte_ADNS9800(ADNS9800_Motion); //Reading this reg. freezes data in all Delta_X/Y regs.
	
	//If motion has occured, we should read other (data) registers.
	if(motion & 0x80) //if MOT bit = 1
	{
		temp = SPI_read_byte_ADNS9800(ADNS9800_Delta_X_L);
		temp16 = (uint16_t) temp;
		temp = SPI_read_byte_ADNS9800(ADNS9800_Delta_X_H);
		temp16 |= (((uint16_t) temp) << 8);
		data_out->Delta_X = (int16_t) temp16; //Data_X is signed value.

		temp = SPI_read_byte_ADNS9800(ADNS9800_Delta_Y_L);
		temp16 = (uint16_t) temp;
		temp = SPI_read_byte_ADNS9800(ADNS9800_Delta_Y_H);
		temp16 |= (((uint16_t) temp) << 8);
		data_out->Delta_Y = (int16_t) temp16; //Data_Y is also signed value.
		
		mot_flag = 1; //Motion has occured from last reading.
	}	
	else //No motion has occured.
	{ 
		data_out->Delta_X = 0;
		data_out->Delta_Y = 0;
		mot_flag = 0; 
	}
		//We suppose (by don't know exactly!) that Motion[6] is FAULT bit and Motion[5] is LP_Valid bit.
		if(motion & (1 << 6))
			data_out->LASER_fault = fault;
		else
			data_out->LASER_fault = good;
		
		if(motion & (1 << 5))
			data_out->LP_Valid = good;
		else
			data_out->LP_Valid = fault;
		
		//Determine OP_modes
		temp = (motion & 0x06) >> 1;
		data_out->OP_Mode = temp;
		
		return mot_flag;
}


/* TeST */

//Returns Laser_NEN status (from LASER_CTRL0 reg.) and enables/disables laser continuous ON mode.
//After reset, laser is NOT in cotinuous mode. Also, reading Motion reg. for ex. by OUT functions,
//	disbles continuous mode.
enum on_off ADNS9800_TST_Laser_Control(enum on_off laser_cont_on)
{
	uint8_t temp;
	
	temp = SPI_read_byte_ADNS9800(ADNS9800_LASER_CTRL0);
	if(laser_cont_on == enable) //If we should turn laser to continuosly ON mode.
	{
		temp &= ~(0x0E);	//Clear [3,2,1] bits.
		temp |= (1 << 2); //Set [2] bit to 1.
	}
	else
		temp &= ~(0x0E);
	SPI_write_byte_ADNS9800(ADNS9800_LASER_CTRL0, temp);
	
	if(temp & (1 << 0)) //If bit [0] == 1
		return disable; //LASER_NEN force disabled.
	else
		return enable; //LASER_NEN normal.
}

//Test Observation register.
//Returns 'good' if chip is running SROM code and 'fault' otherwise.
enum fault ADNS9800_TST_SROM_running(void)
{
	uint8_t temp;
	
	SPI_write_byte_ADNS9800(ADNS9800_Observation, 0x00);
	//Wait for one frame periods.
	delay_micro_s(FRAME_PERIOD);
	temp = SPI_read_byte_ADNS9800(ADNS9800_Observation);
	
	if(temp & (1 << 6))
		return good; //Chip is running SROM code.
	else
		return fault; //Chip isn't running SROM code.
}

//Starts internal CRC SROM test procedure, results valid only after SROM has downloaded.
//Also reads Motion register, hence clears residual motion for _OUT_ funtions.
//During CRC test, navigation is halted, SPI should not be used.
//Returns 'good' if SROM CRC is good and 'fault' otherwise.
//Params: *Laser_FAULT_p becames 'good', if XY_LASER pin is not shorted to GND
//	and 'fault', otherwise.
enum fault ADNS9800_TST_SROM_CRC(enum fault * Laser_FAULT_p)
{
	uint8_t temp;
	
	temp = SPI_read_byte_ADNS9800(ADNS9800_Motion);
	if(temp & (1 << 6)) //Motion[FAULT] = 1 if fault detected.
		*Laser_FAULT_p = fault;
	else
		*Laser_FAULT_p = good;
	
	SPI_write_byte_ADNS9800(ADNS9800_SROM_Enable, 0x15);
	delay_micro_s(10e3); //Wait at least 10ms.
	temp = SPI_read_byte_ADNS9800(ADNS9800_Data_Out_Lower);
	if(temp != 0xEF)
		return fault;
	temp = SPI_read_byte_ADNS9800(ADNS9800_Data_Out_Upper);
	if(temp != 0xBE)
		return fault;
	return good;
}

/*** Frame Capture (FrmCpt)***/
//This mode is disabled after reset.

/*** ShutDown ***/
//This mode is disabled after reset.

/*** Rest modes (REST) ***/
//This mode is disabled after reset.

/*** WakeUp ***/
//This mode is not used after reset.
