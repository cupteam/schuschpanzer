#ifndef __ADNS9800_DRV_H
#define __ADNS9800_DRV_H

/** Debug settings for ADNS9800 driver lib. **/
#define ADNS9800_DEBUG //Test parameter asserts. Should be commented after debugging.

void Lib_Error_Message(char * file_name, uint32_t line_num); //Function for error reporting through UART. This function is not needed after debugging finished.
#define LIB_ERROR_CALL Lib_Error_Message( __FILE__, __LINE__ ); // We suppose, that UART has already enabled.

/* ADNS-9800 Register's adresses */
#define	ADNS9800_Product_ID	0x00
#define	ADNS9800_Revision_ID	0x01
#define	ADNS9800_Motion	0x02
#define	ADNS9800_Delta_X_L	0x03
#define	ADNS9800_Delta_X_H	0x04
#define	ADNS9800_Delta_Y_L	0x05
#define	ADNS9800_Delta_Y_H	0x06
#define	ADNS9800_SQUAL	0x07
#define	ADNS9800_Pixel_Sum	0x08
#define	ADNS9800_Max_Pixel	0x09	//Maximum_Pixel
#define	ADNS9800_Min_Pixel	0x0a	//Minimum_Pixel
#define	ADNS9800_Shtr_Lower	0x0b	//Shutter_Lower
#define	ADNS9800_Shtr_Upper	0x0c	//Shutter_Upper
#define	ADNS9800_Frm_Prd_Lower	0x0d	//Frame_Period_Lower
#define	ADNS9800_Frm_Prd_Upper	0x0e	//Frame_Period_Upper
#define	ADNS9800_Conf_I	0x0f	//Configuration_I
#define	ADNS9800_Conf_II	0x10	//Configuration_II
#define	ADNS9800_Frame_Capture	0x12
#define	ADNS9800_SROM_Enable	0x13
#define	ADNS9800_Run_DnShft	0x14	//Run_Downshift
#define	ADNS9800_Rest1_Rate	0x15
#define	ADNS9800_Rest1_DnShft	0x16	//Rest1_Downshift
#define	ADNS9800_Rest2_Rate	0x17
#define	ADNS9800_Rest2_DnShft	0x18	//Rest2_Downshift
#define	ADNS9800_Rest3_Rate	0x19
#define	ADNS9800_Frm_Prd_Max_Lower	0x1a	//Frame_Period_Max_Bound_Lower
#define	ADNS9800_Frm_Prd_Max_Upper	0x1b	//Frame_Period_Max_Bound_Upper
#define	ADNS9800_Frm_Prd_Min_Lower	0x1c	//Frame_Period_Min_Bound_Lower
#define	ADNS9800_Frm_Prd_Min_Upper	0x1d	//Frame_Period_Min_Bound_Upper
#define	ADNS9800_Shtr_Max_Lower	0x1e	//Shutter_Max_Bound_Lower
#define	ADNS9800_Shtr_Max_Upper	0x1f	//Shutter_Max_Bound_Upper
#define	ADNS9800_LASER_CTRL0	0x20

#define	ADNS9800_Observation	0x24
#define	ADNS9800_Data_Out_Lower	0x25
#define	ADNS9800_Data_Out_Upper	0x26

#define	ADNS9800_SROM_ID	0x2a
#define	ADNS9800_Lift_Dtct_Thr	0x2e	//Lift_Detection_Thr
#define	ADNS9800_Conf_V	0x2f	//Configuration_V

#define	ADNS9800_Conf_IV	0x39	//Configuration_IV
#define	ADNS9800_Pwr_Up_Rst	0x3a	//Power_Up_Reset
#define	ADNS9800_Shutdown	0x3b

#define	ADNS9800_Inv_Prod_ID	0x3f	//Inverse_Product_ID

#define	ADNS9800_Snap_Angle	0x42

#define	ADNS9800_Motion_Burst	0x50
#define	ADNS9800_SROM_Load_Brst	0x62	//SROM_Load_Burst
#define	ADNS9800_Pixel_Burst	0x64


enum on_off {disable, enable};
enum fault {good, fault};

/* Two structs for two alternative ways for obtaining OUT data: sequential reading (ADNS9800_out_t) 
and burst reading (ADNS9800_motion_burst_data_t). Burst reading also gives all STATistic information.*/

//Struct for motion data out and Motion reg. state.
typedef struct {
	enum fault LASER_fault; //If XY_LASER is shorted to GND.
	enum fault LP_Valid; //If Laser Power register has complementry values (good) ot not (fault).
	uint8_t OP_Mode; //Operating mode of sensor - 0x00 (Run), 0x01 (Rest 1), 0x02 (Rest 2), 0x03 (Rest 3).
	//Frame_Pix_First bit not used in this structure.
	
	//Movement values in counts.
	int16_t Delta_X; //X movement from last report. Abs. value is determined by resolution.
	int16_t Delta_Y; //Y movement from last report. Abs. value is determined by resolution.
} ADNS9800_out_t;

//Struct for burst reading of Motion data.
//14 sensor's data out registers
typedef struct {
	uint8_t Motion;
	uint8_t Observation;
	uint8_t Delta_X_L;
	uint8_t Delta_X_H;
	uint8_t Delta_Y_L;
	uint8_t Delta_Y_H;
	uint8_t SQUAL;
	uint8_t Pixel_Sum;
	uint8_t Maximum_Pixel;
	uint8_t Minimum_Pixel;
	uint8_t Shutter_Upper;
	uint8_t Shutter_Lower;
	uint8_t Frame_Period_Upper;
	uint8_t Frame_Period_Lower;
} ADNS9800_motion_burst_data_t;

//Struct for info about sensor.
//3 bytes.
typedef struct{
	uint8_t Product_ID;
	uint8_t Revision_ID;
	uint8_t SROM_ID; //0x00 if firmware haven't been downloaded.
} ADNS9800_info_t;

//Struct for STATistic data about frame image and it's processing.
//12 bytes.
typedef struct{
	uint16_t SQUAL4; //SQUAL reg. multiplied by 4.
	uint32_t Pixel_Sum512; //Pixel_Sum reg. multiplied by 512. 
	uint8_t Max_Pixel; //Seven bit uint.
	uint8_t Min_Pixel; //Seven bit uint.
	uint16_t Shutter;
	uint16_t Frame_Period; 
} ADNS9800_STAT_t;

//WARNING: In general case, functions below performs no checks of their parameters, so user is responsible for correctness.

/***** ON *****/

//Powers Up sensor and Loads firmware (Shadow ROM).
//Returns SROM_ID or 0x00 (if SROM loading has failed).
//Not enables Laser if SROM_ID = 0x00.
int8_t ADNS9800_PWRON(void);


/***** CONF *****/

//Sets the limits (bounds), given in Frame_Period_MAX_Bound, Frame_Period_MIN_Bound and Shutter_MAX_Bound for automatic frame rate and shutter control (auto).
//Params - desired values of the bounds. Possible values:
//		1. Frame_Period_MAX_Bound, Frame_Period_MIN_Bound -- 0x0FA0...0x61A8. (Frame Rate = 50MHz/Frame_Period_X_X).
//		2. Shutter_MAX_Bound -- Frame_Period_MAX_Bound >= Frame_Period_MIN_Bound + Shutter_MAX_Bound. (Shutter Value = (1/50MHz)*Shutter_MAX_Bound.)
//Def. values (after reset, satisfies conditions): Frame_Period_MAX_Bound = 0x5DC0 (2083fps), Frame_Period_MIN_Bound = 0x0FA0 (12000fps), 
// Shutter_MAX_Bound = 0x4E20 (0.4 ms).
//After reset ADNS-9800 always runs in auto mode.
void ADNS9800_CONF_bounds_auto(uint16_t Frame_Period_MAX_Bound, uint16_t Frame_Period_MIN_Bound, uint16_t Shutter_MAX_Bound);

//Sets Lift Detection Threshold (see p.38 in DS, Lift_Detection_Thr register).
//Def. value -- 0x10 (2.4mm). Max value -- 0x1F.
void ADNS9800_CONF_set_lift_detect_thrsh(uint8_t threshold);

//Sets resolution for X and Y axes. Resolution = 200 cpi * res_x (or res_y for Y axe).
//Max value for res_x and res_y = 0x29 (eq. 8200 cpi resolution). Def value = 0x09 (eq. 1800 cpi) for both.
void ADNS9800_CONF_resolution(uint8_t res_x, uint8_t res_y);

//Enables/disables Angle Snapping function. (see p. 39 in DS, Snap_Angle register).
//Def - disabled.
void ADNS9800_CONF_snap_angle_ctrl(enum on_off action);


/***** TST *****/

//Returns Laser_NEN status (from LASER_CTRL0 reg.) and enables/disables laser continuous ON mode.
//After reset, laser is NOT in cotinuous mode. Also, reading Motion reg. for ex. by OUT functions,
//	disbles continuous mode.
enum on_off ADNS9800_TST_Laser_Control(enum on_off laser_cont_on);

//Test Observation register.
//Returns 'good' if chip is running SROM code and 'fault' otherwise.
enum fault ADNS9800_TST_SROM_running(void);

//Starts internal CRC SROM test procedure, results valid only after SROM has downloaded.
//Also reads Motion register, hence clears residual motion for _OUT_ funtions.
//During CRC test, navigation is halted, SPI should not be used.
//Returns 'good' if SROM CRC is good and 'fault' otherwise.
//Params: *Laser_FAULT_p becames 'good', if XY_LASER pin is not shorted to GND
//	and 'fault', otherwise.
enum fault ADNS9800_TST_SROM_CRC(enum fault * Laser_FAULT_p);


/***** INFO *****/

//Reads INFO data from registers.
void ADNS9800_INFO(ADNS9800_info_t * info_struct);


/***** STATistic *****/

//Reads registers with STATistic data about frame image and it's processing.
//Details are in struct ADNS9800_STAT_t definition.
void ADNS9800_STAT(ADNS9800_STAT_t * stat_struct);


/***** OUT *****/

//Reads Motion register, and filles struct fields by movement data. 
//If motion from last report has occured - returns 1 (and 0, otherwise).
//Function also filles other struct filds by some Motion register's bits.
uint8_t ADNS9800_OUT_movement(ADNS9800_out_t * data_out);

/*** Frame Capture (FrmCpt)***/
//This mode is disabled after reset.

/*** ShutDown ***/
//This mode is disabled after reset.

/*** Rest modes (REST) ***/
//This mode is disabled after reset.

/*** WakeUp ***/
//This mode is not used after reset.

#endif
