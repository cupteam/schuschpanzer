#ifndef __UTILS_H
#define __UTILS_H

//For uint16_t and int16_t
#define BYTE_H(x) ((uint8_t)(((uint16_t)(x)) >> 8))
#define BYTE_L(x) ((uint8_t)(((uint16_t)(x)) & 0x00FF))

//To modify only decided bits in any reg. All parameters: VARIABLE, MASK, SHIFTED_VAL must have equal size.
#define MODIFY_BIT_FIELD(VARIABLE, MASK, SHIFTED_VAL)  (VARIABLE) = (((VARIABLE) & (~(MASK))) | (SHIFTED_VAL))


//Two functions for delays. Not very pricision. May be deleted by compilers. Must be tested for working when compiler/optimization changes.
/* TODO
//Limits: SystemCoreClock - 20MHz ... 400 MHz, pause_length = 200ns ... 1.000.000ns (1 ms).
uint32_t delay_nano_s(uint32_t pause_length);
	Not realized!
*/

//Limits: SystemCoreClock - 20MHz ... 400 MHz, pause_length = 1us ... 1.000.000us (1s).
//delay_micro_s(1) is eq. about 1.5 us. Hence delay_micro_s(pause_length) give pause_length + 0.5 us delay.
uint32_t delay_micro_s(uint32_t pause_length);

#endif
