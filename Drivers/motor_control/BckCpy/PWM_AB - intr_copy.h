#ifndef __PWM_AB_H
#define __PWM_AB_H


/** Debug settings for ADNS9800 driver lib. **/
#define PWM_AB_DEBUG //Test parameter asserts. Should be commented after debuging.

void Lib_Error_Message(char * file_name, uint32_t line_num); //Function for error reporting through UART. This function is not needed after debugging finished.
#define LIB_ERROR_CALL Lib_Error_Message( __FILE__, __LINE__ ); // We suppose, that UART has already enabled.


//Max PWM frequency = 100 kHz.
//We suppose f(CK_INT) = 72 MHz.

#define TIM_INT_PRIOR 0x80 //Interrupt priority for PWM UEV interrupt. Must be the highest used in system. Possible values: (max.prior.)0x00,0x10,...,0xF0(min.prior.).

//Init timer for PWM (both channels) and pins as direction and standby outputs, i.e. prepares MCU peripherals for motor control.
//Default motor's state after init: IC driver disabled, PWMs = 0 (eq. 0%), motor is in free rotating (driver IC is disabled).
//Parameter: max_PWM_value ( =< 1000) is maximum value, that can be set by PWM_set_value();
//	Higer values will be replaced by max_PWM_value (saturation effect).
//Should be used during system initialisation.
void PWM_init(uint32_t max_PWM_value);

//Enables motor driver IC for motor actions. Should be called between PWM_init() and other PWM_ funtions.
//After: motor is in short brake mode. IC driver is active and dissipates power.
void PWM_enable(void);

//Sets PWM duty cycle, where (0 is 0%, 1000 is 100%).
//Parameters: Sign of Ch_x_value - direction of rotation: >0 CW, 0< CCW.
//PWM value := |Ch_x_value| , if |Ch_x_value| <= max_PWM, else PWM value := max_PWM.
//	zero value - short brake (fast, through dir. outputs).
void PWM_set_value(int32_t Ch_A_value, int32_t Ch_B_value);

//Disables motor driver IC (standby mode) for current reduction ( ICC+IM < 2uA).
//All direction and PWM sygnals are ignored. Motor motor is in free rotating mode.
void PWM_disable(void);

//Handler of UEV interrupt of TIM3.
void TIM3_IRQHandler(void);

#endif
