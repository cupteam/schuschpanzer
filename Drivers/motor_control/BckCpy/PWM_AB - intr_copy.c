#include "stm32f4xx.h"
#include "PWM_AB.h"
#include "stm32f411_bit_band.h"
#include "utils.h"


//Max value, possible for PWM parameter (greater values gives saturation).
//Value can be decreased by PWM_init() to prevent motor overload, if power supply voltage is higer than recommended for motor.
uint32_t max_PWM = 1000; //(e.q. permanent HIGH at output level or 100% PWM, or NO limitations)

volatile uint32_t col_flag = 1; //For detection of collisions. Now we're outside of PWM_set_value.

//Global variables for data transfer to interrupt.
//i_xxx_buff[0] - Ch.A, i_xxx_buff[1] - Ch.B.
volatile int32_t i_dir_buff[2]; //+1 - CW, 0 - brake, -1 - CCW.
volatile int32_t i_pwm_buff[2]; //PWM values.

volatile enum {bef_init = 1, disabled, enabled, run} pwm_state; //State machine. 
	//1 - Before init...(). Standby mode, free rotating.
	//2 - Disabled. Standby mode, free rotating (for energy saving).
	//3 - Enabled. Short brake mode. Motor driver IC is fully enabled, but motor is stopped.
	//4 - Run. The motor is rotating.

//Max PWM frequency for driver IC (TB6612FNG, Toshiba) = 100 kHz.
//We suppose f(CK_INT) = 72 MHz.
//We will use fPWM < 540 Hz, 1000 counts.
//	NOTE: Only from values greater 100 (10.0%) PWM we can suppose linear relation between PWM duty cycle and current draw.
//  Used motor has L = 2mH, R = 10.8 Ohm. (See: https://www.precisionmicrodrives.com/application-notes/ab-022-pwm-frequency-for-linear-motion-control)
// Hence, T(PSC) = 134.

//Inits timer for PWM (both channels) and pins for direction, standby outputs.
//Default motor's state after init: IC driver disabled, PWMs = 0 (eq. 0%), motor is in free rotating (driver IC is disabled).
//Parameter: max_PWM_value ( =< 1000) is maximum value, that can be set by PWM_set_value();
//	Higer values will be replaced by max_PWM_value (saturation effect).
void PWM_init(uint32_t max_PWM_value)
{
	
	uint32_t temp32;
	uint16_t temp16;
	
	//This func can be called for "bef_init" state only.
#ifdef PWM_AB_DEBUG		
	if(pwm_state != bef_init)
		LIB_ERROR_CALL;
#endif
	//Change state.
	pwm_state = disabled;
	
	//Set maximum PWM duty cycle for PWM_set_value() to prevent motor overvoltage.
	if(max_PWM_value < max_PWM)
		max_PWM = max_PWM_value;
	
	//Configuring GPIO as direction pins (IN1x,IN2x) for both channels.
	//Default state after config. - free rotating (IN1x = 0, IN2x = 0).
	//In1A - PC10, In2A - PC11. Motor A.
	//In1B - PC12, In2B - PD2. Motor B.
	
	//Enable clocking of GPIOC, GPIOD.
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	
	//PC10,11,12 - outputs, low speed, push-pull, no pulling, = 0.

	temp32 = GPIOC->PUPDR; //No pulling (i.e. floating) - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_PUPDR_PUPDR10 | GPIO_PUPDR_PUPDR11 | GPIO_PUPDR_PUPDR12, 0);
	GPIOC->PUPDR = temp32;
	
	temp32 = GPIOC->OSPEEDR; //Low speed < 2 MHz - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_OSPEEDER_OSPEEDR10 | GPIO_OSPEEDER_OSPEEDR11 | GPIO_OSPEEDER_OSPEEDR12, 0);
	GPIOC->OSPEEDR = temp32;
	
	temp32 = GPIOC->OTYPER;	//Push-pull - 0.
	MODIFY_BIT_FIELD(temp32, GPIO_OTYPER_ODR_10 | GPIO_OTYPER_ODR_11 | GPIO_OTYPER_ODR_12, 0);
	GPIOC->OTYPER = temp32;
	
	temp32 = GPIOC->ODR; //Output reg. - PC10,11,12 = 0.
	MODIFY_BIT_FIELD(temp32, GPIO_ODR_ODR_10 | GPIO_ODR_ODR_11 | GPIO_ODR_ODR_12, 0);
	GPIOC->ODR = temp32;
	
	temp32 = GPIOC->MODER; //Mode - output (01).
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER10 | GPIO_MODER_MODER11 | GPIO_MODER_MODER12, GPIO_MODER_MODER10_0 | GPIO_MODER_MODER11_0 | GPIO_MODER_MODER12_0);
	GPIOC->MODER = temp32;
	
	
	//PD2 - output, low speed, push-pull, no pulling, = 0.
	
	temp32 = GPIOD->PUPDR; //No pulling (i.e. floating) - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_PUPDR_PUPDR2, 0);
	GPIOD->PUPDR = temp32;
	
	temp32 = GPIOD->OSPEEDR; //Low speed < 2 MHz - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_OSPEEDER_OSPEEDR2, 0);
	GPIOD->OSPEEDR = temp32;
	
	temp32 = GPIOD->OTYPER;	//Push-pull - 0.
	MODIFY_BIT_FIELD(temp32, GPIO_OTYPER_ODR_2, 0);
	GPIOD->OTYPER = temp32;
	
	temp32 = GPIOD->ODR; //Output reg. - PD2 = 0.
	MODIFY_BIT_FIELD(temp32, GPIO_ODR_ODR_2, 0);
	GPIOD->ODR = temp32;
	
	temp32 = GPIOD->MODER; //Mode - output (01).
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER2, GPIO_MODER_MODER2_0);
	GPIOD->MODER = temp32;
	
	
	//Configuring #STBY - PB6 pin.
	//Enable clocking of GPIOB.
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	
	//PB6 - output, low speed, push-pull, no pulling, = 0.
	
	temp32 = GPIOB->PUPDR; //No pulling (i.e. floating) - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_PUPDR_PUPDR6, 0);
	GPIOB->PUPDR = temp32;
	
	temp32 = GPIOB->OSPEEDR; //Low speed < 2 MHz - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_OSPEEDER_OSPEEDR6, 0);
	GPIOB->OSPEEDR = temp32;
	
	temp32 = GPIOB->OTYPER;	//Push-pull - 0.
	MODIFY_BIT_FIELD(temp32, GPIO_OTYPER_ODR_6, 0);
	GPIOB->OTYPER = temp32;
	
	temp32 = GPIOB->ODR; //Output reg. - PB6 = 0.
	MODIFY_BIT_FIELD(temp32, GPIO_ODR_ODR_6, 0);
	GPIOB->ODR = temp32;
	
	temp32 = GPIOB->MODER; //Mode - output (01).
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER6, GPIO_MODER_MODER6_0);
	GPIOB->MODER = temp32;
	
	/*Configuring timer TIM3 for PWM on Ch1/Ch2.*/
	//Configuring GPIO pins for alternative function.
	//PWM_A (TIM3_CH1) - PB4.
	//PWM_B (TIM3_CH2) - PB5.
	
		/*	For other peripherals:
	� Configure the desired I/O as an alternate function in the GPIOx_MODER register
			NOTE: In this moment pin became an output. And if AF is AF00 (def. after reset), 
			output switches to LOW state until desired AFx will be choosed! Hence this 
			action better make the last (third) in this intialisation.
	� Select the type, pull-up/pull-down and output speed via the GPIOx_OTYPER,
		GPIOx_PUPDR and GPIOx_OSPEEDER registers, respectively
	� Connect the I/O to the desired AFx in the GPIOx_AFRL or GPIOx_AFRH register 
	*/
	
	//PB4, PB5 - AF02 (outputs), low speed, push-pull, no pulling.

	temp32 = GPIOB->PUPDR; //No pulling (i.e. floating) - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_PUPDR_PUPDR4 | GPIO_PUPDR_PUPDR5, 0);
	GPIOB->PUPDR = temp32;
	
	temp32 = GPIOB->OSPEEDR; //Low speed < 2 MHz - 00.
	MODIFY_BIT_FIELD(temp32, GPIO_OSPEEDER_OSPEEDR4 | GPIO_OSPEEDER_OSPEEDR5, 0);
	GPIOB->OSPEEDR = temp32;
	
	temp32 = GPIOB->OTYPER;	//Push-pull - 0.
	MODIFY_BIT_FIELD(temp32, GPIO_OTYPER_ODR_4 | GPIO_OTYPER_ODR_5, 0);
	GPIOB->OTYPER = temp32;
	
	temp32 = GPIOB->AFR[0]; //PB4, PB5 - AF02.
	MODIFY_BIT_FIELD(temp32, (((uint32_t) 0xF) << 16) | (((uint32_t) 0xF) << 20),  (((uint32_t) GPIO_AF_TIM3) << 16) | (((uint32_t) GPIO_AF_TIM3) << 20));
	GPIOB->AFR[0] = temp32;
	
	temp32 = GPIOB->MODER; //Mode - AF (10).
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER4 | GPIO_MODER_MODER5, GPIO_MODER_MODER4_1 | GPIO_MODER_MODER5_1);
	GPIOB->MODER = temp32;
	
	//Configuring TIM3 for PWM (we suppose counter disabled: CCEN = 0).
	//Configuring Time Base Unit - Period, Prescaler, Clock source and Slave modes.
	//1. Set prescaler divide ratio to 134 (_PSC = 133).
	TIM3->PSC = 133; //Needs in UEV event.
	
	//2. Set Auto Reload Register to 999. (TIM3 is 16-bit counter).
	TIM3->ARR = 999; //Also needs in UEV event, because we'll enable preload mode.
	
	//3. Clear counter register (CNT), also it will be cleared by UEV event.
	TIM3->CNT = 0;
		
		//Configuration of control registers.
	//1. TIMx_CR1
	temp16 = 0;
	//.ARPE = 1; _ARR register is buffered.
	//.CMS = 00; Edge-aligned (not center) mode.
	//.DIR = 0; Up counting.
	//.OPM = 0; Counter isn't stopped at UEV.
	//.URS = 0; All UEV sources is available.
	//.UDIS = 0; UEV event is enabled.
	MODIFY_BIT_FIELD(temp16, TIM_CR1_ARPE, TIM_CR1_ARPE);
	TIM3->CR1 = temp16;
	
	//2. TIMx_CR2
	temp16 = 0;
	//.TI1S = 0; Disable XOR of TI1, TI2, TI3 inputs.
	TIM3->CR2 = temp16;
	
	//3. TIMx_SMCR
	temp16 = 0;
	//.ECE = 0; External clock mode 2 is disabled.
	//.ETPS = 00; External trigger prescaler is OFF.
	//.MSM = 0; No action on TRGI.
	//.SMS = 000; Slave mode disabled (If CEN = 1, prescaler is clocked directly by internal clock).
	//TS not used now.
	TIM3->SMCR = temp16;
	
	//Config. channels for PWM.
	//0. Disable all channels (if enabled).
	temp16 = TIM3->CCER;
	//TIMx_CCER.CC1E = 0;
	//TIMx_CCER.CC2E = 0;
	//TIMx_CCER.CC3E = 0;
	//TIMx_CCER.CC4E = 0;
	MODIFY_BIT_FIELD(temp16, TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E, 0);
	TIM3->CCER = temp16;
	
	//1. Config. _CCMR1 (bits in output mode, x = 1 or 2)
	temp16 = 0;
	//.OCxCE = 0; OCxRef is not affected by the ETRF input.
	//.OCxM = 110; PWM 1. OCxRef = 1 until _CNT >= _CCRx.
	//.OCxPE = 0; //Preload of _CCRx disabled (we use UEV interrupt) .
	//.OCxFE = 0; //Normal speed.
	//.CCxS = 00; //Channel x is output.
	temp16 = TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2;
	//temp16 |= TIM_CCMR1_OC1PE | TIM_CCMR1_OC2PE;
	TIM3->CCMR1 = temp16;
	
	//2. Config. _CCER.
	//.CCxP/.CCxNP = 0. CCx output active high.
	TIM3->CCER = 0;
	
	//Load CCRx values.
	TIM3->CCR1 = 0; //PWM value for Ch.1.
	TIM3->CCR2 = 0; //PWM value for Ch.2.
	
	//Generate UEV
	//Sw. generation of UEV event.
	//TIM3_EGR.UG = 1; //To apply settings from preload registers.
	BIT_BAND_REG_SET(TIM3_BASE, TIM_EGR, TIM_EGR_UG_BIT_NUM); //It also clears the counter CNT.
	
	//Enable channels Ch.1 and Ch.2. It enables it's outputs and output values are 0.
	temp16 = TIM3->CCER;
	//TIMx_CCER.CC1E = 1;
	//TIMx_CCER.CC2E = 1;
	//TIMx_CCER.CC3E = 0;
	//TIMx_CCER.CC4E = 0;
	MODIFY_BIT_FIELD(temp16, TIM_CCER_CC1E | TIM_CCER_CC2E, TIM_CCER_CC1E | TIM_CCER_CC2E);
	TIM3->CCER = temp16;
	
	//The last action. Counter enable.
	//TIM3_CR1.CEN = 1;	
	BIT_BAND_REG_SET(TIM3_BASE, TIM_CR1, TIM_CR1_CEN_BIT_NUM); //Counter starts.
	
	
	Config interrupt in timer:
		
	//Configure the mask bits to Enable Interrupt for line 13.
?	BIT_BAND_REG_SET(EXTI_BASE, EXTI_IMR, EXTI_IMR_LINE13_BIT_NUM);
	
	TIM3->SR &= ~(TIM_SR_UIF); //Clear UE flag (to prevent false triggering of UE interrupt).
	TIM3->DIER |= TIM_DIER_UIE; //Enable UE interrupt.
	
	/* Config NVIC for IRQ#29 (=TIM3_IRQn) */
	
	//Set priority to 0(Decimal), the highest priority (for PWM).
	NVIC_SetPriority(TIM3_IRQn, 0x0); //This interrupt can not use _From_ISR functions.
	
	//Enable interrupt IRQ#29 (=TIM3_IRQn). 
	NVIC_EnableIRQ(TIM3_IRQn);
	
	//Here we should enable global interrupts by calling __enable_irq();, but they has been enabled after reset (by default).
}



//Enables motor driver IC. Should be called between PWM_init() and any other PWM_... funtion.
//Sets state (3): motor is in short brake. IC driver is active.
void PWM_enable(void)
{
	uint32_t temp32;
	
	//This func can be called for "disabled" state only.
#ifdef PWM_AB_DEBUG	
	if(pwm_state != disabled)
		LIB_ERROR_CALL;
#endif
	//Change state.
	pwm_state = enabled;
	
	
	//Short brake (IN1x = 1, IN2x = 1, #STBY = 1).
	//In1A - PC10, In2A - PC11. Motor A.
	//In1B - PC12, In2B - PD2. Motor B.
	//#STBY - PB6 pin.
	
	//Motor A.
	temp32 = GPIOC->ODR; //Output reg.
	MODIFY_BIT_FIELD(temp32, GPIO_ODR_ODR_10 | GPIO_ODR_ODR_11, GPIO_ODR_ODR_10 | GPIO_ODR_ODR_11);
	GPIOC->ODR = temp32;
	
	//Motor B.
	BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_12_BIT_NUM);
	BIT_BAND_REG_SET(GPIOD_BASE, GPIO_ODR, GPIO_ODR_ODR_2_BIT_NUM);
	
	//#STBY.
	BIT_BAND_REG_SET(GPIOB_BASE, GPIO_ODR, GPIO_ODR_ODR_6_BIT_NUM);
	
	return;
}

//Disables motor driver IC (standby mode) for current reduction ( ICC+IM < 2uA).
//All direction and PWM sygnals are ignored. Motor is in free rotating mode.
//UEV interrupt is disabled.
void PWM_disable(void)
{
	uint32_t temp32;
	
	//This func can be called for bef_init state only.
#ifdef PWM_AB_DEBUG	
	if(pwm_state != enabled)
		LIB_ERROR_CALL;
#endif
	//Change state.
	pwm_state = disabled;
	
	//Short brake (IN1x = 0, IN2x = 0, #STBY = 0, PWM value A/B = 0).
	//In1A - PC10, In2A - PC11. Motor A.
	//In1B - PC12, In2B - PD2. Motor B.
	//#STBY - PB6 pin.
	
	//#STBY.	
	BIT_BAND_REG_CLR(GPIOB_BASE, GPIO_ODR, GPIO_ODR_ODR_6_BIT_NUM);
	
	//Disable interrupt generation.
	
	
	//Motor A.
	temp32 = GPIOC->ODR; //Output reg.
	MODIFY_BIT_FIELD(temp32, GPIO_ODR_ODR_10 | GPIO_ODR_ODR_11, 0);
	GPIOC->ODR = temp32;
	
	//Motor B.
	BIT_BAND_REG_CLR(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_12_BIT_NUM);
	BIT_BAND_REG_CLR(GPIOD_BASE, GPIO_ODR, GPIO_ODR_ODR_2_BIT_NUM);
	
	//PWMs = 0.
	TIM3->CCR1 = 0; //PWM value for Ch.1.
	TIM3->CCR2 = 0; //PWM value for Ch.2.

	return;
}

//Loads PWM and direction to hadware registers.
void PWM_load_from_buff(void)
{
	//Ch.1.
	if(i_dir_buff[0] == 1)
	{
		//Set CW direction. 
		//In1A (PC10) = 1, In2A (PC11) = 0.
		BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_10_BIT_NUM);
		BIT_BAND_REG_CLR(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_11_BIT_NUM);
		
		TIM3->CCR1 = i_pwm_buff[0]; //Ch_A_value;
	}
	else
	{
		if(i_dir_buff[0] == -1)
		{
			//Set CCW direction. 
			//In1A (PC10) = 0, In2A (PC11) = 1.
			BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_11_BIT_NUM);
			BIT_BAND_REG_CLR(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_10_BIT_NUM);
			
			TIM3->CCR1 = i_pwm_buff[0]; //Ch_A_value;
		}
		else //0 - brake.
		{
			//Set short brake. 
			//In1A (PC10) = 1, In2A (PC11) = 1.
			BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_10_BIT_NUM);
			BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_11_BIT_NUM);
			
			//Set PWM Ch.1.	
			TIM3->CCR1 = i_pwm_buff[0];
		}
	}
	
	//Ch.2.
	if(i_dir_buff[1] == 1)
	{
		//Set CW direction. 
		//In1B (PC12) = 1, In2B (PD2) = 0.
		BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_12_BIT_NUM);
		BIT_BAND_REG_CLR(GPIOD_BASE, GPIO_ODR, GPIO_ODR_ODR_2_BIT_NUM);
		
		TIM3->CCR2 = i_pwm_buff[1]; //Ch_B_value.
	}
	else
	{
		if(i_dir_buff[1] == -1)
		{
			//Set CCW direction. 
			//In1B (PC12) = 0, In2B (PD2) = 1.
			BIT_BAND_REG_SET(GPIOD_BASE, GPIO_ODR, GPIO_ODR_ODR_2_BIT_NUM);
			BIT_BAND_REG_CLR(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_12_BIT_NUM);
			
			TIM3->CCR2 = i_pwm_buff[1];
		}
		else //0 - brake.
		{
			//Set short brake. 
			//In1B (PC12) = 1, In2B (PD2) = 1.
			BIT_BAND_REG_SET(GPIOC_BASE, GPIO_ODR, GPIO_ODR_ODR_12_BIT_NUM);
			BIT_BAND_REG_SET(GPIOD_BASE, GPIO_ODR, GPIO_ODR_ODR_2_BIT_NUM);
			
			//Set PWM Ch.2.	
			TIM3->CCR2 = i_pwm_buff[1];
		}
	}
}

//Sets PWM duty cycle, where (0 is 0%, 1000 is 100%).
//Parameters: Sign of Ch_x_value - direction of rotation: >0 CW, 0< CCW.
//	zero value - short brake (through direction outputs).
//PWM value = |Ch_x_value|, if |Ch_x_value| <= max_PWM; else PWM value = max_PWM.
void PWM_set_value(int32_t Ch_A_value, int32_t Ch_B_value)
{
	
	//This func can be called for "enabled" and "run" states only.
#ifdef PWM_AB_DEBUG	
	if(pwm_state != enabled && pwm_state != run)
		LIB_ERROR_CALL;
#endif
	//Change state.
	if( Ch_A_value != 0 || Ch_B_value != 0 )
		pwm_state = run;
	else
		pwm_state = enabled;
	
	
	col_flag = 0; //We're in PWM_set_value function.
	
	/* Channel A */
	if(Ch_A_value > 0)
	{
		i_dir_buff[0] = 1; //CW.
		
		//Set PWM Ch.1.
		if(Ch_A_value > max_PWM)
			i_pwm_buff[0] = max_PWM;
		else
			i_pwm_buff[0] = Ch_A_value;
	}
	else
	{
		if(Ch_A_value < 0)
		{
			if(Ch_A_value != -2147483648) //If (- Ch_x_value exits).
				Ch_A_value = - Ch_A_value;
			else
				Ch_A_value = max_PWM;
			
			i_dir_buff[0] = -1; //CCW.
			
			//Set PWM Ch.1.
			if(Ch_A_value > max_PWM)
				i_pwm_buff[0] = max_PWM;
			else
				i_pwm_buff[0] = Ch_A_value;
		}
		else
		{
			i_dir_buff[0] = 0; //Brake.
			i_pwm_buff[0] = 0;
		}
	}
	
	/* Channel B */
	if(Ch_B_value > 0) //CW
	{
		//Set CW direction. 
		i_dir_buff[1] = 1;
		
		//Set PWM Ch.2.
		if(Ch_B_value > max_PWM)
			i_pwm_buff[1] = max_PWM;
		else
			i_pwm_buff[1] = Ch_B_value;
	}
	else
	{
		if(Ch_B_value < 0) //CCW
		{
			if(Ch_B_value != -2147483648) //If (- Ch_x_value exits).
				Ch_B_value = - Ch_B_value;
			else
				Ch_B_value = max_PWM;
			
			//Set CCW direction. 
			i_dir_buff[1] = -1;
			//Set PWM Ch.2.
			if(Ch_B_value > max_PWM)
				i_pwm_buff[1] = max_PWM;
			else
				i_pwm_buff[1] = Ch_B_value;
		}
		else //Brake
		{
			i_dir_buff[1] = 0;
			//Set PWM Ch.2.	
			i_pwm_buff[1] = 0;
		}
	}

	if(col_flag == 1) //Collision against interrupt routine has occured, we have to load values "manually".
	{
		PWM_load_from_buff(); //Load values for direction and PWM.
	}
	
	col_flag = 1; //We are going away.
}

//Handler of UEV interrupt of TIM3.
void TIM3_IRQHandler(void) //#29, TIM3 global interrupt.
{
	TIM3->SR &= ~(TIM_SR_UIF); //Clear UE flag.
	if(col_flag == 0) //If we collide with PWM_set_value.
	{
		col_flag = 1; //Inform PWM_set_value about collision.
		return; //Nothing to do.
	}
	else
	{
		PWM_load_from_buff(); //Load values of direction and PWM.
		return;
	}
}