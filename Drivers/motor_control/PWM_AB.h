#ifndef __PWM_AB_H
#define __PWM_AB_H


/** Debug settings for ADNS9800 driver lib. **/
#define PWM_AB_DEBUG //Test parameter asserts. Should be commented after debuging.

void Lib_Error_Message(char * file_name, uint32_t line_num); //Function for error reporting through UART. This function is not needed after debugging finished.
#define LIB_ERROR_CALL Lib_Error_Message( __FILE__, __LINE__ ); // We suppose, that UART has already enabled.

//If we want to be shure: all new PWM values has REALLY applied in PWM_set_value().
//It uses TIM3 interrupt (priority level is 8) to set pwm_ready flag to 1 when values setted by PWM_set_value() function has applied.
#define USE_UPD_EVENT 

//Max PWM frequency = 100 kHz.
//We suppose f(CK_INT) = 72 MHz.

//#define TIM_INT_PRIOR 0x80 //Interrupt priority for PWM UEV interrupt. Must be the highest used in system. Possible values: (max.prior.)0x00,0x10,...,0xF0(min.prior.).

//Inits timer for PWM (both channels) and pins for direction, standby outputs.
//Default motor's state after init: Power saving: PWMs = 0 (eq. 0%), motor is in free rotating (driver IC is disabled), all motor outputs have low level.
//Parameter: max_PWM_value ( =< 1000) is maximum value, that can be set by PWM_set_value();
//	Higer values will be replaced by max_PWM_value (saturation effect).
void PWM_init(uint32_t max_PWM_value);

//Enables motor driver IC. Should be called between PWM_init() and any other PWM_... funtion.
//Sets state (3): motor is in short brake. IC driver becames active and dissipates power.
void PWM_enable(void);

//! We have to wait callback if we want to be shure in PWM changing.
//Sets PWM duty cycle, where (0 is 0%, 1000 is 100%).
//Parameters: Sign of Ch_x_value - direction of rotation: >0 CW, 0< CCW.
//	zero value - short brake (through direction outputs).
//PWM value = |Ch_x_value|, if |Ch_x_value| <= max_PWM; else PWM value = max_PWM.
void PWM_set_value(int32_t Ch_A_value, int32_t Ch_B_value);

//Disables motor driver IC (standby mode) for current reduction ( ICC+IM < 2uA).
//All direction and PWM sygnals are ignored. Motor motor is in free rotating mode.
void PWM_disable(void);

//Handler of UEV interrupt of TIM3.
//void TIM3_IRQHandler(void);

#endif
