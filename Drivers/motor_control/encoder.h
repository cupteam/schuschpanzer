#ifndef __ENCODER_H
#define __ENCODER_H

//Library for encremental encoder without contact ringing (for example: hall, optical sensors, but not suitable for mechanical switches).
//The wheel, controlled by encoder can change rotation direction, hence encoder value is signed variable.
//Because TIM2 is 32-bit counter and it has value in -2^31...(2^31)-1 limits, we can count appr. 82km by 34mm diameter wheel @ 1:100 and 28 ppr in one direction (C_MODE_TI1_TI2).
//CW rotation increments encoder, CCW  - decrements.
//It enables GPIO clocks (it can be reenabled safety, if has already enabled).
//??NOTE: This lib assumes, that init is doing after reset and all used here peripheral registers has default reset's value.


//Count modes . In any mode both lines is used, but in first two modes counting will be 2 times slower (2 counts per revolution).
#define C_MODE_TI1_ONLY 2	//Counting only on TI1 edges.
#define C_MODE_TI2_ONLY	1	//Counting only on TI2 edges.
#define C_MODE_TI1_TI2	3	//Counting on TI1 and TI2 edges. Classic encoder mode, 1 count per quadrant.

//Initialization of TIM2.
//Enables clocking of timer module, used GPIO, cofigures pins e.t.c.
//After initialization encoder value is zero.
//Returns: Nothing.
//Parameters: count_mode - C_MODE_TI1_ONLY, C_MODE_TI2_ONLY or C_MODE_TI1_TI2.
//	count_mode - mode of timer (see timer's user manual or C_MODE_XX in encoder.h).
void ENC_A_init(uint8_t count_mode);

//Returns encoder value. (The aggregate movement counted with encremental encoder).
//NOTE: Encoder value is signed.
int32_t ENC_A_value(void);

//Resets encoder value to zero (Zeroizes the aggregate movement).
//It should work even when counter running.
void ENC_A_reset(void);

//Initialization of TIM5.
//Enables clocking of timer module, used GPIO, cofigures pins e.t.c.
//After initialization encoder value is zero.
//Returns: Nothing.
//Parameters: count_mode - C_MODE_TI1_ONLY, C_MODE_TI2_ONLY or C_MODE_TI1_TI2.
//	count_mode - mode of timer (see timer's user manual or C_MODE_XX in encoder.h).
void ENC_B_init(uint8_t count_mode);

//Returns encoder value. (The aggregate movement counted with encremental encoder).
//NOTE: Encoder value is signed.
int32_t ENC_B_value(void);

//Resets encoder value to zero (Zeroizes the aggregate movement).
//It should work even when counter running.
void ENC_B_reset(void);

#endif
