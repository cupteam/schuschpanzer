#include "stm32f4xx.h"
#include "encoder.h"
#include "stm32f411_bit_band.h"
#include "utils.h"

//Initialization of TIM2 (Motor channel - A).
//Enables clocking of timer module, configures pins e.t.c.,
//After initialization encoder value is zero.
//Returns:...
//Parameters:
//	count_mode - mode of timer (see timer's user manual or C_MODE_XX in encoder.h).
void ENC_A_init(uint8_t count_mode)
{
	
	uint32_t temp32;
	uint16_t temp16;
	
	//Enable clocking of TIM2 module (APB1 bus).
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	
	//Enable clocking of GPIOB (for PB3, TIM2_CH2).
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	
	//Enable clocking of GPIOA (for PA15, TIM2_CH1).
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;	
	
	//Configure input pins for timer TIM2 in encoder mode. (TIM2_CH1 - PA15, TIM2_CH2 - PB3)
	/*	For other peripherals:
	� Configure the desired I/O as an alternate function in the GPIOx_MODER register
			NOTE: In this moment pin became an output. And if AF is AF00 (def. after reset), 
			output switches to LOW state until desired AFx will be choosed! Hence this 
			action better make the last (third) in this intialisation.
	� Select the type, pull-up/pull-down and output speed via the GPIOx_OTYPER,
		GPIOx_PUPDR and GPIOx_OSPEEDER registers, respectively
	� Connect the I/O to the desired AFx in the GPIOx_AFRL or GPIOx_AFRH register 
	*/
	
	//Setting Alternative Function for PA15 - TIM2_CH1.	
	temp32 = GPIOA->AFR[1];
	MODIFY_BIT_FIELD(temp32, (((uint32_t) 0xF) << 28),  (((uint32_t) 0x1) << 28)); //AFRH15[3:0] := AF01.
	GPIOA->AFR[1] = temp32;	
		
	
	//So, we need AF01, MODER - AF, OTYPER - not relevant, OSPEEDR - not relevant, PUPDR - no pulling/pushing (we've 3.3k pull-up on sensor's outputs).
	temp32 = GPIOA->MODER;
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER15, GPIO_MODER_MODER15_1); //MODER.PA15 - AF (0b10)
	GPIOA->MODER = temp32;
	
	//GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR2 | GPIO_OSPEEDER_OSPEEDR3); // Low speed. Not relevant.
	//GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_2 | GPIO_OTYPER_OT_3); //Push-pull. Not relevant.
	//GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR3_0); //Pull-up. Not relevant.
	
	
	//Setting Alternative Function for PB3 - TIM2_CH2.
	temp32 = GPIOB->AFR[0];
	MODIFY_BIT_FIELD(temp32, (((uint32_t) 0xF) << 12),  (((uint32_t) 0x1) << 12));
	GPIOB->AFR[0] = temp32;
	
	
	//So, we need AF01, MODER - AF, OTYPER - not relevant, OSPEEDR - not relevant, PUPDR - no pulling/pushing (we've 3.3k pull-up on sensor's outputs).
	temp32 = GPIOB->MODER;
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER3, GPIO_MODER_MODER3_1); //MODER.PB3 - AF (0b10)
	GPIOB->MODER = temp32;
	
	//GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR2 | GPIO_OSPEEDER_OSPEEDR3); // Low speed. Not relevant.
	//GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_2 | GPIO_OTYPER_OT_3); //Push-pull. Not relevant.
	//GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR3_0); //Pull-up. Not relevant.
	
	
	/* Cofiguring the timer (TIM2). */
	
	//So, we have to configure Time Base Unit, General control regs, Slave mode (for encoder) and Input channels.
	
	//Configuration of Time Base Unit
	//1. Set prescaler divide ratio to 1 (without division).
	TIM2->PSC = 0; //Needs in UEV event.
	
	//2. Set Auto Reload Register to max. value: 0xFFFF FFFF. (TIM2 is 32-bit counter).
	TIM2->ARR = 0xFFFFFFFF; //Also needs in UEV event, because we'll enable this feature.
	
	//3. Clear counter.
	TIM2->CNT = 0x00000000;
	
	//Configuration of control registers.
	//1. TIMx_CR1
	temp16 = 0;
	//TIM2_CR1.CKD =  0b00; fDTS = fCK_INT.
	//.ARPE = 1; _ARR register is buffered.
	//.CMS = 00; Edge-aligned (not center) mode.
	//.DIR is auto.
	//.OPM = 0; Counter isn't stopped at UEV.
	//.URS = 0; All UEV sources is available.
	//.UDIS = 0; UEV event is enabled.
	MODIFY_BIT_FIELD(temp16, TIM_CR1_ARPE, TIM_CR1_ARPE);
	TIM2->CR1 = temp16;
	
	//2. TIMx_CR2
	temp16 = 0;
	//.TI1S = 0; Disable XOR of TI1, TI2, TI3 inputs.
	TIM2->CR2 = temp16;
	
	//3. TIMx_SMCR
	temp16 = 0;
	//.ECE = 0; External clock mode 2 is disabled.
	//.ETPS = 00; External trigger prescaler is OFF.
	//.MSM = 0; No action on TRGI.
	//.SMS = 000; Slave mode disabled (we must disable any slave modes to conf. TS bits).
	//TS not used now.
	TIM2->SMCR = temp16;
	
	//Configuration input channels.
	//0. Disable all channels (if enabled). In encoder mode we don't need in CCxE at all.
	temp16 = TIM2->CCER;
	//TIMx_CCER.CC1E = 0;
	//TIMx_CCER.CC2E = 0;
	//TIMx_CCER.CC3E = 0;
	//TIMx_CCER.CC4E = 0;
	MODIFY_BIT_FIELD(temp16, TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E, 0);
	TIM2->CCER = temp16;
	
	//1. Switch Capt./Comp. Ch.1 and Ch.2 to Capture (i.e. input) mode.
	temp16 = TIM2->CCMR1;
	//TIM2_CCMR1.CC1S = 0b01 Input, mapped to TI1.
	//TIM2_CCMR1.CC2S = 0b01 Input, mapped to TI2.
	MODIFY_BIT_FIELD(temp16, TIM_CCMR1_CC1S | TIM_CCMR1_CC2S, TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0);
	
	//2. We don't use input filters - IC1/2F[3:0] = 000;
	//Hence, sampling is done at fDTS. 
	//TIM2_CCMR1.IC1F = 0b000;
	//TIM2_CCMR1.IC2F = 0b000;
	MODIFY_BIT_FIELD(temp16, TIM_CCMR1_IC1F | TIM_CCMR1_IC2F, 0);
	
	TIM2->CCMR1 = temp16;
	
	//3. Set edge detector (polarity) without inversion.
	temp16 = TIM2->CCER;
	//TIM2_CCER.CC1P = 0;
	//TIM2_CCER.CC1NP = 0;
	
	//TIM2_CCER.CC2P = 0;
	//TIM2_CCER.CC2NP = 0;
	
	MODIFY_BIT_FIELD(temp16, TIM_CCER_CC1P | TIM_CCER_CC1NP | TIM_CCER_CC2P |TIM_CCER_CC2NP, 0);
	TIM2->CCER = temp16;
	
	//4. Encoder mode (count on TI1 and/or TI2 edges.)
	temp16 = TIM2->SMCR;
	if(count_mode == C_MODE_TI1_TI2)
		//TIM2_SMCR.SMS = 0b011;
		MODIFY_BIT_FIELD(temp16, TIM_SMCR_SMS, TIM_SMCR_SMS_1 | TIM_SMCR_SMS_0);
	else
		if(count_mode == C_MODE_TI1_ONLY)
			//TIM2_SMCR.SMS = 0b010;
			MODIFY_BIT_FIELD(temp16, TIM_SMCR_SMS, TIM_SMCR_SMS_1);
		else //C_MODE_TI2_ONLY
			//TIM2_SMCR.SMS = 0b001;
			MODIFY_BIT_FIELD(temp16, TIM_SMCR_SMS, TIM_SMCR_SMS_0);
	TIM2->SMCR = temp16;
	
	//Global enable and settings apply.
	//Sw. generation of UEV event.
	//TIM2_EGR.UG = 1; //To apply settings from preload registers.
	BIT_BAND_REG_SET(TIM2_BASE, TIM_EGR, TIM_EGR_UG_BIT_NUM); //It also clears the counter CNT.
	
	//The last action. Counter enable.
	//TIM2_CR1.CEN = 1;	
	BIT_BAND_REG_SET(TIM2_BASE, TIM_CR1, TIM_CR1_CEN_BIT_NUM);
	
	return;
}


//Initialization of TIM5 (Motor channel - B).
//Enables clocking of timer module, cofigures pins e.t.c.,
//After initialization encoder value is zero.
//Returns:...
//Parameters:
//	count_mode - mode of timer (see timer's user manual or C_MODE_XX in encoder.h).
void ENC_B_init(uint8_t count_mode)
{
	
	uint32_t temp32;
	uint16_t temp16;

	//Enable clocking of TIM5 module (APB1 bus).
	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN;
	
	//Enable clocking of GPIOA.
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;	
	
	//Configure input pins for timer TIM5 in encoder mode. (TIM5_CH1 - PA0, TIM5_CH2 - PA1)
	/*	For other peripherals:
	� Configure the desired I/O as an alternate function in the GPIOx_MODER register
			NOTE: In this moment pin became an output. And if AF is AF00 (def. after reset), 
			output switches to LOW state until desired AFx will be choosed! Hence this 
			action better make the last (third) in this intialisation.
	� Select the type, pull-up/pull-down and output speed via the GPIOx_OTYPER,
		GPIOx_PUPDR and GPIOx_OSPEEDER registers, respectively
	� Connect the I/O to the desired AFx in the GPIOx_AFRL or GPIOx_AFRH register 
	*/
	
	//Setting Alternative Function for PA0 - TIM5_CH1, setting Alternative Function for PA1 - TIM5_CH2.
	temp32 = GPIOA->AFR[0];
	MODIFY_BIT_FIELD(temp32, (uint32_t)(0xF << 4 | 0xF << 0), (uint32_t) (0x2 << 4 | 0x2 << 0)); //Set AF02 for both pins.
	GPIOA->AFR[0] = temp32;
	
	
	//So, we need AF02, MODER - AF, OTYPER - not relevant, OSPEEDR - not relevant, PUPDR - no pulling/pushing (we've 3.3k pull-up on sensor's outputs).
	temp32 = GPIOA->MODER;
	MODIFY_BIT_FIELD(temp32, GPIO_MODER_MODER0 | GPIO_MODER_MODER1, (uint32_t)(0x2 << 2 | 0x2)); //MODER.PA1, MODER.PA0 - AF (0b10)
	GPIOA->MODER = temp32;
	
	//GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR2 | GPIO_OSPEEDER_OSPEEDR3); // Low speed. Not relevant.
	//GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_2 | GPIO_OTYPER_OT_3); //Push-pull. Not relevant.
	//GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR3_0); //Pull-up. Not relevant.
	
	
	/* Cofiguring particle timer (TIM5). */
	
	//So, we have to configure Time Base Unit, General control regs, Slave mode (for encoder) and Input channels.
	
	//Configuration of Time Base Unit
	//1. Set prescaler divide ratio to 1 (without division).
	TIM5->PSC = 0; //Needs in UEV event.
	
	//2. Set Auto Reload Register to max. value: 0xFFFF FFFF. (TIM5 is 32-bit counter).
	TIM5->ARR = 0xFFFFFFFF; //Also needs in UEV event, because we'll enable this feature.
	
	//3. Clear counter.
	TIM5->CNT = 0x00000000;
	
	//Configuration of control registers.
	//1. TIMx_CR1
	temp16 = 0;
	//TIM5_CR1.CKD =  0b00; fDTS = fCK_INT.
	//.ARPE = 1; _ARR register is buffered.
	//.CMS = 00; Edge-aligned (not center) mode.
	//.DIR is auto.
	//.OPM = 0; Counter isn't stopped at UEV.
	//.URS = 0; All UEV sources is available.
	//.UDIS = 0; UEV event is enabled.
	MODIFY_BIT_FIELD(temp16, TIM_CR1_ARPE, TIM_CR1_ARPE);
	TIM5->CR1 = temp16;
	
	//2. TIMx_CR2
	temp16 = 0;
	//.TI1S = 0; Disable XOR of TI1, TI2, TI3 inputs.
	TIM5->CR2 = temp16;
	
	//3. TIMx_SMCR
	temp16 = 0;
	//.ECE = 0; External clock mode 2 is disabled.
	//.ETPS = 00; External trigger prescaler is OFF.
	//.MSM = 0; No action on TRGI.
	//.SMS = 000; Slave mode disabled (we must disable any slave modes to conf. TS bits).
	//TS not used now.
	TIM5->SMCR = temp16;
	
	//Configuration input channels.
	//0. Disable all channels (if enabled). In encoder mode we don't need in CCxE at all.
	temp16 = TIM5->CCER;
	//TIMx_CCER.CC1E = 0;
	//TIMx_CCER.CC2E = 0;
	//TIMx_CCER.CC3E = 0;
	//TIMx_CCER.CC4E = 0;
	MODIFY_BIT_FIELD(temp16, TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E, 0);
	TIM5->CCER = temp16;
	
	//1. Switch Capt./Comp. Ch.1 and Ch.2 to Capture (i.e. input) mode.
	temp16 = TIM5->CCMR1;
	//TIM5_CCMR1.CC1S = 0b01 Input, mapped to TI1.
	//TIM5_CCMR1.CC2S = 0b01 Input, mapped to TI2.
	MODIFY_BIT_FIELD(temp16, TIM_CCMR1_CC1S | TIM_CCMR1_CC2S, TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0);
	
	//2. We don't use input filters - IC1/2F[3:0] = 000;
	//Hence, sampling is done at fDTS. 
	//TIM5_CCMR1.IC1F = 0b000;
	//TIM5_CCMR1.IC2F = 0b000;
	MODIFY_BIT_FIELD(temp16, TIM_CCMR1_IC1F | TIM_CCMR1_IC2F, 0);
	
	TIM5->CCMR1 = temp16;
	
	//3. Set edge detector (polarity) without inversion.
	temp16 = TIM5->CCER;
	//TIM5_CCER.CC1P = 0;
	//TIM5_CCER.CC1NP = 0;
	
	//TIM5_CCER.CC2P = 0;
	//TIM5_CCER.CC2NP = 0;
	
	MODIFY_BIT_FIELD(temp16, TIM_CCER_CC1P | TIM_CCER_CC1NP | TIM_CCER_CC2P |TIM_CCER_CC2NP, 0);
	TIM5->CCER = temp16;
	
	//4. Encoder mode (count on TI1 and/or TI2 edges.)
	temp16 = TIM5->SMCR;
	if(count_mode == C_MODE_TI1_TI2)
		//TIM5_SMCR.SMS = 0b011;
		MODIFY_BIT_FIELD(temp16, TIM_SMCR_SMS, TIM_SMCR_SMS_1 | TIM_SMCR_SMS_0);
	else
		if(count_mode == C_MODE_TI1_ONLY)
			//TIM5_SMCR.SMS = 0b010;
			MODIFY_BIT_FIELD(temp16, TIM_SMCR_SMS, TIM_SMCR_SMS_1);
		else //C_MODE_TI2_ONLY
			//TIM5_SMCR.SMS = 0b001;
			MODIFY_BIT_FIELD(temp16, TIM_SMCR_SMS, TIM_SMCR_SMS_0);
	TIM5->SMCR = temp16;
	
	//Global enable and settings apply.
	//Sw. generation of UEV event.
	//TIM5_EGR.UG = 1; //To apply settings from preload registers.
	BIT_BAND_REG_SET(TIM5_BASE, TIM_EGR, TIM_EGR_UG_BIT_NUM); //It also clears the counter CNT.
	
	//The last action. Counter enable.
	//TIM5_CR1.CEN = 1;	
	BIT_BAND_REG_SET(TIM5_BASE, TIM_CR1, TIM_CR1_CEN_BIT_NUM);
	
	return;
	
}

//Returns encoder value. (The aggregate movement counted with encremental encoder).
//NOTE: Encoder value is signed value.
int32_t ENC_A_value(void)
{
	return((int32_t) TIM2->CNT);
}

//Returns encoder value. (The aggregate movement counted with encremental encoder).
//NOTE: Encoder value is signed value.
int32_t ENC_B_value(void)
{
	return((int32_t) TIM5->CNT);
}

//Resets encoder value to zero (Zeroizes the aggregate movement).
//It should work even when counter running.
void ENC_A_reset(void)
{
	TIM2->CNT = 0;
}

//Resets encoder value to zero (Zeroizes the aggregate movement).
//It should work even when counter running.
void ENC_B_reset(void)
{
	TIM5->CNT = 0;
}

