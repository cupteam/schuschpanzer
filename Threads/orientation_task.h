#ifndef __ORIENTATION_TASK_H
#define __ORIENTATION_TASK_H

//Reads data from ADNS-9800 and sends them to UART (for ADNS-9800 optical sensor testing).
void vOrientation_task (void * pvParameters);

//Some task settings.

#define RES_X_AXIS 444 //1800 //Resolution in counts per inch for X axis.
#define RES_Y_AXIS 444 //1800 //Resolution in counts per inch for Y axis.



#define GYRO_FULLSCALE_CODE  FS1000dps //Possible values FS250dps, FS500dps, FS1000dps, FS2000dps
#define GYRO_FULLSCALE_VALUE 1000 //Must be set with accordance to GYRO_FULLSCALE_CODE. 
//TO DO: Autoconversion GYRO_FULLSCALE_CODE to GYRO_FULLSCALE_VALUE

#define ACCEL_FULLSCALE_CODE FS8g //Possible values of full scale FS2g, FS4g, FS8g, FS16g; 
#define ACCEL_FULLSCALE_VALUE 8 //Must be set with accordance to ACCEL_FULLSCALE_CODE.
//TO DO: Autoconversion ACCEL_FULLSCALE_CODE to ACCEL_FULLSCALE_VALUE

#endif

