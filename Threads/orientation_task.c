#include "stm32f4xx.h" // SPL config file stm32f4xx_conf.h is autoincluded (by USE_STDPERIPH_DRIVER in stm32f4xx.h)
#include "stm32f411_bit_band.h" //Macros and definitions for using bit band.
#include "orientation_task.h"

//For UART
#include "UART_output.h"

//For FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

//AVAGO optical sensor ADNS-9800 support.
#include "SPI_ADNS-9800.h" //For MPU9250 interface support.
#include "ADNS-9800_drv.h" //Driver for MPU9250.

//For motor encoders.
#include "encoder.h"
//For motor PWMs.
#include "PWM_AB.h"

//Stdlib functions
#include <stdio.h> //For printf.

extern xQueueHandle xMyQueue;	//Handler of MyQueue

extern xTaskHandle Task1_handler; //Handler of first task to find it's stack watermark

extern volatile uint32_t glob_var1; //For communication between interrupt handler routine and task.

extern volatile uint32_t pwm_ready; //For pwm check.

typedef struct 
{
	uint8_t string[20]; //It must be a null-terminated string.
} text_message;


void vOrientation_task (void * pvParameters)
{
	text_message receive;
	uint8_t world[] = "world!\r\n";
	
	float a = 0.75687, b = 10.5692, c ;
	
	uint32_t irq_var;
	
	UBaseType_t stack_watermark;
	char mark[100];
	
	uint8_t temp, motion_flag;
	uint16_t temp16;
	enum fault state, laser_state;
	enum on_off working;
	ADNS9800_info_t sensor_info; //Information about sensor.
	ADNS9800_out_t movement_out; //Movement data from sensor and some additional data.
	
	//Movement in counts.
	int32_t movement_x, movement_y; //It give above 7 km @ 8200 cpi resolution.
	
	float norm_coef_x, norm_coef_y;
	float float_movement_x_axis, float_movement_y_axis; //Movement in mm.
	

	struct MPU_9250_data_struct
	{
		//Accelerometer
		float ax;
		float ay;
		float az;
		
		//Thermometer
		float t;
//For future sensors.)
//		float tx;
//		float ty;		
//		float tz;
		
		//Gyroscope
		float gx;
		float gy;
		float gz;
		
		//Magnetometer
		float mx;
		float my;
		float mz;
	} sensor_data;

	uint8_t out_data[22]; //For data reading.
	
	int32_t enc_value_A;	//Present movement of wheel (Ch.A). 
	int32_t enc_value_B, temp_i32;	//Present movement of wheel (Ch.B). 
	
	
	
	//Initialization of SPI interface for ADNS9800 data transfer.
	Init_SPI1_for_ADNS9800();
	
	//Turn ON sensor and Load firmware (Shadow ROM).
	//Returns SROM_ID or 0x00 (if SROM loading has failed).
	//Not enables Laser if SROM_ID = 0x00.
	temp = ADNS9800_PWRON();
	
	sprintf(mark,"SROM ID: %u.\r\n", (unsigned int) temp);
	USART_send_my_string((uint8_t *) mark);

	
	/** Some tests and info data to ensure in proper operation of the sensor. **/

	//Reads INFO data from registers.
	ADNS9800_INFO(&sensor_info);
	sprintf(mark,"Product_ID: %u.\r\n", (unsigned int) sensor_info.Product_ID);
	USART_send_my_string((uint8_t *) mark);

	sprintf(mark,"Revision_ID: %u.\r\n", (unsigned int) sensor_info.Revision_ID);
	USART_send_my_string((uint8_t *) mark);


	//Returns 'good' if chip is running SROM code and 'fault' otherwise.
	state = ADNS9800_TST_SROM_running();
	if(state == good)
		sprintf(mark,"SROM is running.\r\n");
	else
		sprintf(mark,"SROM is NOT running!\r\n");
	USART_send_my_string((uint8_t *) mark);
	
	//Returns Laser_NEN status (from LASER_CTRL0 reg.) and enables/disables laser continuous ON mode.
	//After reset, laser is NOT in cotinuous mode. Also, reading Motion reg. for ex. by OUT functions,
	//	disbles continuous mode.
	working = ADNS9800_TST_Laser_Control(disable);
	if(working == enable)
		sprintf(mark,"Laser_NEN is enabled.\r\n");
	else
		sprintf(mark,"Laser_NEN is disabled!\r\n");
	USART_send_my_string((uint8_t *) mark);
	
	//Starts internal CRC SROM test procedure, results valid only after SROM has downloaded.
	//Also reads Motion register, hence clears residual motion for _OUT_ funtions.
	//During CRC test, navigation is halted, SPI should not be used.
	//Returns 'good' if SROM CRC is good and 'fault' otherwise.
	//Params: *Laser_FAULT_p becames 'good', if XY_LASER pin is not shorted to GND
	//	and 'fault', otherwise.
	
	/*
	state = ADNS9800_TST_SROM_CRC(&laser_state);
	if(state == good)
		sprintf(mark,"SROM CRC is correct.\r\n");
	else
		sprintf(mark,"SROM CRC test has failed!\r\n");
	USART_send_my_string((uint8_t *) mark);
	
	//Clear residual motion.
	movement_x = 0;
	movement_y = 0;

	if(laser_state == good)
		sprintf(mark,"XY_LASER pin is good.\r\n");
	else
		sprintf(mark,"XY_LASER pin is fault! (shorted to GND)\r\n");
	USART_send_my_string((uint8_t *) mark);
*/	
		
		//Clear residual motion.
	movement_x = 0;
	movement_y = 0;
		
		
	//Sets resolution for X and Y axes. Resolution = 200 cpi * res_x (res_y, for Y axe).
	//Max value for res_x and res_y is 0x29 (eq. 8200 cpi resolution). Def value = 0x09 (eq. 1800 cpi) for both axes.
	ADNS9800_CONF_resolution(0x09, 0x09);
	
	//Initialization of encoder.
	ENC_A_init(C_MODE_TI1_TI2);
	ENC_B_init(C_MODE_TI1_TI2);
	
	//Init of PWMs, max_PWM = 1000;
	PWM_init(1000);
	sprintf(mark,"PWM_init has done!\r\n");
	USART_send_my_string((uint8_t *) mark);
	
	PWM_enable();
	sprintf(mark,"PWM_enable has done!\r\n");
	USART_send_my_string((uint8_t *) mark);
	
//	PWM_set_value(300, 150);
	
	while(1)
	{
		/*
//	SPI_20MHz_batch_read_mpu9250(MPU9250_ACCEL_XOUT_H, out_data, 22);
SPI_1MHz_batch_read_mpu9250(MPU9250_ACCEL_XOUT_H, out_data, 22);
		out_raw_struct = (MPU_9250_raw_data_t *) out_data; */
		
		//Reads Motion register, and filles struct fields by movement data.
		//If motion from last report has occured - returns 1 (or 0 otherwise).
		//Function also filles struct fields by state of some Motion register's bits.
		motion_flag = ADNS9800_OUT_movement(&movement_out);
			
		if(movement_out.LASER_fault == fault)
		{
			sprintf(mark,"XY_LASER pin is fault! (shorted to GND)\r\n");
			USART_send_my_string((uint8_t *) mark);
		}
		
		if(movement_out.LP_Valid == fault)
		{
			sprintf(mark,"Laser Power hasn't compementary values!\r\n");
			USART_send_my_string((uint8_t *) mark);
		}
		
		if(movement_out.OP_Mode != 0x00) //If we are not in Run mode.
		{
			sprintf(mark,"We in some of Rest modes!\r\n");
			USART_send_my_string((uint8_t *) mark);
		}
		
		//Movement in counts.
		movement_x += movement_out.Delta_X;
		movement_y += movement_out.Delta_Y;
		
 	
		//Conversion raw data in movement_x/y to float (in mm) with normalization.
		//
		
		norm_coef_x = (25.4 / RES_X_AXIS); //1 inch = 25.4 mm.
		norm_coef_y = (25.4 / RES_Y_AXIS); //1 inch = 25.4 mm.
		
		float_movement_x_axis = movement_x * norm_coef_x;
		float_movement_y_axis = movement_y * norm_coef_y;
		
		enc_value_A = ENC_A_value(); //Get movement.
		enc_value_B = ENC_B_value(); //Get movement.
			
		sprintf(mark,"PWM_ready: %u\t", pwm_ready);
		USART_send_my_string((uint8_t *) mark);
		
		if(glob_var1 < 5 || glob_var1 >= 10)
		{
			if(temp_i32 != enc_value_B/100)
			{
				PWM_set_value(enc_value_B/100, enc_value_B/100); //Use movement of A channel as PWM value.
					sprintf(mark,"PWM_ready: %u\t", pwm_ready);
					USART_send_my_string((uint8_t *) mark);
			}
			sprintf(mark,"PWM_B: %i, gv: %u\t", enc_value_B/100, glob_var1);
//		PWM_set_value(0, 250); //Use movement of A channel as PWM value.
//		sprintf(mark,"PWM_B value: %u\r\n", 250);
			USART_send_my_string((uint8_t *) mark);
		}
		else
		{
			if(glob_var1 == 9)
			{
				PWM_enable();
				sprintf(mark,"PWM enabled. \t");
				USART_send_my_string((uint8_t *) mark);
				glob_var1++;
			}
			
			if(glob_var1 == 7)
			{
				PWM_disable();
				sprintf(mark,"PWM disabled. \t");
				USART_send_my_string((uint8_t *) mark);
				glob_var1++;
			}
			
			if(glob_var1 == 5)
			{
				PWM_set_value(0, 0); //Set to 0 both pwm channels.
				sprintf(mark,"PWMs = 0 \t");
				USART_send_my_string((uint8_t *) mark);
				glob_var1++;
			}
		}
		
		temp_i32 = enc_value_B/100; //Store previous value.
/*		
		sensor_data.ax = ((int16_t) temp16) * norm_coef;
		
		temp16 = ((uint16_t) out_raw_struct->accel_yH) << 8 | out_raw_struct->accel_yL;
		sensor_data.ay = ((int16_t) temp16) * norm_coef;
		
		temp16 = ((uint16_t) out_raw_struct->accel_zH) << 8 | out_raw_struct->accel_zL;
		sensor_data.az = ((int16_t) temp16) * norm_coef;
		
			//Thermometer
		temp16 = ((uint16_t) out_raw_struct->temp_H) << 8 | out_raw_struct->temp_L;
		sensor_data.t = ( (int16_t) temp16 - 0) / 333.87 + 21; //p.33 of MPU9250 Reg Map & Descr and MPU9150 datasheet.
		
		//Gyroscope
		norm_coef = (GYRO_FULLSCALE_VALUE / 32768.0);
		temp16 = ((uint16_t) out_raw_struct->gyro_xH) << 8 | out_raw_struct->gyro_xL;
		sensor_data.gx = ((int16_t) temp16) * norm_coef;
		
		temp16 = ((uint16_t) out_raw_struct->gyro_yH) << 8 | out_raw_struct->gyro_yL;
		sensor_data.gy = ((int16_t) temp16) * norm_coef;
		
		temp16 = ((uint16_t) out_raw_struct->gyro_zH) << 8 | out_raw_struct->gyro_zL;
		sensor_data.gz = ((int16_t) temp16) * norm_coef;
		
		//Magnetometer
		//TO DO: Here we should test mag_ST1 and mag_ST2 for correctness of mag. data.
		norm_coef = ( 4800 / 32768.0); //Mag full scale = 4800 uT, and we have 16-bit output setting.
		temp16 = ((uint16_t) out_raw_struct->mag_xH) << 8 | out_raw_struct->mag_xL;
		sensor_data.mx = ((int16_t) temp16) * norm_coef;
		sensor_data.mx = sensor_data.mx * ((((uint16_t) my_AK8963_calibr_data.asax) + 128)/256.0); //Factory's sensivity adjustment.
		
		temp16 = ((uint16_t) out_raw_struct->mag_yH) << 8 | out_raw_struct->mag_yL;
		sensor_data.my = ((int16_t) temp16) * norm_coef;
		sensor_data.my = sensor_data.my * ((((uint16_t) my_AK8963_calibr_data.asay) + 128)/256.0); //Factory's sensivity adjustment.
		
		temp16 = ((uint16_t) out_raw_struct->mag_zH) << 8 | out_raw_struct->mag_zL;
		sensor_data.mz = ((int16_t) temp16) * norm_coef;
		sensor_data.mz = sensor_data.mz * ((((uint16_t) my_AK8963_calibr_data.asaz) + 128)/256.0); //Factory's sensivity adjustment.
	*/
//		if(motion_flag == 0)
//		{
//			sprintf(mark,"No_motion.");
//			USART_send_my_string((uint8_t *) mark);
//		}
		
		sprintf(mark,"x: %f\t", float_movement_x_axis);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark,"y: %f\t", float_movement_y_axis);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark,"Encoder A: %-11d\t", enc_value_A);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark,"Encoder B: %-11d\r\n", enc_value_B);
		USART_send_my_string((uint8_t *) mark);

/*		
		//Returns Laser_NEN status (from LASER_CTRL0 reg.) and enables/disables laser continuous ON mode.
		//After reset, laser is NOT in cotinuous mode. Also, reading Motion reg. for ex. by OUT functions,
		//	disbles continuous mode.
		working = ADNS9800_TST_Laser_Control(disable);
		if(working == enable)
			sprintf(mark,"Laser_NEN is enabled.\r\n");
		else
			sprintf(mark,"Laser_NEN is disabled!\r\n");
		USART_send_my_string((uint8_t *) mark);
	
		
		//Accel
		sprintf(mark,"a: %f", sensor_data.ax);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark," %f", sensor_data.ay);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark," %f\t", sensor_data.az);
		USART_send_my_string((uint8_t *) mark);
		
		//Gyro
		sprintf(mark,"g: %f", sensor_data.gx);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark," %f", sensor_data.gy);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark," %f\t", sensor_data.gz);
		USART_send_my_string((uint8_t *) mark);
	
		//Magnetometer
		sprintf(mark,"m: %f", sensor_data.mx);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark," %f", sensor_data.my);
		USART_send_my_string((uint8_t *) mark);
		
		sprintf(mark," %f\t", sensor_data.mz);
		USART_send_my_string((uint8_t *) mark);
		
		//Thermo
		sprintf(mark,"Therm = %f C.\r\n", sensor_data.t);
		USART_send_my_string((uint8_t *) mark);
*/		
		//Receive message from vTask1
		xQueueReceive(xMyQueue, &receive, portMAX_DELAY);
		
		//Sending messages to terminal, it takes approx. 50ms.
//		USART_send_my_string(receive.string);
//		USART_send_my_string(world);
		
		//Sending stack watermark data
		stack_watermark = uxTaskGetStackHighWaterMark( Task1_handler ); //For first task
		sprintf(mark,"StackWaterMark for Task1: %u.\r\n", (unsigned int) stack_watermark);
//		USART_send_my_string((uint8_t *) mark);
		
		stack_watermark = uxTaskGetStackHighWaterMark( NULL ); //For second task
		sprintf(mark,"StackWaterMark for Task2: %u.\r\n", (unsigned int) stack_watermark);
//		USART_send_my_string((uint8_t *) mark);
/*		
		c = a+b;
		sprintf(mark,"a+b = %f+%f = %f\r\n", a, b, c);
		USART_send_my_string((uint8_t *) mark);
		
		c = a-b;
		sprintf(mark,"a-b = %f-%f = %f\r\n", a, b, c);
		USART_send_my_string((uint8_t *) mark);
					
		c = a*b;
		sprintf(mark,"a*b = %f*%f = %f\r\n", a, b, c);
		USART_send_my_string((uint8_t *) mark);
		
		c = a/b;
		sprintf(mark,"a/b = %f/%f = %f\r\n", a, b, c);
		USART_send_my_string((uint8_t *) mark);
		
		irq_var = glob_var1; //Test atomarity
		sprintf(mark,"glob_var1 = %u\r\n", irq_var);
		USART_send_my_string((uint8_t *) mark);
	*/	
		
		//Delay for approx. 50ms + 1s.
		vTaskDelay(100);
	}
}
